#!/bin/bash
# https://docs.google.com/document/d/1hemKdspC7vx55ahje69FJW9cQTJpy2t7uvB9kUfsjSU/
unalias git1
alias git1='git clone --recursive --depth 1'

git1 https://github.com/nasa/Common-Metadata-Repository

#Code Commit repo is in AWS Account 868303011581
git1 https://git-codecommit.us-east-1.amazonaws.com/v1/repos/noaa-cmr-cloud-deployments

git1 https://git.earthdata.nasa.gov/scm/cmr/populate-parameter-store.git

git1 https://github.com/littleidiot40/cmr-opensearch/
pushd cmr-opensearch
git checkout 4f40d001c4e9e87562f7ff34f56b5e49c899d42e # applies fix
popd

# extern deps:
https://leiningen.org/#install
https://www.terraform.io/downloads.html
https://leiningen.org/#install
https://releases.hashicorp.com/packer/1.7.2/packer_1.7.2_linux_amd64.zip
https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html

