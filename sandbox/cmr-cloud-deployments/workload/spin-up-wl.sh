#!/bin/bash
# Run from top level repo directory.
# Sets up core infrastructure and apps.
# Accepts two command line arguments:
#   environment: The environment name (wl or smwl)
#   auto-approve: true to apply auto approve in terraform, anything else is false.
# Runs in the following order:
#   1) Spinup elasticsearch
#   2) Deploy infrastructure
#   3) Deploy apps

set -e

script_name=$(basename $0)

if [ $# -ne 2 ]; then
  printf "Usage: $script_name <environment> <auto-approve>\n"
  exit 1
fi

auto_approve=""
if [ $2 = "true" ]; then
  auto_approve="-auto-approve"
fi

export TF_VAR_environment_name=$1
export TF_VAR_exec_task=false
export TF_VAR_snapshot_id="${SNAPSHOT_ID:?Need to set SNAPSHOT_ID to db snapshot to restore from.}"
# These variable setup the tunnel to elasticsearch.
export REMOTE_PORT="${REMOTE_PORT:-80}"
export LOCAL_PORT="${LOCAL_PORT:-8080}"
export REMOTE_HOST="${REMOTE_HOST:?Use terraform to fetch the correct load-balancer host for ES}"
export ELASTIC_HOST="${ELASTIC_HOST:-http://localhost:8080}"
export SSH_PRIVATE_KEY_FILE="${SSH_PRIVATE_KEY_FILE:-~/.ssh/id_rsa}"

JUMP_BOX=$(aws ec2 describe-instances \
               --filter "Name=tag:Name,Values=*SSM Jumpbox*" \
               --query "Reservations[].Instances[?State.Name == 'running'].InstanceId[]" \
               --output text)

ssh -i ${SSH_PRIVATE_KEY_FILE} -f -N \
    -L ${LOCAL_PORT}:${REMOTE_HOST}:${REMOTE_PORT} \
    ec2-user@${JUMP_BOX} \
    -o ProxyCommand="aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters 'portNumber=%p'" \
    -o ServerAliveInterval=60 \
    -o UserKnownHostsFile=/dev/null \
    -o StrictHostKeyChecking=no

printf "Starting elastic...\n"
(workload/spin-up-elasticsearch.sh; scripts/kill_aws_tunnel.sh) &
elastic_pid=$!

printf "Creating infrastructure...\n"
(cd terraform/deployments/core_infrastructure && terraform init -no-color && terraform apply -no-color $auto_approve)

printf "Setting up parameter store...\n"
./workload/setup-parameter-store.sh $TF_VAR_environment_name &
parameter_store_pid=$!

printf "Creating app infrastructure...\n"
(cd terraform && terraform init -no-color && terraform apply -no-color $auto_approve)

printf "Creating driver infrastructure...\n"
(cd terraform/deployments/workload && terraform init -no-color && terraform apply -no-color $auto_approve)

wait $elastic_pid
wait $parameter_store_pid
