#!/bin/bash
# Call from top level repo directory.
# Need to point some of the public host parameters to the load balancer for workload.
# The load balancer is destroyed on spin down so run this script to ensure vars are
# pointing to the new one.

set -e
script_name=$(basename $0)

if [ $# -ne 1 ]; then
  printf "Usage: $script_name <environment-name>\n"
  exit 1
fi

environment_name=$1

load_balancer=$(cd terraform/deployments/core_infrastructure && terraform output -raw app_load_balancer_dns_name)

hosts=(access-control/CMR_ACCESS_CONTROL_PUBLIC_HOST
       ingest/CMR_INGEST_PUBLIC_HOST
       legacy-services/CMR_PUBLIC_SERVER
       opensearch/server_name
       search/CMR_SEARCH_PUBLIC_HOST
       service-bridge/CMR_SERVICE_BRIDGE_PUBLIC_HOST)

for i in "${hosts[@]}"
do
  set -x
  aws ssm put-parameter --overwrite --name "/$environment_name/$i" --type String --value="$load_balancer"
  { set +x; } 2>/dev/null
done

# Set parameters with path
set -x
aws ssm put-parameter --overwrite \
                      --name "/$environment_name/legacy-services/CMR_CMR_PUBLIC_SEARCH_ENDPOINT" \
                      --type String \
                      --value="$load_balancer/search"

aws ssm put-parameter --overwrite \
                      --name "/$environment_name/legacy-services/CMR_WSDL_ROOT" \
                      --type String \
                      --value="$load_balancer/legacy-services/echo-wsdl/v10/"

aws ssm put-parameter --overwrite \
                      --name "/$environment_name/opensearch/opensearch_url" \
                      --type String \
                      --value="$load_balancer/opensearch"

aws ssm put-parameter --overwrite \
                      --name "/$environment_name/opensearch/public_catalog_rest_endpoint" \
                      --type String \
                      --value="$load_balancer/search/"
