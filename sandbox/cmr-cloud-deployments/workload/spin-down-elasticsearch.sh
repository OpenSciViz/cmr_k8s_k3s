#!/bin/bash
# Shuts down all of the Elasticsearch instances running in workload.

NUM_EXPECTED_SUSPENDED_PROCESSES=9

spin_down_asg() {
  # Suspend auto-scaling - extremely important that the processes are suspended
  # otherwise we lose all the data because the ASG will terminate the instances
  # and start new ones
  declare asg_name="$1"
  aws autoscaling suspend-processes --auto-scaling-group-name $asg_name

  # Verify auto-scaling processes are suspended
  suspended_count="$(aws autoscaling describe-auto-scaling-groups \
                       --auto-scaling-group-name "$asg_name" \
                       | jq ".AutoScalingGroups[0].SuspendedProcesses | length")"

  if [ $suspended_count -ge $NUM_EXPECTED_SUSPENDED_PROCESSES ]; then
    printf "$(date) Verified the correct number of suspended processes: $suspended_count.\n"
  else
    printf "$(date) ERROR: Suspend processes did not successfully complete. Only $suspended_count processes, but there should be $NUM_EXPECTED_SUSPENDED_PROCESSES. Exiting.\n"
    exit 1
  fi

  # Get all of the Elasticsearch instance IDs in the auto-scaling group
  instance_ids="$(aws autoscaling describe-auto-scaling-groups \
                    --auto-scaling-group-name "$asg_name" \
                    | jq ".AutoScalingGroups[0].Instances[].InstanceId" \
                    | tr -d '\n' \
                    | sed 's/"/ /g')"

  printf "$(date) Stopping Instance IDs $instance_ids\n"

  # Shut down all of the Elasticsearch instance IDs
  aws ec2 stop-instances --instance-ids $instance_ids
}

data_asg="${DATA_AUTO_SCALING_GROUP_NAME:-cmr-wl-elasticsearch-cluster-ElasticClusterAutoscalingGroup-2UEHG79KKREV}"
master_asg="${MASTER_AUTO_SCALING_GROUP_NAME:-cmr-wl-elasticsearch-cluster-ElasticClusterMasterNodeAutoscalingGroup-1ENFUKA71E5V5}"
printf "$(date) Spin down Elasticsearch data nodes auto scaling group.\n"
spin_down_asg $data_asg
printf "$(date) Spin down Elasticsearch master nodes auto scaling group.\n"
spin_down_asg $master_asg
