#!/bin/bash
# Teardown workload run.
# Accepts two command line arguments:
#   environment: The environment name (wl or smwl)
#   auto-approve: true to apply auto approve in terraform, anything else is false.
# Destroys in following order:
#   1) Workload drivers
#   2) Apps
#   3) Infrastructure
#   4) Elasticsearch

set -e

script_name=$(basename $0)

if [ $# -ne 2 ]; then
  printf "Usage: $script_name <environment> <auto-approve>\n"
  exit 1
fi

auto_approve=""
if [ $2 = "true" ]; then
  auto_approve="-auto-approve"
fi

export TF_VAR_environment_name=$1

printf "Destroying workload drivers...\n"
(cd terraform/deployments/workload && terraform init -no-color)
if [ ! -z "$(cd terraform/deployments/workload && \
                terraform show -json | jq -r '.values | select(.!=null)')" ]; then
  cluster=$(cd terraform/deployments/workload && terraform output -json | jq -r '.cluster_name.value')
  if [ $cluster != "null" ]; then
    while read task; do
      aws ecs stop-task --cluster $cluster --task $task
    done < <(aws ecs list-tasks --cluster $cluster | jq -r -c '.taskArns[]')
  fi
  (cd terraform/deployments/workload && terraform destroy -no-color $auto_approve)
else
  printf "No workload drivers to destroy...\n"
fi

printf "Destroying apps...\n"
(cd terraform && terraform init -no-color)
if [ ! -z "$(cd terraform && \
                terraform show -json | jq -r '.values | select(.!=null)')" ]; then
  (cd terraform && terraform destroy -no-color $auto_approve)
else
  printf "No apps to destroy...\n"
fi

printf "Destroying infrastructure...\n"
(cd terraform/deployments/core_infrastructure && terraform init -no-color)
if [ ! -z "$(cd terraform/deployments/core_infrastructure && \
                terraform show -json | jq -r '.values | select(.!=null)')" ]; then
  (cd terraform/deployments/core_infrastructure && terraform destroy -no-color $auto_approve)
else
  printf "No infrastructure to destroy...\n"
fi

printf "Stopping elastic...\n"
workload/spin-down-elasticsearch.sh

printf "Workload successfully torn down.\n"
