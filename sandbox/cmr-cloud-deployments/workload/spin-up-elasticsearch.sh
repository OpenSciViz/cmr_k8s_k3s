#!/bin/bash
# Starts up all of the Elasticsearch instances in workload. Note that the
# ELASTIC_HOST must be set outside this script

set -e

wait_for_green() {
  # Poll cluster health until green.
  # Retry count defaults to 120 which is 60 minutes.
  green=false
  retry_count=${RETRY_COUNT:-120}
  while [[ $green == false ]] && [[ $retry_count -gt 0 ]]
  do
    status="$(curl -s -k $ELASTIC_HOST/_cluster/health \
               | jq ".status")"
    if [[ $status != "\"green\"" ]]; then
      printf "$(date) Elasticsearch not yet green. Health is ${status}. Waiting...\n"
      retry_count=$((retry_count-1))
      sleep 30
    else
      green=true
    fi
  done
  if [[ $green == true ]]; then
    printf "$(date) Elasticsearch is now green.\n"
  else
    printf "$(date) ERROR: Elasticsearch never reached green state. Health is ${status}. Exiting.\n"
    exit 1
  fi
}

if [[ -z "$ELASTIC_HOST" ]]; then
  printf "$(date) ERROR: ELASTIC_HOST must be set. Exiting.\n"
  exit 1
fi

DATA_AUTO_SCALING_GROUP_NAME="${DATA_AUTO_SCALING_GROUP_NAME:-cmr-wl-elasticsearch-cluster-ElasticClusterAutoscalingGroup-2UEHG79KKREV}"
NUM_EXPECTED_DATA_INSTANCES=15

# Get a list of all the EC2 instances in the workload elasticsearch auto-scaling group
num_instance_ids="$(aws autoscaling describe-auto-scaling-groups \
                      --auto-scaling-group-name "$DATA_AUTO_SCALING_GROUP_NAME" \
                      | jq ".AutoScalingGroups[0].Instances | length")"

# Make sure that the correct number (currently 15) is returned
if [ $num_instance_ids == $NUM_EXPECTED_DATA_INSTANCES ]; then
  printf "$(date) Verified the correct number of data instances: $num_instance_ids.\n"
else
  printf "$(date) ERROR: Found $num_instance_ids data instances in the auto scaling group, but there should be $NUM_EXPECTED_DATA_INSTANCES. Exiting.\n"
  exit 1
fi

instance_ids="$(aws autoscaling describe-auto-scaling-groups \
                  --auto-scaling-group-name "$DATA_AUTO_SCALING_GROUP_NAME" \
                  | jq ".AutoScalingGroups[0].Instances[].InstanceId" \
                  | tr -d '\n' \
                  | sed 's/"/ /g')"

printf "$(date) Starting up ES data instances $instance_ids\n"
aws ec2 start-instances --instance-ids $instance_ids

MASTER_AUTO_SCALING_GROUP_NAME="${MASTER_AUTO_SCALING_GROUP_NAME:-cmr-wl-elasticsearch-cluster-ElasticClusterMasterNodeAutoscalingGroup-1ENFUKA71E5V5}"
NUM_EXPECTED_MASTER_INSTANCES=3

# Get a list of all the EC2 instances in the workload elasticsearch auto-scaling group
num_instance_ids="$(aws autoscaling describe-auto-scaling-groups \
                      --auto-scaling-group-name "$MASTER_AUTO_SCALING_GROUP_NAME" \
                      | jq ".AutoScalingGroups[0].Instances | length")"

# Make sure that the correct number (currently 3) is returned
if [ $num_instance_ids == $NUM_EXPECTED_MASTER_INSTANCES ]; then
  printf "$(date) Verified the correct number of master instances: $num_instance_ids.\n"
else
  printf "$(date) ERROR: Found $num_instance_ids master instances in the auto scaling group, but there should be $NUM_EXPECTED_MASTER_INSTANCES. Exiting.\n"
  exit 1
fi

instance_ids="$(aws autoscaling describe-auto-scaling-groups \
                  --auto-scaling-group-name "$MASTER_AUTO_SCALING_GROUP_NAME" \
                  | jq ".AutoScalingGroups[0].Instances[].InstanceId" \
                  | tr -d '\n' \
                  | sed 's/"/ /g')"

printf "$(date) Starting up ES master instances $instance_ids\n"
aws ec2 start-instances --instance-ids $instance_ids

# Note: this needs to be run in a top level script if running this in parallel
# Wait until Elasticsearch is in a green state
wait_for_green

# Only after Elasticsearch is green resume auto-scaling
printf "$(date) Resuming auto-scaling processes now that Elasticsearch is green (safe to do).\n"
aws autoscaling resume-processes --auto-scaling-group-name $DATA_AUTO_SCALING_GROUP_NAME
aws autoscaling resume-processes --auto-scaling-group-name $MASTER_AUTO_SCALING_GROUP_NAME

# Restore from snapshot if SNAPSHOT_REPOSITORY and SNAPSHOT_TO_RESTORE are non-empty.
if [[ ! -z $SNAPSHOT_REPOSITORY ]] && [[ ! -z $SNAPSHOT_TO_RESTORE ]]; then
  printf "$(date) Restoring from snapshot: $SNAPSHOT_REPOSITORY/$SNAPSHOT_TO_RESTORE...\n"
  curl -XPOST -i $ELASTIC_HOST/*/_close?pretty
  curl -XPOST -i -H 'Content-Type: application/json' $ELASTIC_HOST/_snapshot/${SNAPSHOT_REPOSITORY}/${SNAPSHOT_TO_RESTORE}/_restore?pretty -d '{"indices": "*,-.slm*,-ilm*,-.transfrom*,-security*"}'
  # Wait until Elasticsearch is in a green state
  wait_for_green
  curl -XPOST -i $ELASTIC_HOST/*/_open?pretty
fi
