#!/usr/bin/env python3

from distutils.core import setup

setup(name="ngap-verification",
      version="1.0",
      description="ngap 2.0 cmr tests",
      install_requires=[
        "beautifulsoup4",
        "lxml",
        "requests",
        "pytest",
        "pytest-xdist",
        "Pillow"])
