"""
Common CMR utility functions.
"""
import concurrent.futures
from config import config
import itertools
import json
import os
import pytest
import requests
import re
import templates
import threading
import time

# A hacky way to parallelize testing.
WORKER_NUM = int(re.search(r'\d+', os.environ.get("PYTEST_XDIST_WORKER", "0")).group())
MAX_IDS_PER_WORKER = 2000
COUNTER = itertools.count(WORKER_NUM * MAX_IDS_PER_WORKER)

# Created resources to cleanup after session.
CREATED_RESOURCES = {"collection": set(),
                     "variable": set(),
                     "service": set(),
                     "acl": set(),
                     "group": set()}

# Lock for ingest events.
INGEST_LOCK = threading.Lock()

def generate_id():
  """
  increment counter.

  Raises:
    Exception: Ran out of id space for worker.
  """
  identifier = next(COUNTER)
  if identifier >= (WORKER_NUM * MAX_IDS_PER_WORKER + MAX_IDS_PER_WORKER):
    raise Exception("Ran out of identifier spaces.")
  return identifier

def generate_native_id():
  """
  Generate a native id.
  """
  return "{default_native_id}_{id_number}".format(default_native_id=config.default_native_id,
                                                  id_number=generate_id())
def wait_for_concept_index(create,
                           search_fn,
                           concept_id,
                           revision_id,
                           concept_type,
                           max_poll_wait=config.max_poll_wait,
                           poll_interval=config.poll_interval,
                           token=config.token):
  """
  Wait for concept to be indexed.

  Params:
    create: True or False. True == Create operation. False == Delete operation.
    search_fn: Search function to find if indexed.
    concept_id: The concept ID to search for.
    concept_type: The type of the concept.
    max_poll_wait: Maximum time to wait for index.
    poll_interval: The interval to poll at.
  """
  expected_hits = 1 if create else 0
  timeout = time.time() + max_poll_wait
  concept_id_key = 'id' if concept_type == 'acl' else 'concept-id'
  params = {concept_id_key: concept_id}
  if 'granule' == concept_type:
    accept = 'application/vnd.nasa.cmr.umm+json;version=1.4'
  elif 'collection' == concept_type:
    accept = 'application/vnd.nasa.cmr.umm+json'
  else:
    accept = 'application/json'
  headers = {"Authorization": token,
             "Accept": accept}
  while True:
    search_response = search_fn(concept_type, params=params, headers=headers)
    search_response.raise_for_status()
    if int(search_response.headers["CMR-Hits"]) == expected_hits:
      if expected_hits == 1:
        if concept_type in {"collection", "granule"}:
          found_rev = search_response.json()["items"][0]["meta"]["revision-id"]
        else:
          found_rev = search_response.json()["items"][0]["revision_id"]
        if found_rev == revision_id: break
      else:
        break
    if time.time() > timeout:
      raise TimeoutError("Concept indexing time has exceeded maximum poll wait time.")
    time.sleep(poll_interval)

def search(concept_type,
           params=None,
           search_endpoint=config.search_endpoint,
           extension='',
           headers=None):
  """
  Search CMR with params.

  Params:
    concept_type: Concept type to search.
    params: Parameters to search with.
    search_endpoint: Search endpoint to search against.
    extension: Extension to search with (json, umm_json, echo10, etc.).
    headers: Headers to search with

  Returns:
    requests object: Response from search request.
  """
  if extension != '': extension = ".{extension}".format(extension=extension)
  search_url = "{search_endpoint}/{concept_type}s{extension}".format(search_endpoint=search_endpoint,
                                                                     concept_type=concept_type,
                                                                     extension=extension)
  return requests.get(search_url, headers=headers, params=params)


def get_ingest_url(ingest_endpoint, concept_type, native_id, provider):
  """
  Construct ingest URL.

  Params:
    ingest_endpoint: Ingest endpoint.
    concept_type: The concept type.
    native_id: Native ID to ingest.
    provider: Provider to ingest into.

  Returns:
    string: Constructed Ingest URL.
  """
  ingest_url = "{ingest_endpoint}/providers/{provider}/{concept_type}s/{native_id}".format(ingest_endpoint=ingest_endpoint,
                                                                                           provider=provider,
                                                                                           concept_type=concept_type,
                                                                                           native_id=native_id)
  return ingest_url

def ingest(concept_type,
           content_type,
           data,
           native_id=None,
           token=None,
           provider=config.provider,
           ingest_endpoint=config.ingest_endpoint,
           wait_for_index=True,
           max_poll_wait=config.max_poll_wait,
           poll_interval=config.poll_interval):
  """
  Ingest a concept into CMR.

  Params:
    concept_type: Concept type to ingest.
    content_type: Content type of data.
    native_id: Native ID to ingest.
    data: Data to ingest.
    token: Echo token.
    provider: Provider to ingest into.
    ingest_endpoint: Ingest endpoint to ingest into.
    wait_for_index: Wait for the concept to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
    requests object: Response from PUT request.

  Raises:
    TimeoutError: Maximum poll wait time has been exceeded.
  """
  if not native_id: native_id = generate_native_id()
  ingest_url = get_ingest_url(ingest_endpoint, concept_type, native_id, provider)
  headers = {"Authorization": token,
             "Content-Type": content_type,
             "Accept": "application/json"}
  response = requests.put(ingest_url, headers=headers, data=data)
  try:
    response.raise_for_status()
    with INGEST_LOCK:
      if concept_type != 'granule':
        CREATED_RESOURCES[concept_type].add(native_id)
  except requests.exceptions.HTTPError:
    return response
  if wait_for_index:
    j = response.json()
    wait_for_concept_index(True,
                           search,
                           j["concept-id"],
                           j["revision-id"],
                           concept_type,
                           max_poll_wait,
                           poll_interval)
  return response

def delete(concept_type,
           native_id,
           token=None,
           provider=config.provider,
           ingest_endpoint=config.ingest_endpoint,
           wait_for_index=False,
           max_poll_wait=config.max_poll_wait,
           poll_interval=config.poll_interval):
  """
  Delete a concept into CMR.

  Params:
    concept_type: Concept type to ingest.
    native_id: Native ID to ingest.
    token: Echo token.
    provider: Provider to ingest into.
    ingest_endpoint: Ingest endpoint to ingest into.
    wait_for_index: Wait for the concept to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
    requests object: Response from DELETE request.
  """
  ingest_url = get_ingest_url(ingest_endpoint, concept_type, native_id, provider)
  headers = {"Authorization": token,
             "Accept": "application/json"}
  response = requests.delete(ingest_url, headers=headers)
  try:
    response.raise_for_status()
    with INGEST_LOCK:
      if concept_type != 'granule' and native_id in CREATED_RESOURCES[concept_type]:
        CREATED_RESOURCES[concept_type].remove(native_id)
  except requests.exceptions.HTTPError:
    return response
  if wait_for_index:
    j = response.json()
    wait_for_concept_index(False,
                           search,
                           j["concept-id"],
                           j["revision-id"],
                           concept_type,
                           max_poll_wait,
                           poll_interval)
  return response

def search_opensearch(concept_type,
                      opensearch_endpoint=config.opensearch_endpoint,
                      params=None,
                      headers=None):
  """
  Search CMR Opensearch with params.

  Params:
    concept_type: Concept type to search (collection or granule).
    params: Parameters to search with.
    opensearch_endpoint: Search endpoint to search against.
    headers: Headers to search with

  Raises:
    ValueError: Concept type must be either granule or collection.

  Returns:
    requests object: Response from search request.
  """
  if concept_type not in {"collection", "granule"}:
    raise ValueError("Don't know how to search opensearch with concept type: {}".format(concept_type))

  return search(concept_type,
                params=params,
                headers=headers,
                search_endpoint=opensearch_endpoint)

def retrieve(concept_id, revision_id='', token=None, accept=None, search_endpoint=config.search_endpoint):
  """
  Retrieve CMR concept id.

  Params:
    concept_id: Concept ID to retrieve.
    revision_id: Revision ID to retrieve.
    token: Echo token.
    search_endpoint: Search endpoint to search against.

  Returns:
    requests object: Response from search request.
  """
  if revision_id: revision_id = "/" + revision_id
  retrieve_url = "{search_endpoint}/concepts/{concept_id}{revision_id}".format(search_endpoint=search_endpoint,
                                                                               concept_id=concept_id,
                                                                               revision_id=revision_id)
  headers = {"Authorization": token, "Accept": accept}
  return requests.get(retrieve_url, headers=headers)

def read_file(filename):
  """
  Open and read a file into a string.

  Params:
    filename: The file to read.

  Returns:
    string: Read contents of file into string.
  """
  with open(filename) as inf:
    return inf.read()

def clean_resources(resource_map,
                    ingest_endpoint=config.ingest_endpoint,
                    token=config.token,
                    provider=config.provider,
                    access_control_endpoint=config.access_control_endpoint):
  """
  Delete requested resources.

  Params:
    resource_map: A map of concept_type => list(<native_ids>) for collection/variables/services
                  or concept_type => list(<concept_ids>) for acl/group
    ingest_endpoint: The ingest endpoint to delete from.
    token: Echo token.
    provider: The provider to clean.
    access_control_endpoint: Endpoint to cleanup access control resources from.

  Returns:
    int: number of concepts deleted.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  deleted = 0
  for concept_type, org_identifiers in resource_map.items():
    identifiers = org_identifiers.copy()
    deleted += len(identifiers)
    with concurrent.futures.ThreadPoolExecutor() as executor:
      if concept_type in {"collection", "variable", "service"}:
        futures = {executor.submit(delete,
                                   concept_type,
                                   native_id,
                                   token=token,
                                   provider=provider,
                                   ingest_endpoint=ingest_endpoint): native_id for native_id in identifiers}
      else:
        futures = {executor.submit(delete_access_control,
                                   concept_type,
                                   concept_id,
                                   token=token,
                                   access_control_endpoint=access_control_endpoint): concept_id for concept_id in identifiers}
      for future in concurrent.futures.as_completed(futures):
        response = future.result()
        response.raise_for_status()
  return deleted

def merge(*dicts):
  """
  Merge dictionaries.

  Params:
    dicts: Dictionaries to merge.

  Returns:
    dict: merge(dict1, dict2, dict3, ..., dictn)
  """
  d_lists = []
  for d in dicts:
    d_lists += list(d.items())
  return dict(d_lists)

def collection(attribs={}, dump=True):
  """
  Return umm collection with a unique entry title and shortname.

  Params:
    attribs: UMM attributes to merge.
    dump: Dump output as string.

  Returns:
    dict: UMM Collection.
  """
  identifier = generate_native_id()
  template = merge(templates.collection,
                   {"EntryTitle": "_collection_{}".format(identifier),
                    "ShortName": "_collection{}".format(identifier)},
                   attribs)
  return json.dumps(template) if dump else template

def granule(attribs={}, dump=True):
  """
  Return umm granule with unique granuleUR.

  Params:
    attribs: UMM attributes to merge.
    dump: Dump output as string.

  Returns:
    dict: UMM Granule.
  """
  template = merge(templates.granule,
                   {"GranuleUR": "_granule_{}".format(generate_native_id())},
                   attribs)
  return json.dumps(template) if dump else template

def variable(attribs={}, dump=True):
  """
  Return umm variable with a unique Name.

  Params:
    attribs: UMM attributes to merge.
    dump: Dump output as string.

  Returns:
    dict: UMM Variable.
  """
  template = merge(templates.variable,
                   {"Name": "_variable_{}".format(generate_native_id())},
                   attribs)
  return json.dumps(template) if dump else template

def service(attribs={}, dump=True):
  """
  Return umm service.

  Params:
    attribs: UMM attributes to merge.
    dump: Dump output as string.

  Returns:
    dict: UMM Service.
  """
  template = merge(templates.service,
                   {"Name": "_service_{}".format(generate_native_id())},
                   attribs)
  return json.dumps(template) if dump else template

def acl(attribs={}, dump=True):
  """
  Return acl

  Params:
    attribs: ACL attributes to merge.
    dump: Dump output as string.

  Returns:
    dict: ACL
  """
  template = merge(templates.acl,
                   attribs)
  cp_template = template.copy()
  cp_template["catalog_item_identity"]["name"] = "_acl_{}".format(generate_native_id())
  return json.dumps(cp_template) if dump else cp_template

def group(attribs={}, dump=True):
  """
  Return group

  Params:
    attribs: Group attributes to merge.
    dump: Dump output as string.

  Returns:
    dict: Group
  """
  template = merge(templates.group,
                   {"name": "_group_{}".format(generate_native_id())},
                   attribs)
  return json.dumps(template) if dump else template

def token_user_info(attribs={}, dump=True):
  """
  Return token creation info.

  Params:
    attribs: token attributes to merge
    dump: Dump output as string.

  Returns:
    dict: token user info.
  """
  template = merge(templates.token_info, attribs)
  return json.dumps(template) if dump else template

def ingest_concepts(concept_type, num_concepts, token=None, collection_entry_title=None):
  """
  Create num_concepts concepts.

  Params:
    concept_type: Concept type to create.
    num_concepts: Number of concepts to create.
    token: Token to ingest.
    collection_identifier: Used for granule ingest. EntryTitle of collection.

  Raises:
    ValueError: On bad concept_type.
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    list: List of concept_id, revision_id, native_id tuples for created concepts.
  """
  concepts = []
  content_type = "application/vnd.nasa.cmr.umm+json"
  if concept_type == "granule": content_type += ";version=1.4"
  with concurrent.futures.ThreadPoolExecutor() as executor:
    futures = {}
    for _ in range(num_concepts):
      native_id = generate_native_id()
      if concept_type == 'collection':
        concept = collection()
      elif concept_type == 'granule':
        if collection_entry_title is None:
          raise ValueError("Don't know how to create granule without collection_entry_title.")
        concept = granule({"CollectionReference": {"EntryTitle": collection_entry_title}})
      elif concept_type == 'variable':
        concept = variable()
      elif concept_type == 'service':
        concept = service()
      else:
        raise ValueError("Do not know how to create concept_type: {}.".format(concept_type))
      futures[executor.submit(ingest,
                              concept_type,
                              content_type,
                              concept,
                              native_id,
                              token)] = native_id
    for future in concurrent.futures.as_completed(futures):
      native_id = futures[future]
      response = future.result()
      response.raise_for_status()
      ingested_concept = response.json()
      concept_id = ingested_concept["concept-id"]
      revision_id = ingested_concept["revision-id"]
      concepts.append((concept_id, revision_id, native_id))
  return concepts

def wait_for_association(associate,
                         collection_concept_ids,
                         assoc_concept_id,
                         max_poll_wait=config.max_poll_wait,
                         poll_interval=config.poll_interval):
  """
  Wait for association.

  Params:
    associate: True or False. True == Create operation. False == Delete operation.
    assoc_concept_id: The concept ID to search for.
    max_poll_wait: Maximum time to wait for index.
    poll_interval: The interval to poll at.
  """
  timeout = time.time() + max_poll_wait
  params = {"concept-id": collection_concept_ids}
  headers = {"Authorization": config.token,
             "Accept": "application/json"}
  while True:
    search_response = search("collection", params=params, headers=headers)
    search_response.raise_for_status()
    collections = search_response.json()
    num_assoc = 0
    for coll in collections["feed"]["entry"]:
      associations = coll.get("associations", {})
      all_associations = associations.get("services", []) + associations.get("variables", [])
      if assoc_concept_id in all_associations: num_assoc += 1
    if associate and num_assoc == len(collection_concept_ids): break
    elif associate == False and num_assoc == 0: break
    if time.time() > timeout:
      raise TimeoutError("Concept indexing time has exceeded maximum poll wait time.")
    time.sleep(poll_interval)

def perform_association(request_fn,
                        concept_type,
                        assoc_concept_id,
                        collection_concept_ids,
                        associate,
                        search_endpoint=config.search_endpoint,
                        token=None,
                        wait_for_index=True,
                        max_poll_wait=config.max_poll_wait,
                        poll_interval=config.poll_interval):
  """
  Perform association/dissociation to collection for concept_type.

  Params:
    request_fn: requests.post for association, requests.delete for dissociation
    concept_type: Concept type to associate.
    assoc_concept_id: Concept ID of association.
    collection_concept_ids: List of collection concept IDs to associate.
    search_endpoint: The search endpoint.
    associate: Create == True or delete == False.
    token: Echo Token.
    wait_for_index: Wait for the associate to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
   requests: requests object.
  """
  association_url = "{search_endpoint}/{concept_type}s/{assoc_concept_id}/associations".format(search_endpoint=search_endpoint,
                                                                                               concept_type=concept_type,
                                                                                               assoc_concept_id=assoc_concept_id)
  formatted_collection_concept_ids = [{'concept_id': concept_id} for concept_id in collection_concept_ids]
  headers = {"Authorization": token}
  response = request_fn(association_url, headers=headers, data=json.dumps(formatted_collection_concept_ids))
  if wait_for_index:
    wait_for_association(associate, collection_concept_ids, assoc_concept_id)
  return response

def create_association(concept_type,
                       assoc_concept_id,
                       collection_concept_ids,
                       search_endpoint=config.search_endpoint,
                       token=None,
                       wait_for_index=True,
                       max_poll_wait=config.max_poll_wait,
                       poll_interval=config.poll_interval):
  """
  Create association to collection for concept_type.

  Params:
    concept_type: Concept type to associate.
    assoc_concept_id: Concept ID of association.
    collection_concept_ids: List of collection concept IDs to associate.
    search_endpoint: The search endpoint.
    token: Echo Token.
    wait_for_index: Wait for the associate to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
   requests: requests object.
  """
  return perform_association(requests.post,
                             concept_type,
                             assoc_concept_id,
                             collection_concept_ids,
                             True,
                             search_endpoint=search_endpoint,
                             token=token,
                             wait_for_index=wait_for_index,
                             max_poll_wait=max_poll_wait,
                             poll_interval=poll_interval)

def delete_association(concept_type,
                       assoc_concept_id,
                       collection_concept_ids,
                       search_endpoint=config.search_endpoint,
                       token=None,
                       wait_for_index=True,
                       max_poll_wait=config.max_poll_wait,
                       poll_interval=config.poll_interval):
  """
  Delete association to collection for concept_type.

  Params:
    concept_type: Concept type to associate.
    assoc_concept_id: Concept ID of association.
    collection_concept_ids: List of collection concept IDs to associate.
    search_endpoint: The search endpoint.
    token: Echo Token.
    wait_for_index: Wait for the associate to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
   requests: requests object.
  """
  return perform_association(requests.delete,
                             concept_type,
                             assoc_concept_id,
                             collection_concept_ids,
                             False,
                             search_endpoint=search_endpoint,
                             token=token,
                             wait_for_index=wait_for_index,
                             max_poll_wait=max_poll_wait,
                             poll_interval=poll_interval)

def create_tag(tag, search_endpoint=config.search_endpoint, token=None):
  """
  Create a tag.

  Params:
    tag: The tag to create.
    search_endpoint: The search endpoint.
    token: The token.

  Returns:
    request object for tag.
  """
  tag_url = "{}/tags".format(search_endpoint)
  headers = {"Authorization": token,
             "Content-Type": "application/json"}
  return requests.post(tag_url, headers=headers, data=tag)

def retrieve_tag(tag_key, search_endpoint=config.search_endpoint, token=None):
  """
  Retrieve a tag from search.

  Params:
    tag_key: The tag key to retrieve.
    search_endpoint: The search endpoint to hit.
    token: Echo token.

  Returns:
    request object with get request of tag.
  """
  tag_url = "{search_endpoint}/tags/{tag_key}".format(search_endpoint=search_endpoint,
                                                      tag_key=tag_key)
  headers = {"Authorization": token}
  return requests.get(tag_url, headers=headers)


def update_tag(tag_key, tag, search_endpoint=config.search_endpoint, token=None):
  """
  Update a tag.

  Params:
    tag_key: The tag key to retrieve.
    tag: Updated tag.
    search_endpoint: The search endpoint to hit.
    token: Echo token.

  Returns:
    request object with get request of tag.
  """
  tag_url = "{search_endpoint}/tags/{tag_key}".format(search_endpoint=search_endpoint,
                                                      tag_key=tag_key)
  headers = {"Authorization": token,
             "Content-Type": "application/json"}
  return requests.put(tag_url, headers=headers, data=tag)

def delete_tag(tag_key, search_endpoint=config.search_endpoint, token=None):
  """
  Delete a tag.

  Params:
    tag_key: The tag key to retrieve.
    search_endpoint: The search endpoint to hit.
    token: Echo token.

  Returns:
    request object with get request of tag.
  """
  tag_url = "{search_endpoint}/tags/{tag_key}".format(search_endpoint=search_endpoint,
                                                      tag_key=tag_key)
  headers = {"Authorization": token}
  return requests.delete(tag_url, headers=headers)

def associate_tag(tag_key,
                  collection_concept_ids=None,
                  tag_data=None,
                  search_endpoint=config.search_endpoint,
                  token=None):
  """
  Associate a tag to collections.

  Params:
    tag_key: The key of the tag to associate.
    collection_concept_ids: List of collection concept IDs to associate. Optional parameter, will use default data if not provided.
    tag_data: Data to associate as string. This should be of the form [{\"concept_id\": <concept-id>, \"data\": <data>}, ...].
    search_endpoint: The search endpoint.
    token: The echo token.

  Raises:
    TypeError: Need to provider either tag_data or collection_concept_ids.

  Returns:
    request object with tag association information
  """
  tag_url = "{search_endpoint}/tags/{tag_key}/associations".format(search_endpoint=search_endpoint,
                                                                   tag_key=tag_key)
  headers = {"Authorization": token,
             "Content-Type": "application/json"}
  if tag_data:
    data = tag_data
  elif collection_concept_ids:
    data = json.dumps([{"concept_id": concept_id, "data": "TEST_DATA"} for concept_id in collection_concept_ids])
  else:
    raise TypeError("Must provide either a list of collection concept ids or list of tag data.")

  return requests.post(tag_url, data=data, headers=headers)

def dissociate_tag(tag_key,
                   collection_concept_ids,
                   search_endpoint=config.search_endpoint,
                   token=None):
  """
  Dissociate a tag from collections.

  Params:
    tag_key: The key of the tag to associate.
    collection_concept_ids: List of collection concept IDs to associate.
    search_endpoint: The search endpoint.
    token: The echo token.

  Returns:
    request object with tag dissociation information
  """
  tag_url = "{search_endpoint}/tags/{tag_key}/associations".format(search_endpoint=search_endpoint,
                                                                   tag_key=tag_key)
  headers = {"Authorization": token,
             "Content-Type": "application/json"}
  data = json.dumps([{"concept_id": concept_id} for concept_id in collection_concept_ids])
  return requests.delete(tag_url, headers=headers, data=data)

def get_tag_info(tag_key):
  """
  Return tag info, uses default.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    dict: Default tag info.
  """
  headers = {"Authorization": config.token}
  params = {"tag_key": config.tag_key}
  response = search("tag",
                    params=params,
                    headers=headers)
  response.raise_for_status()
  return response.json()["items"][0]

def create_token(token_info, content_type, legacy_services_endpoint=config.legacy_services_endpoint):
  """
  Create a token.

  Params:
    token_info: token xml/json used to create token.
    content_type: Content type of token_info.
    legacy_services_endpoint: Endpoint to create token.
  """
  token_url = "{legacy_services_endpoint}/tokens".format(legacy_services_endpoint=legacy_services_endpoint)
  headers = {"Content-Type": content_type,
             "Accept": "application/json"}
  return requests.post(token_url, data=token_info, headers=headers)

def delete_token(token, legacy_services_endpoint=config.legacy_services_endpoint):
  """
  Delete a token.

  Params:
    token_info: token xml/json used to create token.
    legacy_services_endpoint: Endpoint to create token.
  """
  token_url = "{legacy_services_endpoint}/tokens/{token}".format(legacy_services_endpoint=legacy_services_endpoint,
                                                                 token=token)
  headers = {"Accept": "application/json",
             "Authorization": token}
  return requests.delete(token_url, headers=headers)

def create_default_token():
  """
  Get a default token for quick use

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  response = create_token(token_user_info(), "application/json")
  response.raise_for_status()
  return response.json()["token"]["id"]

def get_token_info(token, legacy_services_endpoint=config.legacy_services_endpoint):
  """
  Returns token info.

  Params:
    token: Token to check.
    legacy_services_endpoint: Endpoint to get token info.
  """
  headers = {"Authorization": token,
             "Accept": "application/json"}
  token_url = "{legacy_services_endpoint}/tokens/{token}/token_info".format(legacy_services_endpoint=legacy_services_endpoint,
                                                                            token=token)
  return requests.get(token_url, headers=headers)

def search_access_control(concept_type,
                          headers=None,
                          params=None,
                          access_control_endpoint=config.access_control_endpoint):
  """
  Search group or ACL for access-control

  Params:
    concept_type: ACL Or Group.
    token: Echo token to use.
    access_control_endpoint: Endpoint to update ACL.

  Returns:
    Requests object search info.
  """
  ac_url = "{access_control_endpoint}/{concept_type}s".format(access_control_endpoint=access_control_endpoint,
                                                              concept_type=concept_type)
  return requests.get(ac_url, headers=headers, params=params)


def create_access_control(concept_type,
                          concept,
                          token=None,
                          access_control_endpoint=config.access_control_endpoint,
                          wait_for_index=True,
                          max_poll_wait=config.max_poll_wait,
                          poll_interval=config.poll_interval):
  """
  Create an ACL or Group.

  Params:
    concept_type: acl or group.
    concept: The acl or group to create.
    token: Echo token to use.
    access_control_endpoint: Endpoint to create ACL or Group.
    wait_for_index: Wait for the concept to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
    Requests object info about ACL or Group.
  """
  ac_url = "{access_control_endpoint}/{concept_type}s".format(access_control_endpoint=access_control_endpoint,
                                                              concept_type=concept_type)
  headers = {"Content-Type": "application/json",
             "Authorization": token}
  response = requests.post(ac_url, headers=headers, data=concept)
  j = response.json()
  try:
    response.raise_for_status()
    with INGEST_LOCK:
      CREATED_RESOURCES[concept_type].add(j["concept_id"])
  except requests.exceptions.HTTPError:
    return response
  if wait_for_index:
    wait_for_concept_index(True,
                           search_access_control,
                           j["concept_id"],
                           j["revision_id"],
                           concept_type,
                           max_poll_wait,
                           poll_interval)
  return response

def delete_access_control(concept_type,
                          concept_id,
                          token=None,
                          access_control_endpoint=config.access_control_endpoint,
                          wait_for_index=False,
                          max_poll_wait=config.max_poll_wait,
                          poll_interval=config.poll_interval):
  """
  Delete an ACL or Group.

  Params:
    concept_type: ACL or Group.
    concept_id: ACL or Group concept id to delete.
    token: Echo token to use.
    access_control_endpoint: Endpoint to delete ACL or Group.
    wait_for_index: Wait for the concept to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
    Requests object info about ACL or Group.
  """
  ac_url = "{access_control_endpoint}/{concept_type}s/{concept_id}".format(access_control_endpoint=access_control_endpoint,
                                                                           concept_type=concept_type,
                                                                           concept_id=concept_id)
  headers = {"Authorization": token}
  response = requests.delete(ac_url, headers=headers)
  j = response.json()
  concept_id_key = 'concept-id' if concept_type == 'acl' else 'concept_id'
  revision_id_key = 'revision-id' if concept_type == 'acl' else 'revision_id'
  try:
    response.raise_for_status()
    with INGEST_LOCK:
      CREATED_RESOURCES[concept_type].remove(j[concept_id_key])
  except requests.exceptions.HTTPError:
    return response
  if wait_for_index:
    wait_for_concept_index(False,
                           search_access_control,
                           j[concept_id_key],
                           j[revision_id_key],
                           concept_type,
                           max_poll_wait,
                           poll_interval)
  return response

def update_access_control(concept_type,
                          updated_concept,
                          concept_id,
                          token=None,
                          access_control_endpoint=config.access_control_endpoint,
                          wait_for_index=False,
                          max_poll_wait=config.max_poll_wait,
                          poll_interval=config.poll_interval):
  """
  Update an ACL or Group.

  Params:
    concept_type: acl or group
    updated_concept: The acl or group to update.
    concept_id: ACL concept id to update.
    token: Echo token to use.
    access_control_endpoint: Endpoint to update ACL.
    wait_for_index: Wait for the concept to be indexed.
    max_poll_wait: Maximum wait time for indexing event.
    poll_interval: Interval to poll indexing at.

  Returns:
    Requests object info about ACL.
  """
  ac_url = "{access_control_endpoint}/{concept_type}s/{concept_id}".format(access_control_endpoint=access_control_endpoint,
                                                                           concept_type=concept_type,
                                                                           concept_id=concept_id)
  headers = {"Content-Type": "application/json",
             "Authorization": token}
  response = requests.put(ac_url, data=updated_concept, headers=headers)
  j = response.json()
  if wait_for_index:
    wait_for_concept_index(False,
                           search_access_control,
                           j["concept_id"],
                           j["revision_id"],
                           concept_type,
                           max_poll_wait,
                           poll_interval)
  return response


def retrieve_access_control(concept_type,
                            concept_id,
                            token=None,
                            access_control_endpoint=config.access_control_endpoint):
  """
  Retrieve group or ACL for access-control

  Params:
    concept_type: ACL or Group.
    concept_id: Concept ID to retrieve.
    token: Echo token to use.
    access_control_endpoint: Endpoint to update ACL.

  Returns:
    Requests object retrieve info.
  """
  ac_url = "{access_control_endpoint}/{concept_type}s/{concept_id}".format(access_control_endpoint=access_control_endpoint,
                                                                           concept_type=concept_type,
                                                                           concept_id=concept_id)
  headers = {"Authorization": token}
  return requests.get(ac_url, headers=headers)

def get_health(endpoint):
  """
  Get health for endpoint.

  Params:
    endpoint: Endpoint to get health.

  Returns:
    Request object with health.
  """
  return requests.get("{}/health".format(endpoint))

def reindex_collection_permitted_groups(ingest_endpoint=config.ingest_endpoint,
                                        token=None):
  """
  Reindex collection permitted groups.

  Params:
    ingest_endpoint: The ingest endpoint to send job to.
    token: Echo Token to use.

  Returns:
    Requests object information.
  """
  ingest_url = "{}/jobs/reindex-collection-permitted-groups".format(ingest_endpoint)
  headers = {"Authorization": token}
  return requests.post(ingest_url, headers=headers)

def query_service_bridge(service_bridge_service,
                         collection_concept_id,
                         params=None,
                         headers=None,
                         service_bridge_endpoint=config.service_bridge_endpoint):
  """
  Query a service bridge service.

  Parms:
    service_bridge_service: ous or size-estimate.
    collection_concept_id: The collection to query.
    params: Request params.
    headers: Request headers.
    service_bridge_endpoint: The endpoint to query.

  Raises:
    ValueError: service must be ous or size-estimate.

  Returns:
    Service-bridge request object.
  """
  if service_bridge_service not in {"ous", "size-estimate"}:
    raise ValueError("Don't know how to query service {} from service-bridge".format(service_bridge_service))

  service_bridge_url = "{service_bridge_endpoint}/{service_bridge_service}/collection/{collection_concept_id}".format(service_bridge_endpoint=service_bridge_endpoint,
                                                                                                                      service_bridge_service=service_bridge_service,
                                                                                                                      collection_concept_id=collection_concept_id)
  return requests.get(service_bridge_url, params=params, headers=headers)
