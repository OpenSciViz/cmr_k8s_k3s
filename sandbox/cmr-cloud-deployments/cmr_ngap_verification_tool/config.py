"""
Testing configuration
"""
import os
import json
import uuid

class config(object):

  def __init__(self):
    with open(os.path.join(os.path.dirname(__file__), 'resources/config.json')) as config_json:
      self.config = json.load(config_json)
      self.config["default_native_id"] = "test_cmr_ngap_{}".format(uuid.uuid4().hex)

  def __getattr__(self, name):
    try:
      return self.config[name]
    except KeyError:
      raise KeyError("Unable to find key {}. Please ensure this key is configured in resources/config.json".format(name))

config = config()
