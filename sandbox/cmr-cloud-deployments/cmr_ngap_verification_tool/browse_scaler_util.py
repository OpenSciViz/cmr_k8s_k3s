"""
Browse scaler utility functions.
"""
import requests
import io
from config import config
from PIL import Image, ImageChops
import os

def retrieve_browse_image(concept_type,
                          concept_id,
                          height=None,
                          width=None,
                          browse_scaler_endpoint=config.browse_scaler_endpoint):
  """
  Retrieve browse image from browse scaler

  Params:
    concept_type: The concept type. 'collection' or 'granule'.
    concept_id: The concept ID to retrieve.
    height (optional): The height to resize as.
    width (optional): The width to resize as.
    browse_scaler_endpoint (optional): The endpoint of browse scaler to use.

  Returns:
    requests.Response: Response from browse scaler.
  """
  params = {}
  browse_concept_type = 'dataset' if concept_type == 'collection' else concept_type
  browse_url = "{}/browse_images/{}s/{}".format(browse_scaler_endpoint,
                                                browse_concept_type,
                                                concept_id)
  if height:
    params["h"] = height
  if width:
    params["w"] = width

  return requests.get(browse_url, params=params)

def parse_browse_image_from_concept(concept):
  """
  Retrieve the browse image from a concept.

  Params:
    concept: The entire concept to parse.

  Returns:
    string: URL of the browse image.
  """
  for related_url in concept.get("RelatedUrls", []):
    if related_url.get("Type", "") == "GET RELATED VISUALIZATION":
      return related_url["URL"]

def get_image_aspect(image_url=None,
                     raw_image=None):
  """
  Get width and height from image at url.

  Params:
    image_url: URL of image to retrieve.
    raw_image: The raw image to use.

  Returns:
    tuple(int, int): Width, Height of image.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
    ValueError: Need to supply either image_url or raw_image.
  """
  if image_url:
    image_response = requests.get(image_url)
    image_response.raise_for_status()
    image_content = image_response.content
  elif raw_image:
    image_content = raw_image
  else:
    raise ValueError("Must supply either an image url or the raw image.")
  with io.BytesIO(image_content) as bytes_io:
    with Image.open(bytes_io) as image:
      width, height = image.size
      return width, height

def compare_with_not_found(image_content):
  """
  Compare image_content with not_found image.

  Params:
    image_content: Image to compare.

  Returns:
    Minimal rectangle that contains all the changes between two images.
  """
  with Image.open(os.path.join(os.path.dirname(__file__), 'resources/image_unavailable.png')) as not_found:
    with io.BytesIO(image_content) as bytes_io:
      with Image.open(bytes_io) as image:
        return ImageChops.difference(image, not_found).getbbox()
