"""
Common assertions
"""
from config import config
import json
import utils
import requests

def assert_all_health(health_check):
  """
  Assert all health values are true from recursive dict.

  Params:
    health_check: Health check values.

  Returns:
    Nothing: None.
  """
  for k, v in health_check.items():
    if k == 'ok?': assert v == True
    elif isinstance(v, dict):
      assert_all_health(v)

def assert_retrieval(concept_id,
                     revision_id='',
                     token=None,
                     content_type=None,
                     assert_not_found=False):
  """
  Basic search retrieval assertions

  Params:
    concept_id: Concept ID.
    revision_id: Concept revision id.
    token: Echo token.
    content_type: Content Type metadata is natively in.
    assert_not_found: Change expected status code to 404.

  Returns:
    requests-object: Response object.

  Asserts:
    Status is 200
    Content-Type is type returned (if provided)
  """
  response = utils.retrieve(concept_id, str(revision_id), token=token)
  if assert_not_found:
    assert 404 == response.status_code
  else:
    assert 200 == response.status_code
  if content_type:
    assert content_type in response.headers["Content-Type"]
  return response

def assert_search(concept_type, concept_id, token=None):
  """
  Basic search assertions for a single concept ID.

  Params:
    concept_type: Concept type.
    concept_id: Concept ID.
    token: Echo token.

  Returns:
    dict: Parsed JSON response.

  Asserts:
    Status is 200
    Only one hit for the concept id
    Concept ID returned is concept ID searched against
  """
  params = {"concept_id": concept_id}
  headers = {"Authorization": token,
             "Accept": "application/json"}
  response = utils.search(concept_type, params=params, headers=headers)
  assert 200 == response.status_code
  assert 1 == int(response.headers["CMR-Hits"])
  parsed_response = response.json()
  if concept_type in {"collection", "granule"}:
    parsed_concept_id = parsed_response["feed"]["entry"][0]["id"]
  else:
    parsed_concept_id = parsed_response["items"][0]["concept_id"]
  assert concept_id == parsed_concept_id
  return parsed_response

def assert_site_routes(site_endpoint,
                       routes):
  """
  Assert able to visit routes for site_endpoint.

  Params:
    site_endpoint: Endpoint to test. ex. https://cmr.sit.earthdata.nasa.gov/search
    routes: Routes to check.

  Returns:
    None: Void.
  """
  for route in routes:
    full_route = "{}{}".format(site_endpoint,
                               route)
    r = requests.get(full_route, allow_redirects=True)
    if r.status_code != 200:
      print("Failure at route: {}".format(full_route))
    assert r.status_code == 200
