"""
Basic opensearch assertions

Asserts:
  Assert opensearch search matches what is in CMR for granule and collection
  Opensearch health is OK
"""
from assertions import assert_all_health
from config import config
import utils
import re
from bs4 import BeautifulSoup

###############################################################################
#                       General Opensearch Assertions                         #
###############################################################################

def assert_opensearch_search(concept_type, cmr_params, opensearch_params=None):
  """
  Test opensearch by comparing with cmr results.

  Params:
    concept_type: Type of concept to type.
    cmr_params: CMR Params to send.
    opensearch_params: Opensearch search params.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  # Get concept from opensearch
  opensearch_response = utils.search_opensearch(concept_type,
                                                params=opensearch_params)
  opensearch_response.raise_for_status()
  assert 200 == opensearch_response.status_code

  headers = {"Accept": "application/atom+xml"}
  cmr_response = utils.search(concept_type,
                              params=cmr_params,
                              headers=headers)
  cmr_response.raise_for_status()
  cmr_ids = {entry.find("id").string for entry in BeautifulSoup(cmr_response.text, "xml").findAll("entry")}
  opensearch_ids = {entry.find("dc:identifier").string for entry in BeautifulSoup(opensearch_response.text, "xml").findAll("entry")}
  assert cmr_ids == opensearch_ids

###############################################################################
#                             Health Test                                     #
###############################################################################

def test_opensearch_health():
  """
  Assert opensearch health.
  """
  health = utils.get_health(config.opensearch_endpoint)
  assert 200 == health.status_code
  assert_all_health(health.json())

###############################################################################
#                             Collection Test                                 #
###############################################################################

def test_opensearch_collection():
  """
  Test opensearch returns collection. Tests by comparing with CMR.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  # Default opensearch parameters that are sent to CMR.
  cmr_params = {"include_has_granules": "true",
                "include_tags": "org.ceos.wgiss.cwic.*,opensearch.granule.osdd,org.geoss.geoss_data-core,gov.nasa.eosdis",
                "page_num": 1,
                "page_size": 10,
                "pretty": "true"}
  assert_opensearch_search("collection", cmr_params)

###############################################################################
#                             Granule Tests                                   #
###############################################################################

def test_opensearch_granule():
  """
  Test opensearch returns collection. Tests by comparing with CMR.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  search_shortname = "MOD10A1"
  # Default opensearch parameters that are sent to CMR.
  cmr_params = {"page_num": 1,
                "page_size": 10,
                "short_name": search_shortname}
  opensearch_params = {"shortName": search_shortname}
  assert_opensearch_search("granule", cmr_params, opensearch_params)
