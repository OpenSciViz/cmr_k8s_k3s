"""
Basic service bridge assertions

Asserts:
  OUS service returns appropriate URL
  Size estimate returns appropriate resource
  SES Health is OK
"""
from assertions import assert_all_health
from config import config
import utils

###############################################################################
#                             Health Test                                     #
###############################################################################

def test_service_bridge_health():
  """
  Assert service bridge health
  """
  health = utils.get_health(config.service_bridge_endpoint)
  assert 200 == health.status_code
  assert_all_health(health.json())

###############################################################################
#                             OUS Test                                        #
###############################################################################

def test_service_bridge_ous(default_granule):
  """
  Assert ous resource is working.
  """
  collection_concept_id, granule_concept_id = default_granule
  headers = {"Authorization": config.token}
  params = {"granules": granule_concept_id}
  response = utils.query_service_bridge("ous",
                                        collection_concept_id,
                                        params=params,
                                        headers=headers)
  response.raise_for_status()
  assert 200 == response.status_code
  assert 1 == int(response.headers["Cmr-Hits"])
  # The default_granule has an opendap url
  assert "https://f5eil01.edn.ecs.nasa.gov/opendap/DEV01/FS2/AIRS/AIRX3STD.006/2016.01.20/AIRS.2016.01.20.L3.RetStd001.v6.0.31.0.G16027144006.hdf.nc" == response.json()["items"][0]

###############################################################################
#                             SES Test                                        #
###############################################################################

def test_service_bridge_size_estimate(default_granule):
  """
  Assert able to query SES
  """
  collection_concept_id, granule_concept_id = default_granule
  variable_concept_id = utils.ingest_concepts("variable",
                                              1,
                                              token=config.token)[0][0]
  association_response = utils.create_association("variable",
                                                  variable_concept_id,
                                                  [collection_concept_id],
                                                  token=config.token)
  association_response.raise_for_status()
  params = {"granules": granule_concept_id,
            "variables": variable_concept_id}
  headers = {"Authorization": config.token}
  service_bridge_response = utils.query_service_bridge("size-estimate",
                                                       collection_concept_id,
                                                       params=params,
                                                       headers=headers)
  service_bridge_response.raise_for_status()
  assert 200 == service_bridge_response.status_code
  assert 1 == int(service_bridge_response.headers["Cmr-Hits"])
  assert 1 == service_bridge_response.json()["hits"]
