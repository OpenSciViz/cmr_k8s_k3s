"""
Common fixtures and teardown fns.
"""
from config import config
import json
import pytest
import utils

def pytest_sessionfinish(session, exitstatus):
  """
  Clean created resources.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  deleted = utils.clean_resources(utils.CREATED_RESOURCES)

@pytest.fixture
def default_collection():
  """
  Create a default collection into a provider.

  Params:
    grant_guest: Grant guest read access to collection.

  Yields:
    string: Concept-id of collection.
    dict: The collection ingested.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  collection = utils.collection(dump=False)
  collection_response = utils.ingest("collection",
                                     "application/vnd.nasa.cmr.umm+json",
                                     json.dumps(collection),
                                     token=config.token)
  collection_response.raise_for_status()
  collection_json = collection_response.json()

  yield collection_json["concept-id"], collection

@pytest.fixture
def default_granule(default_collection):
  """
  Create a default collection and granule into a provider.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  collection_concept_id, collection = default_collection
  granule = utils.granule({"CollectionReference": {"EntryTitle": collection["EntryTitle"]}})
  granule_response = utils.ingest("granule",
                                  "application/vnd.nasa.cmr.umm+json;version=1.4",
                                  granule,
                                  token=config.token)
  granule_response.raise_for_status()
  granule_concept_id = granule_response.json()["concept-id"]
  yield collection_concept_id, granule_concept_id

@pytest.fixture
def default_granule_with_concept(default_collection):
  """
  Create a default granule and return the concept id and the concept itself.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  _, collection = default_collection
  granule = utils.granule({"CollectionReference": {"EntryTitle": collection["EntryTitle"]}},
                          dump=False)
  granule_response = utils.ingest("granule",
                                  "application/vnd.nasa.cmr.umm+json;version=1.4",
                                  json.dumps(granule),
                                  token=config.token)
  granule_response.raise_for_status()
  granule_concept_id = granule_response.json()["concept-id"]
  yield granule, granule_concept_id

@pytest.fixture
def default_acl():
  """
  Create an ACL for default provider to test with.

  Yields on tuple of (the acl create response, the acl used to create)

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  acl = utils.acl(dump=False)
  acl_response = utils.create_access_control("acl",
                                             json.dumps(acl),
                                             token=config.token)
  acl_response.raise_for_status()
  yield acl_response, acl
  # Cleanup ACL
  acl_concept_id = acl_response.json()["concept_id"]
  acl_response = utils.delete_access_control("acl", acl_concept_id, token=config.token)
  acl_response.raise_for_status()

@pytest.fixture
def default_group():
  """
  Create an Group for default provider to test with.

  Yields on tuple of (Group concept id, the Group create response)

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  group = utils.group()
  group_response = utils.create_access_control("group",
                                               group,
                                               token=config.token)
  group_response.raise_for_status()
  yield group_response
  # Cleanup Group
  group_concept_id = group_response.json()["concept_id"]
  group_response = utils.delete_access_control("group", group_concept_id, token=config.token)
  group_response.raise_for_status()


###############################################################################
#                   Configuration for pytest markers                          #
###############################################################################
def pytest_addoption(parser):
    parser.addoption("--allow-reindex",
                     action="store_true",
                     default=False,
                     help="Allow lengthy tests with reindexing to be run.")

def pytest_configure(config):
  config.addinivalue_line("markers", "reindex: mark test as reindexing required.")

def pytest_collection_modifyitems(config, items):
    if config.getoption("--allow-reindex"):
        # --allow-reindex given in cli: do not skip reindexing jobs.
        return
    skip_reindex = pytest.mark.skip(reason="need --allow-reindex option to run.")
    for item in items:
      if "reindex" in item.keywords:
          item.add_marker(skip_reindex)
