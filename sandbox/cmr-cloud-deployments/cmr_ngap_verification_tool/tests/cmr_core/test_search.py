"""
Basic search-app assertions

Asserts:
  Concept Retrieval
  Concept Search
  Concept Association
  Collection Renderer renders collection html
  Granule search by collection concept ID
  Search health is OK
"""
from assertions import *
from config import config
import json
import utils

###############################################################################
#                            General Search Assertions                        #
###############################################################################

def concept_search_assertions(concept_type, collection_entry_title=None):
  """
  Test concept search.

  Asserts:
    Able to search concept by concept ID.
    Able to search for a group of concepts.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  num_concepts = 30
  content_type = "application/vnd.nasa.cmr.umm+json"
  concepts = utils.ingest_concepts(concept_type,
                                   num_concepts,
                                   token=config.token,
                                   collection_entry_title=collection_entry_title)
  ingested_concept_ids = [concept_id for (concept_id, _, _) in concepts]

  for concept_id in ingested_concept_ids:
    assert_search(concept_type, concept_id, token=config.token)

  params = {"page_size": num_concepts,
            "concept_id": ingested_concept_ids}
  headers = {"Authorization": config.token,
             "Accept": "application/json"}
  response = utils.search(concept_type, params=params, headers=headers)
  response.raise_for_status()
  assert num_concepts == int(response.headers["CMR-Hits"])

  search_response = response.json()
  if concept_type in {"collection", "granule"}:
    concept_ids = [entry["id"] for entry in search_response["feed"]["entry"]]
  else:
    concept_ids = [item["concept_id"] for item in search_response["items"]]

  assert set(concept_ids) == {concept[0] for concept in concepts}

def concept_retrieval_assertions(concept_type, collection_entry_title=None):
  """
  Test concept retrieval.

  Asserts:
    Able to retrieve concept by concept ID.
    Able to retrieve concept by concept ID and revision ID.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  # Creating 3 revisions of 10 concepts
  num_revisions = 2
  num_concepts = 2
  content_type = "application/vnd.nasa.cmr.umm+json"
  concepts = []
  for revisions in range(num_revisions):
    concepts = utils.ingest_concepts(concept_type,
                                     num_concepts,
                                     token=config.token,
                                     collection_entry_title=collection_entry_title)

  for (concept_id, revision_id, _) in concepts:
    # Assert able to retrieve with no revision id
    assert_retrieval(concept_id, token=config.token, content_type=content_type)

    # Assert able to retrieve by revision id
    assert_retrieval(concept_id, str(revision_id), token=config.token, content_type=content_type)

      # Assert max revision + 1 is not found
    assert_retrieval(concept_id, str(revision_id + 1), token=config.token, assert_not_found=True)

def assert_concept_association(association_type, concept_type, assoc_concept_id, collection_concept_ids):
  """
  Test concept associations or dissociation with concept.

  Params:
    association_type: create or delete
    concept_type: Association concept type.
    assoc_concept_id: Concept ID to create association.
    collection_concept_ids: Collection concept IDs to associate.

  Asserts:
    Associations created is equal to collections provided.
    Association exists in collection.

  Raises:
    ValueError: On bad concept_type.
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    list: List of associations.
  """
  if association_type == 'create':
    association_fn = utils.create_association
  elif association_type == 'delete':
    association_fn = utils.delete_association
  else:
    raise ValueError("Do not know how to test association type: {}".format(association_type))

  response = association_fn(concept_type,
                            assoc_concept_id,
                            collection_concept_ids,
                            token=config.token)
  response.raise_for_status()
  associations = response.json()
  assert len(collection_concept_ids) == len(associations)

  association_key = "{}_association".format(concept_type)
  associated = set()

  for association in associations:
    concept_id = association[association_key]["concept_id"]
    associated_item = association["associated_item"]["concept_id"]
    associated.add(associated_item)

  assert associated == set(collection_concept_ids)



  params = {"concept_id": collection_concept_ids,
            "page_size": len(collection_concept_ids)}
  headers = {"Authorization": config.token,
             "Accept": "application/json"}
  response = utils.search("collection",
                          params=params,
                          headers=headers)
  response.raise_for_status()
  collections = response.json()
  if association_type == 'create':
    test_fn = lambda x, y: x in y
  elif association_type == 'delete':
    test_fn = lambda x, y: x not in y
  else:
    raise ValueError("Do not know how to test association type: {}".format(association_type))

  for entry in collections["feed"]["entry"]:
    assert test_fn(assoc_concept_id, entry.get("associations", {}).get("{}s".format(concept_type), []))

  return associations

def assert_association(concept_type, assoc_concept_id, collection_concept_ids):
  """
  Test concept associations and dissociation with concept.

  Params:
    association_type: create or delete
    concept_type: Association concept type.
    assoc_concept_id: Concept ID to create association.
    collection_concept_ids: Collection concept IDs to associate.

  Asserts:
    Associations created is equal to collections provided.
    Association exists in collection.

  Raises:
    ValueError: On bad concept_type.
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    list: List of associations.
  """
  associations = assert_concept_association("create",
                                            concept_type,
                                            assoc_concept_id,
                                            collection_concept_ids)
  assert_concept_association("delete",
                             concept_type,
                             assoc_concept_id,
                             collection_concept_ids)

###############################################################################
#                             Health Test                                     #
###############################################################################

def test_search_health():
  """
  Assert search health.
  """

  health = utils.get_health(config.search_endpoint)
  assert 200 == health.status_code
  assert_all_health(health.json())

###############################################################################
#                             Site Test                                       #
###############################################################################

def test_search_site_routes():
  """
  Test search routes.
  """
  site_routes = ["/",
                 "/sitemap.xml",
                 "/site/sitemap.xml",
                 "/site/collections/directory",
                 "/site/collections/directory/eosdis",
                 "/site/docs/search",
                 "/site/docs/search/api",
                 "/site/docs/search/site",
                 "/site/search_api_docs.html",
                 "/site/search_site_docs.html",
                 "/site/docs/search/api",
                 "/site/docs/search/site"]
  assert_site_routes(config.search_endpoint,
                     site_routes)

###############################################################################
#                             Collection Tests                                #
###############################################################################

def test_collection_retrieval():
  """
  Test collection retrieval.

  Asserts:
    Able to retrieve collection by concept ID.
    Able to retrieve collection by concept ID and revision ID.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  concept_retrieval_assertions("collection")

def test_collection_search():
  """
  Test collection search.

  Asserts:
    Able to search collection by concept ID.
    Able to search for a group of collections.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  concept_search_assertions("collection")

def test_collection_renderer():
  """
  Test able to render collection.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  collection = utils.collection({"EntryTitle": "test_cmr_ngap_collection_renderer_{}".format(utils.generate_native_id())})
  response = utils.ingest("collection",
                          "application/vnd.nasa.cmr.umm+json",
                          collection,
                          token=config.token)
  response.raise_for_status()

  ingest_collection = response.json()
  concept_id = ingest_collection["concept-id"]
  response = utils.retrieve(concept_id, token=config.token, accept="text/html")
  response.raise_for_status()

  assert 200 == response.status_code
  assert "text/html" in response.headers["Content-Type"]
  assert "test_cmr_ngap_collection_renderer" in response.text

###############################################################################
#                             Granule Tests                                   #
###############################################################################

def test_granule_retrieval(default_collection):
  """
  Test granule retrieval.

  Asserts:
    Able to retrieve granule by concept ID.
    Able to retrieve granule by concept ID and revision ID.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  _, collection = default_collection
  concept_retrieval_assertions("granule", collection_entry_title=collection["EntryTitle"])

def test_granule_search(default_collection):
  """
  Test granule search.

  Asserts:
    Able to search granule by concept ID.
    Able to search for a group of granules.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  _, collection = default_collection
  concept_search_assertions("granule", collection_entry_title=collection["EntryTitle"])

def test_granule_search_by_collection():
  """
  Test granule search by collection concept ID.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  collections = []
  granules = []
  for i in range(1, 3):
    collection = utils.collection(dump=False)
    collection_response = utils.ingest("collection",
                                       "application/vnd.nasa.cmr.umm+json",
                                       json.dumps(collection),
                                       token=config.token)
    collection_response.raise_for_status()
    collection_concept_id = collection_response.json()["concept-id"]
    collections.append(collection_concept_id)
    num_granules = i * 5
    granule_tuples = utils.ingest_concepts("granule",
                                           num_granules,
                                           config.token,
                                           collection["EntryTitle"])
    granules.append([x[0] for x in granule_tuples])


  headers = {"Authorization": config.token,
             "Accept": "application/json"}
  total_granules = sum([len(x) for x in granules])
  params = {"page_size": total_granules}

  # Test for each collection
  for i, collection_concept_id in enumerate(collections):
    params["collection_concept_id"] = collection_concept_id
    granule_response = utils.search("granule", params=params, headers=headers)
    granule_response.raise_for_status()
    assert len(granules[i]) == int(granule_response.headers["CMR-Hits"])
    assert all([entry['id'] in granules[i] for entry in granule_response.json()["feed"]["entry"]])

  # Test for both collection_response
  params["collection_concept_id"] = collections
  granule_response = utils.search("granule", params=params, headers=headers)
  granule_response.raise_for_status()
  flattened_granules = [granule for sublist in granules for granule in sublist]
  assert total_granules == int(granule_response.headers["CMR-Hits"])
  assert all([entry['id'] in  flattened_granules for entry in granule_response.json()["feed"]["entry"]])

###############################################################################
#                             Variable Tests                                  #
###############################################################################

def test_variable_retrieval():
  """
  Test variable retrieval.

  Asserts:
    Able to retrieve variable by concept ID.
    Able to retrieve variable by concept ID and revision ID.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  concept_retrieval_assertions("variable")

def test_variable_search():
  """
  Test variable search.

  Asserts:
    Able to search variable by concept ID.
    Able to search for a group of variables.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  concept_search_assertions("variable")

def test_variable_association():
  """
  Test variable association and disassociation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  # Create collections
  collections = utils.ingest_concepts("collection", 1, token=config.token)
  collection_concept_ids = [concept_id for (concept_id, _, _) in collections]

  # Create variables
  variables = utils.ingest_concepts("variable", 1, token=config.token)

  for (variable_concept_id, _, _) in variables:
    associations = assert_association("variable",
                                      variable_concept_id,
                                      collection_concept_ids)

###############################################################################
#                             Service Tests                                   #
###############################################################################

def test_service_retrieval():
  """
  Test service retrieval.

  Asserts:
    Able to retrieve service by concept ID.
    Able to retrieve service by concept ID and revision ID.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  concept_retrieval_assertions("service")

def test_service_search():
  """
  Test service search.

  Asserts:
    Able to search service by concept ID.
    Able to search for a group of services.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Void: None.
  """
  concept_search_assertions("service")

def test_service_association():
  """
  Test service association and disassociation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  # Create collections
  collections = utils.ingest_concepts("collection", 1, token=config.token)
  collection_concept_ids = [concept_id for (concept_id, _, _) in collections]

  # Create services
  services = utils.ingest_concepts("service", 1, token=config.token)

  for (service_concept_id, _, _) in services:
    associations = assert_association("service",
                                      service_concept_id,
                                      collection_concept_ids)
