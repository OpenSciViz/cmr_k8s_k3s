"""
Basic ingest-app assertions

Asserts:
  Concept Create
  Concept Update
  Concept Delete
  Ingest health is OK
"""
from assertions import *
from config import config
import json
import utils

###############################################################################
#                            General Ingest Assertions                        #
###############################################################################

def basic_create_assertions(concept_type, content_type, umm_data):
  """
  Test basic creation assertions for a concept.

  Asserts:
    Concept retrieval, search, create

  Params:
    concept_type: The concept type.
    content_type: The content type, assumed to be umm_json.
    umm_data: The data in umm represented as a dict.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    dict: The ingested concept return text.
  """
  response = utils.ingest(concept_type,
                          content_type,
                          json.dumps(umm_data),
                          token=config.token)
  response.raise_for_status()

  ingested_concept = response.json()
  concept_id = ingested_concept["concept-id"]



  # Assert able to retrieve concept from concept search (mdb)
  response = assert_retrieval(concept_id, token=config.token, content_type=content_type)

  # Assert concept umm is what was retrieved
  assert umm_data == response.json()

  # Assert able to retrieve concept from search (elastic)
  assert_search(concept_type, concept_id, token=config.token)

  return ingested_concept

def basic_update_assertions(concept_type, content_type, umm_data, updated_umm_data):
  """
  Test basic update assertions.

  Asserts:
    Concept retrieval with revision id, ingest update and delete

  Params:
    concept_type: The concept_type
    content_type: The content type, assumed to be umm_json.
    umm_data: original umm data to ingest.
    updated_umm_data: umm data to update and test with.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    dict: Update response information.
  """
  native_id = utils.generate_native_id()
  response = utils.ingest(concept_type,
                          content_type,
                          umm_data,
                          native_id=native_id,
                          token=config.token)
  response.raise_for_status()

  initial_concept = response.json()
  initial_concept_id = initial_concept["concept-id"]
  initial_revision_id = int(initial_concept["revision-id"])



  response = utils.ingest(concept_type,
                          content_type,
                          updated_umm_data,
                          native_id=native_id,
                          token=config.token)
  response.raise_for_status()

  updated_concept = response.json()
  updated_concept_id = updated_concept["concept-id"]
  updated_revision_id = int(updated_concept["revision-id"])

  assert initial_concept_id == updated_concept_id
  assert initial_revision_id + 1 == updated_revision_id



  # Do retrieval assertions with revision ID
  response = assert_retrieval(initial_concept_id,
                              revision_id=updated_revision_id,
                              token=config.token,
                              content_type=content_type)

  assert json.loads(updated_umm_data) == response.json()

  return updated_concept


def basic_delete_assertions(concept_type, content_type, umm_data):
  """
  Test basic delete assertions for a concept.

  Asserts:
    Concept is deleted.
    Revision is appropriatly incremented.

  Params:
    concept_type: The concept type.
    content_type: Content type of collection, assumed to be umm.
    umm_data: collection to ingest.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    dict: Deleted concept json info.
  """
  native_id = utils.generate_native_id()
  response = utils.ingest(concept_type,
                          content_type,
                          umm_data,
                          native_id=native_id,
                          token=config.token)
  response.raise_for_status()
  ingested_concept = response.json()
  concept_id = ingested_concept["concept-id"]
  revision_id = ingested_concept["revision-id"]

  response = utils.delete(concept_type,
                          native_id,
                          token=config.token,
                          wait_for_index=True)
  response.raise_for_status()
  deleted_concept = response.json()

  assert concept_id == deleted_concept["concept-id"]
  assert revision_id + 1 == int(deleted_concept["revision-id"])

  response = utils.retrieve(concept_id, token=config.token)
  assert 404 == response.status_code
  assert ("Concept with concept-id [{concept_id}] could not be found.".format(concept_id=concept_id) in response.text)

  params = {"concept_id": concept_id}
  headers = {"Authorization": config.token}
  response = utils.search(concept_type, params=params, headers=headers)
  response.raise_for_status()
  assert 0 == int(response.headers["CMR-Hits"])

  return deleted_concept

###############################################################################
#                             Health Test                                     #
###############################################################################

def test_ingest_health():
  """
  Assert ingest health.
  """
  health = utils.get_health(config.ingest_endpoint)
  assert 200 == health.status_code
  assert_all_health(health.json())

###############################################################################
#                             Site Test                                       #
###############################################################################

def test_ingest_site_routes():
  """
  Test ingest routes.
  """
  site_routes = ["/",
                 "/site/docs/ingest",
                 "/site/docs/ingest/api",
                 "/site/docs/ingest/site",
                 "/site/ingest_api_docs.html",
                 "/site/docs/ingest/api",
                 "/site/docs/ingest/site"]
  assert_site_routes(config.ingest_endpoint,
                     site_routes)

###############################################################################
#                             Collection Tests                                #
###############################################################################

def test_collection_create():
  """
  Test collection creation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  collection = utils.collection(dump=False)
  basic_create_assertions("collection",
                          "application/vnd.nasa.cmr.umm+json",
                          collection)


def test_collection_update():
  """
  Test collection update.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  collection = utils.collection()
  # New EntryTitle and ShortName will be generated
  updated_collection = utils.collection()
  basic_update_assertions("collection",
                          "application/vnd.nasa.cmr.umm+json",
                          collection,
                          updated_collection)

def test_collection_delete():
  """
  Test collection delete.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  collection = utils.collection()
  basic_delete_assertions("collection",
                          "application/vnd.nasa.cmr.umm+json",
                          collection)

###############################################################################
#                             Granule Tests                                   #
###############################################################################

def test_granule_create(default_collection):
  """
  Test granule creation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  _, collection = default_collection
  granule = utils.granule({"CollectionReference": {"EntryTitle": collection["EntryTitle"]}},
                          dump=False)
  basic_create_assertions("granule",
                          "application/vnd.nasa.cmr.umm+json;version=1.4",
                          granule)

def test_granule_update(default_collection):
  """
  Test granule update.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  _, collection = default_collection
  granule = utils.granule({"CollectionReference": {"EntryTitle": collection["EntryTitle"]}},
                          dump=False)
  updated_granule = utils.merge(granule, {"GranuleUR": "change_granule_slightly_{}".format(utils.generate_native_id())})
  basic_update_assertions("granule",
                          "application/vnd.nasa.cmr.umm+json;version=1.4",
                          json.dumps(granule),
                          json.dumps(updated_granule))

def test_granule_delete(default_collection):
  """
  Test granule delete.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  _, collection = default_collection
  granule = utils.granule({"CollectionReference": {"EntryTitle": collection["EntryTitle"]}})
  basic_delete_assertions("granule",
                          "application/vnd.nasa.cmr.umm+json;version=1.4",
                          granule)

###############################################################################
#                             Variable Tests                                  #
###############################################################################

def test_variable_create():
  """
  Test variable creation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  variable = utils.variable(dump=False)
  basic_create_assertions("variable",
                          "application/vnd.nasa.cmr.umm+json",
                          variable)

def test_variable_update():
  """
  Test variable update.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  variable = utils.variable(dump=False)
  updated_variable = utils.merge(variable, {"Definition": "change variable_{}".format(utils.generate_native_id())})
  basic_update_assertions("variable",
                          "application/vnd.nasa.cmr.umm+json",
                          json.dumps(variable),
                          json.dumps(updated_variable))

def test_variable_delete():
  """
  Test variable delete.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  variable = utils.variable()
  basic_delete_assertions("variable",
                          "application/vnd.nasa.cmr.umm+json",
                          variable)

###############################################################################
#                             Service Tests                                   #
###############################################################################

def test_service_create():
  """
  Test service creation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  service = utils.service(dump=False)
  basic_create_assertions("service",
                          "application/vnd.nasa.cmr.umm+json",
                          service)

def test_service_update():
  """
  Test service update.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  service = utils.service()
  # Allowing name generation to update the service.
  updated_service = utils.service()
  basic_update_assertions("service",
                          "application/vnd.nasa.cmr.umm+json",
                          service,
                          updated_service)

def test_service_delete():
  """
  Test service delete.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  service = utils.service()
  basic_delete_assertions("service",
                          "application/vnd.nasa.cmr.umm+json",
                          service)
