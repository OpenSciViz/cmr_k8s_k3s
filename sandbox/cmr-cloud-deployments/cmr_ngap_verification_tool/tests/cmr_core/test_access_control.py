"""
Basic access-control-app assertions

Asserts:
  ACL and Group create
  ACL and Group update
  ACL and Group delete
  ACL and Group search
  ACL and Group retrieval
  ACL permissions applied to collection
  Access-Control health is OK
"""
from assertions import *
from config import config
import json
import pytest
import requests
import utils
import time

###############################################################################
#                       General Access Control Assertions                     #
###############################################################################

def assert_access_control_update(concept_type,
                                 concept_id,
                                 revision_id,
                                 updated_concept,
                                 merge_with={}):
  """
  Basic access control update assertions.

  Params:
    concept_type: group or acl
    concept_id: Concept id to update.
    revision_id: revision id after create.
    updated_concept: the concept to update with.
    merge_with: Extra fields to merge with updated_concept before asserting comparison.

  Asserts:
    Revision is incremented.
    Group or ACL is updated after retrieval.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  assert 1 == revision_id

  updated_response = utils.update_access_control(concept_type,
                                                 json.dumps(updated_concept),
                                                 concept_id,
                                                 token=config.token)
  updated_response.raise_for_status()
  assert 200 == updated_response.status_code
  updated_json = updated_response.json()
  assert 2 == updated_json["revision_id"]


  retrieve_response = utils.retrieve_access_control(concept_type,
                                                    concept_id,
                                                    token=config.token)
  retrieve_response.raise_for_status()
  assert utils.merge(updated_concept, merge_with) == retrieve_response.json()

def assert_access_control_delete(concept_type,
                                 concept):
  """
  Basic access control delete assertions.

  Asserts:
    Revision is incremented.
    Group or ACL is deleted after retrieval.
    Group or ACL removed from search.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  response = utils.create_access_control(concept_type,
                                         concept,
                                         token=config.token)
  response.raise_for_status()
  concept_id = response.json()["concept_id"]


  delete_response = utils.delete_access_control(concept_type,
                                                concept_id,
                                                token=config.token,
                                                wait_for_index=True)
  delete_response.raise_for_status()
  revision_id_key = "revision-id" if concept_type == "acl" else "revision_id"
  assert 200 == delete_response.status_code
  assert 2 == delete_response.json()[revision_id_key]


  retrieve_response = utils.retrieve_access_control(concept_type,
                                                    concept_id,
                                                    token=config.token)
  assert retrieve_response.status_code == 404
  headers = {"Authorization": config.token}
  params = {"id" if concept_type == "acl" else "concept_id": concept_id}
  search_response = utils.search_access_control(concept_type,
                                                headers=headers,
                                                params=params)
  search_response.raise_for_status()
  assert 0 == int(search_response.headers["CMR-Hits"])

###############################################################################
#                             Health Test                                     #
###############################################################################

def test_access_control_health():
  """
  Assert access control health.
  """
  health = utils.get_health(config.access_control_endpoint)
  assert 200 == health.status_code
  assert_all_health(health.json())

###############################################################################
#                             Site Test                                       #
###############################################################################

def test_access_control_site_routes():
  """
  Test access control routes.
  """
  site_routes = ["/",
                 "/site/docs/access-control",
                 "/site/docs/access-control/api",
                 "/site/docs/access-control/usage",
                 "/site/docs/access-control/schema",
                 "/site/docs/access-control/site",
                 "/site/access_control_api_docs.html",
                 "/site/docs/access-control/api",
                 "/site/docs/access-control/site"]
  assert_site_routes(config.access_control_endpoint,
                     site_routes)

###############################################################################
#                               ACL Tests                                     #
###############################################################################

def test_acl_create(default_acl):
  """
  Tests ACL creation. See default_acl fixture for acl creation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  acl_response, _ = default_acl
  assert 200 == acl_response.status_code

def test_acl_update(default_acl):
  """
  Basic update acl assertions.

  Asserts:
    Revision is incremented.
    ACL is updated after retrieval.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  acl_response, _ = default_acl
  acl_json = acl_response.json()
  updated_acl = utils.acl(dump=False)
  updated_acl["catalog_item_identity"]["name"] = "UPDATED TEST ACL FOR PYTEST_{}.".format(utils.generate_native_id())
  assert_access_control_update("acl",
                               acl_json["concept_id"],
                               acl_json["revision_id"],
                               updated_acl)

def test_acl_delete():
  """
  Basic delete acl assertions.

  Asserts:
    Revision is incremented.
    ACL is deleted after retrieval.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  assert_access_control_delete("acl",
                               utils.acl())

def test_acl_retrieve(default_acl):
  """
  Test access control retrieve.

  Asserts:
    Able to search for created ACL

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  acl_response, _ = default_acl
  response = utils.retrieve_access_control("acl",
                                           acl_response.json()["concept_id"],
                                           token=config.token)
  response.raise_for_status()
  assert 200 == response.status_code

def test_acl_search(default_acl):
  """
  Test access control search.

  Asserts:
    Able to search for created ACL

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  acl_response, _ = default_acl
  concept_id = acl_response.json()["concept_id"]
  params = {"id": concept_id}
  headers = {"Authorization": config.token}
  search_response = utils.search_access_control("acl",
                                                headers=headers,
                                                params=params)
  search_response.raise_for_status()
  assert 200 == search_response.status_code
  assert 1 == int(search_response.headers["CMR-Hits"])
  assert concept_id == search_response.json()["items"][0]["concept_id"]

@pytest.mark.reindex
def test_acl_collection_permissions(default_collection, default_acl):
  """
  Test ACL application to a collection.
  """
  collection_concept_id, collection = default_collection
  params = {"concept_id": collection_concept_id}

  # Attempt search collection as guest should fail
  search_response = utils.search("collection", params=params)
  search_response.raise_for_status()
  assert 0 == int(search_response.headers["CMR-Hits"])

  # Grant access to collection
  acl_response, acl = default_acl
  acl["catalog_item_identity"]["collection_identifier"] = {"concept_ids": [collection_concept_id]}
  acl_update_response = utils.update_access_control("acl",
                                                    json.dumps(acl),
                                                    acl_response.json()["concept_id"],
                                                    token=config.token)
  acl_update_response.raise_for_status()
  utils.reindex_collection_permitted_groups(token=config.token)
  # Search should now succeed
  utils.wait_for_concept_index(True,
                               utils.search,
                               collection_concept_id,
                               1,
                               "collection",
                               token=None)
  search_response = utils.search("collection", params=params)
  search_response.raise_for_status()
  assert 1 == int(search_response.headers["CMR-Hits"])

###############################################################################
#                               Group Tests                                   #
###############################################################################

def test_group_create(default_group):
  """
  Tests group creation. See default_group fixture for group creation.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  group_response = default_group
  assert 200 == group_response.status_code

def test_group_update(default_group):
  """
  Basic update group assertions.

  Asserts:
    Revision is incremented.
    group is updated after retrieval.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  group_response = default_group
  group_json = group_response.json()
  updated_group = utils.group(dump=False)
  updated_group["description"] = "UPDATED TEST group FOR PYTEST_{}.".format(utils.generate_native_id())
  assert_access_control_update("group",
                               group_json["concept_id"],
                               group_json["revision_id"],
                               updated_group,
                               merge_with={"num_members": 0})

def test_group_delete():
  """
  Basic delete group assertions.

  Asserts:
    Revision is incremented.
    group is deleted after retrieval.

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  assert_access_control_delete("group",
                               utils.group())

def test_group_retrieve(default_group):
  """
  Test access control retrieve.

  Asserts:
    Able to search for created group

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  group_response = default_group
  response = utils.retrieve_access_control("group",
                                           group_response.json()["concept_id"],
                                           token=config.token)
  response.raise_for_status()
  assert 200 == response.status_code

def test_group_search(default_group):
  """
  Test access control search.

  Asserts:
    Able to search for created group

  Raises:
    requests.exceptions.HTTPError: On bad status code.

  Returns:
    Nothing: None.
  """
  group_response = default_group
  concept_id = group_response.json()["concept_id"]
  params = {"concept_id": concept_id}
  headers = {"Authorization": config.token}
  search_response = utils.search_access_control("group",
                                                headers=headers,
                                                params=params)
  search_response.raise_for_status()
  assert 200 == search_response.status_code
  assert 1 == int(search_response.headers["CMR-Hits"])
  assert concept_id == search_response.json()["items"][0]["concept_id"]
