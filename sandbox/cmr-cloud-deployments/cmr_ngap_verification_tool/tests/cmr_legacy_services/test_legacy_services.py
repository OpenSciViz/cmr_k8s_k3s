"""
Basic legacy service assertions

Asserts:
  token create
  token delete
  invalid token use
  use of token after creation
"""
from config import config
import templates
import utils

def test_token_creation():
  """
  Token creation test.

  Asserts:
    Status code is 201.
    Able to retrieve token info.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  response = utils.create_token(utils.token_user_info(), "application/json")
  response.raise_for_status()
  assert 201 == response.status_code
  token = response.json()["token"]["id"]

  response = utils.get_token_info(token)
  response.raise_for_status()
  assert 200 == response.status_code

  token_info = response.json()
  assert token == token_info["token_info"]["token"]

def test_token_deletion():
  """
  Token delete test.

  Asserts:
    Status code is 200, 204.
    Able to retrieve token info.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  response = utils.create_token(utils.token_user_info(), "application/json")
  response.raise_for_status()
  token = response.json()["token"]["id"]

  response = utils.delete_token(token)
  response.raise_for_status()
  assert response.status_code in {200, 204}

  response = utils.get_token_info(token)
  assert "does not exist" in response.text

def test_invalid_token():
  """
  Test for invalid token.
  """
  headers = {"Authorization": "not_a_real_token",
             "Accept": "application/json"}
  response = utils.search("collection",
                          headers=headers)
  assert 401 == response.status_code
  assert "Token does not exist" in response.text

def test_use_token():
  """
  Search for a collection with a valid token.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """
  # Attempt to ingest without permissions
  collection = utils.collection()
  response = utils.ingest("collection",
                          "application/vnd.nasa.cmr.umm+json",
                          data=collection)
  assert 401 == response.status_code

  # Create token and attempt ingest with new token
  token_response = utils.create_token(utils.token_user_info(), "application/json")
  token_response.raise_for_status()
  token = token_response.json()["token"]["id"]

  response = utils.ingest("collection",
                          "application/vnd.nasa.cmr.umm+json",
                          data=collection,
                          token=token)
  response.raise_for_status()
  assert response.status_code in {200, 201}
