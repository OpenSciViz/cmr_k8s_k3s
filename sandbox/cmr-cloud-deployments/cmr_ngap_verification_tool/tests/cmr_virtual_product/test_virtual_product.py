"""
Basic virtual product assertions

Asserts:
  Virtual product ingest events.
"""
from config import config
import utils
import time

def test_virtual_product():
  """
  Assert that when ingesting to a virtual collection the granule is also ingested
  into the destination collection.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
  """

  additional_attributes = [{"Group": "gov.nasa.gsfc.disc",
                            "Value": "source granule ur",
                            "Description": "Source granule AA.",
                            "Name": "source_granule_ur",
                            "DataType": "STRING"}]
  # Only create source collection if necessary
  search = utils.search("collection",
                        params={"short_name": "VP_TEST"},
                        headers={"Authorization": config.token})
  search.raise_for_status()
  if int(search.headers['CMR-Hits']) != 1:
    source_attributes = {"EntryTitle": "{} VP Test".format(config.provider),
                         "ShortName": "VP_TEST",
                         "Version": "001",
                         "AdditionalAttributes": additional_attributes}
    source_collection = utils.collection(attribs=source_attributes)
    source_response = utils.ingest("collection",
                                   "application/vnd.nasa.cmr.umm+json",
                                   source_collection,
                                   token=config.token)
    source_response.raise_for_status()

  # Only create destination collection if necessary
  search = utils.search("collection",
                        headers={"Accept": "application/json",
                                 "Authorization": config.token},
                        params={"short_name": "VP_TEST_DEST"})
  search.raise_for_status()
  if int(search.headers['CMR-Hits']) != 1:
    dest_attributes = {"EntryTitle": "A Virtual Product test collection.",
                       "ShortName": "VP_TEST_DEST",
                       "Version": "001",
                       "AdditionalAttributes": additional_attributes}
    dest_collection = utils.collection(attribs=dest_attributes)

    dest_response = utils.ingest("collection",
                                 "application/vnd.nasa.cmr.umm+json",
                                 dest_collection,
                                 token=config.token)
    dest_response.raise_for_status()
    dest_concept_id = dest_response.json()["concept-id"]
  else:
    dest_concept_id = search.json()["feed"]["entry"][0]["id"]

  new_id = utils.generate_native_id()
  with open("virtual_product_granule.xml") as granule_file:
    granule = granule_file.read().replace('GENERATED_GRANULE_ID', new_id)

  granule_response = utils.ingest("granule",
                                  "application/echo10+xml",
                                  granule,
                                  token=config.token)
  granule_response.raise_for_status()

  # Wait for granule to be registered to new collection
  timeout = time.time() + config.max_poll_wait
  while True:
    search = utils.search("granule",
                          headers={"Accept": "application/json",
                                   "Authorization": config.token},
                          params={"collection_concept_id": dest_concept_id})
    search.raise_for_status()
    all_urs = [entry["title"] for entry in search.json()["feed"]["entry"]]
    if "VP_TEST_DEST:{}".format(new_id) in all_urs:
      assert "VP_TEST_DEST:{}".format(new_id) in all_urs
      break

    if time.time() > timeout:
      raise TimeoutError("Concept indexing time has exceeded maximum poll wait time.")
    time.sleep(config.poll_interval)
