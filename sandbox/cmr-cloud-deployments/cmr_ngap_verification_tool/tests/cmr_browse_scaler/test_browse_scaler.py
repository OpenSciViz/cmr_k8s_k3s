"""
Basic browse scaler assertions

Asserts:
  Collection image retrieval with and without cache
  Collection image resize with and without cache
  Graule image retrieval with and without cache
  Granule image resize with and without cache
"""
import browse_scaler_util
import requests

###############################################################################
#                         General Browse Scaler Assertions                    #
###############################################################################

def assert_concept_image(concept_type,
                         concept,
                         concept_id):
  """
  Retrieve browse image url from concept and compare that image with the image returned
  from browse-scaler. Compares sizes.

  Params:
    concept_type: The concept type (collection or granule).
    concept: The concept itself.
    concept_id: The concept id to use.

  Returns:
    None: Void.

  Raises:
    requests.exceptions.HTTPError: On bad status code.
    ValueError: When concept does not contain a browse image.
  """
  image_url = browse_scaler_util.parse_browse_image_from_concept(concept)
  if image_url is None:
    raise ValueError("Concept must contain a browse image.")

  original_width, original_height = browse_scaler_util.get_image_aspect(image_url=image_url)
  browse_image_response = browse_scaler_util.retrieve_browse_image(concept_type,
                                                                   concept_id)
  assert browse_image_response.status_code == 200
  browse_width, browse_height = browse_scaler_util.get_image_aspect(raw_image=browse_image_response.content)
  assert browse_width == original_width
  assert browse_height == original_height

def assert_concept_image_resize(concept_type,
                                concept,
                                concept_id):
  """
  Retrieve resized browse image from browse scaler and compare size requested vs
  size returned from python imaging library. Requests resize of original height and width
  divided by 2.

  Params:
    concept_type: The concept type (collection or granule).
    concept_id: The concept id to use.

  Returns:
    None: Void.
  """
  original_image_url = browse_scaler_util.parse_browse_image_from_concept(concept)
  original_width, original_height = browse_scaler_util.get_image_aspect(image_url=original_image_url)

  calculated_width = original_width / 2
  calculated_height = original_height / 2

  browse_image_response = browse_scaler_util.retrieve_browse_image(concept_type,
                                                                   concept_id,
                                                                   height=calculated_height,
                                                                   width=calculated_width)
  assert browse_image_response.status_code == 200

  browse_width, browse_height = browse_scaler_util.get_image_aspect(raw_image=browse_image_response.content)
  assert browse_width == calculated_width
  assert browse_height == calculated_height

def assert_concept_image_not_found(concept_type):
  """
  Retrieve a fake concept id and assert No image is being returned.

  Params:
    concept_type: The concept type to search.
  """
  concept_id = 'CFAKE-IMAGE' if concept_type == 'collection' else 'GFAKE-IMAGE'
  browse_image_response = browse_scaler_util.retrieve_browse_image(concept_type,
                                                                   concept_id)
  assert browse_scaler_util.compare_with_not_found(browse_image_response.content) is None

###############################################################################
#                             Collection Tests                                #
###############################################################################

def test_collection_image(default_collection):
  """
  Test collection image. See assert_concept_image.
  """
  concept_id, collection = default_collection
  assert_concept_image("collection",
                       collection,
                       concept_id)

  # Repeat assertion to confirm caching is working as expected.
  # Soft test in that I can't guarantee it is being returned from the cache
  # but logic dictates it is.
  assert_concept_image("collection",
                       collection,
                       concept_id)

def test_collection_image_with_resize(default_collection):
  """
  Test collection image resize. See assert_concept_image_resize.
  """
  concept_id, collection = default_collection
  assert_concept_image_resize("collection",
                              collection,
                              concept_id)

  # Cache test.
  assert_concept_image_resize("collection",
                              collection,
                              concept_id)

def test_collection_image_not_found():
  """
  Test collection image not found. See assert_concept_image_not_found.
  """
  assert_concept_image_not_found("collection")

  # Cache test.
  assert_concept_image_not_found("collection")

###############################################################################
#                             Granule Tests                                   #
###############################################################################

def test_granule_image(default_granule_with_concept):
  """
  Test granule image. See assert_concept_image.
  """
  granule, concept_id = default_granule_with_concept
  assert_concept_image("granule",
                       granule,
                       concept_id)

  # Cache test.
  assert_concept_image("granule",
                       granule,
                       concept_id)

def test_granule_image_with_resize(default_granule_with_concept):
  """
  Test granule image resize. See assert_concept_image_resize.
  """
  granule, concept_id = default_granule_with_concept
  assert_concept_image_resize("granule",
                              granule,
                              concept_id)

  # Cache test.
  assert_concept_image_resize("granule",
                              granule,
                              concept_id)

def test_granule_image_not_found():
  """
  Test granule image not found. See assert_concept_image_not_found.
  """
  assert_concept_image_not_found("granule")

  # Cache test.
  assert_concept_image_not_found("granule")
