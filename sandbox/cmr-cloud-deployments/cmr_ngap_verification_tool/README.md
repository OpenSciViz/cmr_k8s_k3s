# NGAP Verification Tool

## Description

Tests for cmr_core, cmr_legacy_services, cmr_opensearch, and cmr_service_bridge.

Do not run with system token.

## Installation

    pip3 install .

## Configuration

Create a config.json in resources/. See resources/config.json.template for required fields / example.

## Usage

- `pytest`: Runs all tests.
- `pytest <directory>/*`: Run all tests in directory.
- `pytest <directory>/<file_name>`: Run all tests in file.
- `pytest <directory>/<file_name> -k <test_name>`: Runs test names that match test_name (regex match)

Run with `--allow-reindex` to run tests with lengthy reindex jobs. Might need to increase timeout in this case.

## To speed up tests

- Add `-n auto` to parallelize tests.

## Tests
- Collection, Variable, Service, Granule, ACL, Group: Creation, deletion, update, Search, Retrieval.
- CMR Core site routes are reachable.
- Variable, Service: Association.
- Token: Creation, deletion, usage (valid and invalid).
- Opensearch: Search for collection and granule.
- Health checks: Verify health checks are true for access-control, search, ingest, opensearch, and service-bridge.
- Service-Bridge: OUS and size-estimate services work.
- Browse Scaler: Images are returned regularly and from the cache. Also able to resize.
- Virtual Product: Ingest events handled correctly.

NOTE: For Virtual Product tests the testing provider used MUST have the proper collections configured and be set in environment variable `CMR_VIRTUAL_PRODUCT_PROVIDER_ALIASES`.
