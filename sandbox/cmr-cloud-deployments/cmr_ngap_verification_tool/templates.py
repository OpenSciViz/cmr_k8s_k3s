"""
Concept type templates for general use.
"""
from config import config

collection = \
{
    "AncillaryKeywords": ["CALIBRATED", "EOSDIS", "GEOLOCATED", "INFRARED", "RADIANCE"],
    "CollectionCitations": [{
        "Creator": "AIRS Science Team/Joao Texeira",
        "Editor": "Test Editor",
        "OnlineResource": {
            "Linkage": "https://disc.gsfc.nasa.gov/datacollection/AIRI2CCF_006.html"
        },
        "Publisher": "Goddard Earth Sciences Data and Information Services Center (GES DISC)",
        "Title": "1",
        "SeriesName": "AIRI2CCF",
        "ReleaseDate": "2013-01-15T00:00:00.000Z",
        "Version": "006",
        "ReleasePlace": "Greenbelt, MD, USA",
        "IssueIdentification": "Test Issue Identification",
        "DataPresentationForm": "Test data presentation form",
        "OtherCitationDetails": "Test other citation details"
    }],
    "AdditionalAttributes": [{
        "Group": "gov.nasa.gsfc.disc",
        "Value": "6 minutes",
        "Name": "Data Granularity",
        "Description": "The time coverage of individual data granules.",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "7e8ee64a-d684-4d26-9e5a-cbe3741599d2",
        "Name": "metadata.uuid",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "2015-12-22 13:27:42",
        "Name": "metadata.extraction_date",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "8.1",
        "Name": "metadata.keyword_version",
        "Description": "Not provided",
        "DataType": "FLOAT"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "2015-12-28T18:07:02ZVersion:-3.0Target:10.2",
        "Name": "DIF9.0-to-DIF10-Converter",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "EARTH SCIENCE",
        "Name": "Discipline",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "ECHOUSA/NASA",
        "Name": "IDN_Node.Short_Name",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "2efb470f-61f8-4046-b2ee-ebed27e2f3109ae20472-ba4c-4b01-8d97-857b73fa3c95",
        "Name": "IDN_Node.UUID",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "10.5067/Aqua/AIRS/DATA204",
        "Name": "Original.Dataset_Citation.DOI",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "VERSION 9.9.3",
        "Name": "Original.Metadata_Version",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "http://gcmd.gsfc.nasa.gov/index.html",
        "Name": "Original.xsi:schemaLocation",
        "Description": "Not provided",
        "DataType": "STRING"
    }, {
        "Group": "gov.nasa.gsfc.gcmd",
        "Value": "2018-05-21:docbuilder",
        "Name": "metadata.tool",
        "Description": "Not provided",
        "DataType": "STRING"
    }],
    "SpatialExtent": {
        "SpatialCoverageType": "HORIZONTAL",
        "HorizontalSpatialDomain": {
            "Geometry": {
                "CoordinateSystem": "CARTESIAN",
                "BoundingRectangles": [{
                    "WestBoundingCoordinate": -180.0,
                    "NorthBoundingCoordinate": 90.0,
                    "EastBoundingCoordinate": 180.0,
                    "SouthBoundingCoordinate": -90.0
                }]
            }
        },
        "GranuleSpatialRepresentation": "CARTESIAN"
    },
    "CollectionProgress": "COMPLETE",
    "ScienceKeywords": [{
        "Category": "EARTH SCIENCE",
        "Topic": "SPECTRAL/ENGINEERING",
        "Term": "INFRARED WAVELENGTHS",
        "VariableLevel1": "INFRARED RADIANCE",
        "DetailedVariable": "CLOUD-CLEARED INFRARED RADIANCE"
    }],
    "TemporalExtents": [{
        "RangeDateTimes": [{
            "BeginningDateTime": "2002-08-30T00:00:00.000Z",
            "EndingDateTime": "2016-09-24T23:59:59.999Z"
        }]
    }],
    "ProcessingLevel": {
        "Id": "2"
    },
    "DOI": {
        "DOI": "10.5067/Aqua/AIRS/DATA204"
    },
    "ShortName": "test_cmr_ngap_0",
    "EntryTitle": "test_cmr_ngap_0",
    "PublicationReferences": [{
        "PublicationDate": "2030-04-29T00:00:00.000Z",
        "Series": "IEEE Transactions on Geoscience and Remote Sensing,",
        "Title": "Retrieval of Atmospheric and Surface Parameters From AIRS/AMSU/HSB Data in the Presence of Clouds",
        "DOI": {
            "DOI": "10.1109/TGRS.2002.808236"
        },
        "Pages": "390-409",
        "Volume": "41",
        "Author": "Joel Susskind, Christopher D. Barnet, and John M. Blaisdell",
        "Issue": "2"
    }, {
        "PublicationDate": "2014-03-31T00:00:00.000Z",
        "Series": "J. Appl. Rem. Sens.",
        "Title": "Improved methodology for surface and atmospheric soundings, error estimates, and quality control procedures: the atmospheric infrared sounder science team version-6 retrieval algorithm",
        "DOI": {
            "DOI": "10.1117/1.JRS.8.084994"
        },
        "Pages": "34",
        "Volume": "8",
        "Author": "Joel Susskind, John, M. Blaisdell, and Lena Iredell",
        "Issue": "1"
    }],
    "Quality": "This product contains a flag for each radiance frequency indicating the quality. The three options for this quality flag are: 0 for best quality, 1 for good quality, 2 for do not use. A limitation of this product is in scenes with low contrast between the nine AIRS footprints the cloud clearing determination is poorer. Also in scenes with clouds above an inversion the warmest footprint is not necessarily the clearest.",
    "ISOTopicCategories": ["IMAGERY/BASE MAPS/EARTH COVER"],
    "AccessConstraints": {
        "Description": "None"
    },
    "RelatedUrls": [
        {
            "Description": "Sample download URL",
            "URLContentType": "DistributionURL",
            "GetData": {
                "Format": "Not provided",
                "MimeType": "text/csv",
                "Size": 10,
                "Unit": "KB"
            },
            "Subtype": "APPEEARS",
            "Type": "GET DATA",
            "URL": "https://ghrc.nsstc.nasa.gov/pub/fieldCampaigns/ampr/camex1/browse/camex1_ampr_19930930_202808-211832.gif"
        },
        {
            "Description": "Sample download URL without mime type",
            "URLContentType": "DistributionURL",
            "GetData": {
                "Format": "Not provided",
                "Size": 10,
                "Unit": "KB"
            },
            "Type": "GET DATA",
            "URL": "https://ghrc.nsstc.nasa.gov/pub/fieldCampaigns/ampr/camex1/browse/camex1_ampr_19930930_202808-211832.nc"
        },
        {
            "Description": "GET DATA should resolve to access due to missing mime type and unable to infer.",
            "URLContentType": "DistributionURL",
            "GetData": {
                "Format": "Not provided",
                "Size": 10,
                "Unit": "KB"
            },
            "Type": "GET DATA",
            "URL": "https://ghrc.nsstc.nasa.gov/pub/fieldCampaigns/ampr/camex1/browse/camex1_ampr_19930930_202808-211832"
        },
        {
            "Description": "Sample browse image",
            "URLContentType": "VisualizationURL",
            "GetData": {
                "Format": "Not provided",
                "MimeType": "image/gif",
                "Size": 10,
                "Unit": "KB"
            },
            "Type": "GET RELATED VISUALIZATION",
            "URL": "https://cdn.earthdata.nasa.gov/eui/latest/docs/assets/ed-logos/app-logo_hover_2x.png"
        }, {
            "Description": "Access the data via HTTP.",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "Subtype": "GENERAL DOCUMENTATION",
            "URL": "https://airsl2.gesdisc.eosdis.nasa.gov/data/Aqua_AIRS_Level2/AIRI2CCF.006/"
        }, {
            "Description": "Access the data via the OPeNDAP protocol.",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "Subtype": "GENERAL DOCUMENTATION",
            "URL": "https://airsl2.gesdisc.eosdis.nasa.gov/opendap/Aqua_AIRS_Level2/AIRI2CCF.006/contents.html"
        }, {
            "Description": "Use the Simple Subset Wizard (SSW) to submit subset requests for data sets across multiple data centers from a single unified interface.",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "Subtype": "GENERAL DOCUMENTATION",
            "URL": "https://disc.gsfc.nasa.gov/SSW/#keywords=AIRI2CCF%20006"
        }, {
            "Description": "Use the Earthdata Search to find and retrieve data sets across multiple data centers.",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "Subtype": "GENERAL DOCUMENTATION",
            "URL": "https://search.earthdata.nasa.gov/search?q=AIRI2CCF+006"
        }, {
            "Description": "AIRS home page at NASA/JPL. General information on the AIRS instrument, algorithms, and other AIRS-related activities can be found.",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "Subtype": "GENERAL DOCUMENTATION",
            "URL": "https://airs.jpl.nasa.gov/index.html"
        }, {
            "Description": "AIRS Documentation Page",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "URL": "https://disc.gsfc.nasa.gov/information/documents?title=AIRS%20Documentation"
        }, {
            "Description": "README Document",
            "URLContentType": "PublicationURL",
            "Type": "VIEW RELATED INFORMATION",
            "Subtype": "GENERAL DOCUMENTATION",
            "URL": "https://docserver.gesdisc.eosdis.nasa.gov/repository/Mission/AIRS/3.3_ScienceDataProductDocumentation/3.3.4_ProductGenerationAlgorithms/README.AIRS_V6.pdf"
        }],
    "Abstract": "The Atmospheric Infrared Sounder (AIRS) is a grating spectrometer (R = 1200) aboard the second Earth Observing System (EOS) polar-orbiting platform, EOS Aqua. In combination with the Advanced Microwave Sounding Unit (AMSU) and the Humidity Sounder for Brazil (HSB), AIRS constitutes an innovative atmospheric sounding group of visible, infrared, and microwave sensors. Cloud-Cleared Radiances contain calibrated, geolocated channel-by-channel AIRS infrared radiances (milliWatts/m2/cm-1/steradian) that would have been observed within each AMSU footprint if there were no clouds in the FOV and produced along with the AIRS Standard Product, as they are the radiances used to retrieve the Standard Product. Nevertheless, they are an order of magnitude larger in data volume than the remainder of the Standard Products and, many Standard Product users are expected to have little interest in the Cloud Cleared Radiance. For these reasons they are a separate output file, but like the Standard Product, are generated at all locations.",
    "LocationKeywords": [{
        "Category": "GEOGRAPHIC REGION",
        "Type": "GLOBAL"
    }],
    "MetadataDates": [{
        "Date": "2013-01-10T00:00:00.000Z",
        "Type": "DELETE"
    }, {
        "Date": "2018-05-21T00:00:00.000Z",
        "Type": "UPDATE"
    }],
    "Version": "006",
    "Projects": [{
        "ShortName": "Aqua",
        "LongName": "Earth Observing System (EOS), Aqua"
    }],
    "DataCenters": [{
        "Roles": ["ARCHIVER", "DISTRIBUTOR"],
        "ShortName": "NASA/GSFC/SED/ESD/GCDC/GESDISC",
        "LongName": "Goddard Earth Sciences Data and Information Services Center (formerly Goddard DAAC), Global Change Data Center, Earth Sciences Division, Science and Exploration Directorate, Goddard Space Flight Center, NASA",
        "ContactInformation": {
            "RelatedUrls": [{
                "URLContentType": "DataCenterURL",
                "Type": "HOME PAGE",
                "URL": "https://disc.gsfc.nasa.gov/"
            }],
            "ContactMechanisms": [
                {"Type": "Email",
                 "Value": "support@earthdata.nasa.gov"}
            ]
        }
    }],
    "Platforms": [{
        "ShortName": "Aqua",
        "LongName": "Earth Observing System, Aqua",
        "Instruments": [{
            "ShortName": "AIRS",
            "LongName": "Atmospheric Infrared Sounder"
        }, {
            "ShortName": "AMSU-A",
            "LongName": "Advanced Microwave Sounding Unit-A"
        }]
    }]
}


granule = \
{
  "GranuleUR": "test_cmr_ngap_0",
  "ProviderDates": [{
    "Date": "2030-08-19T03:00:00Z",
    "Type": "Delete"
  }],
  "AccessConstraints": {
    "Value": 5
  },
  "CollectionReference": {
      "EntryTitle": "test_cmr_ngap_0"
  },
  "DataGranule": {
    "ArchiveAndDistributionInformation": [{
      "Name": "GranuleZipFile",
      "Size": 100000,
      "SizeUnit": "KB",
      "Format": "ZIP",
      "MimeType": "application/zip",
      "Checksum": {
        "Value": "E51569BF48DD0FD0640C6503A46D4753",
        "Algorithm": "MD5"
      },
      "Files": [{
        "Name": "GranuleFileName1",
        "Size": 10,
        "SizeUnit": "KB",
        "Format": "NETCDF-4",
        "MimeType": "application/x-netcdf",
        "FormatType": "Native",
        "Checksum": {
          "Value": "E51569BF48DD0FD0640C6503A46D4754",
          "Algorithm": "MD5"
        }
      }, {
        "Name": "GranuleFileName2",
        "Size": 1,
        "SizeUnit": "KB",
        "Format": "ASCII",
        "MimeType": "text/plain",
        "FormatType": "NA"
      }]
    }, {
      "Name": "SupportedGranuleFileNotInPackage",
      "Size": 11,
      "SizeUnit": "KB",
      "Format": "NETCDF-CF",
      "FormatType": "Supported",
      "MimeType": "application/x-netcdf",
      "Checksum": {
        "Value": "E51569BF48DD0FD0640C6503A46D4755",
        "Algorithm": "MD5"
      }
    }],
    "ReprocessingPlanned": "The Reprocessing Planned Statement Value",
    "ReprocessingActual": "The Reprocessing Actual Statement Value",
    "DayNightFlag" : "Unspecified",
    "ProductionDateTime" : "2018-07-19T12:01:01Z",
    "Identifiers": [{
      "Identifier": "SMAP_L3_SM_P_20150407_R13080_001.h5",
      "IdentifierType": "ProducerGranuleId"
    }, {
      "Identifier": "LocalVersionIdValue",
      "IdentifierType": "LocalVersionId"
    }, {
      "Identifier": "FeatureIdValue1",
      "IdentifierType": "FeatureId"
    }, {
      "Identifier": "FeatureIdValue2",
      "IdentifierType": "FeatureId"
    }, {
      "Identifier": "1234",
      "IdentifierType": "Other",
      "IdentifierName": "SomeIdentifier"
    },{
      "Identifier": "CRIDValue",
      "IdentifierType": "CRID"
    }]
  },
  "PGEVersionClass": {
    "PGEName": "A PGE Name",
    "PGEVersion": "6.0.27"
  },
  "TemporalExtent": {
    "RangeDateTime": {
      "BeginningDateTime": "2010-07-17T00:00:00.000Z",
      "EndingDateTime": "2015-07-17T23:59:59.999Z"
    }
  },
  "SpatialExtent": {
    "GranuleLocalities": ["GranuleLocality1", "GranuleLocality2"],
    "HorizontalSpatialDomain": {
      "ZoneIdentifier": "ZoneIdentifier 1",
      "Geometry": {
        "Points": [{
          "Longitude": -77,
          "Latitude": 88
        }, {
          "Longitude":10,
          "Latitude": 10
        }],
        "BoundingRectangles": [{
          "WestBoundingCoordinate": -180,
          "NorthBoundingCoordinate": 85.04450225830078,
          "EastBoundingCoordinate": 180,
          "SouthBoundingCoordinate": -85.04450225830078
        }],
        "GPolygons": [{
          "Boundary" : {
            "Points": [ {"Longitude":-10, "Latitude":-10}, {"Longitude":10, "Latitude":-10}, {"Longitude":10, "Latitude":10}, {"Longitude":-10, "Latitude":10}, {"Longitude":-10, "Latitude":-10}]
          },
          "ExclusiveZone": {
            "Boundaries": [{
              "Points": [{"Longitude":-5, "Latitude":-5}, {"Longitude":-1, "Latitude":-5}, {"Longitude":-1, "Latitude":-1}, {"Longitude":-5, "Latitude":-1}, {"Longitude":-5, "Latitude":-5}]
            }, {
              "Points": [{"Longitude":0, "Latitude":0}, {"Longitude":5, "Latitude":0}, {"Longitude":5, "Latitude":5}, {"Longitude":0, "Latitude":5}, {"Longitude":0, "Latitude":0}]
            }]
          }
        }],
        "Lines": [{
          "Points": [ {"Longitude":-100, "Latitude":-70}, {"Longitude":-88, "Latitude":-66}]
        }]
      }
    },
    "VerticalSpatialDomains": [{
      "Type": "Atmosphere Layer",
      "Value": "Atmosphere Profile"
    }, {
      "Type": "Pressure",
      "Value": "100",
      "Unit": "HectoPascals"
    }, {
      "Type": "Altitude",
      "MinimumValue": "10",
      "MaximumValue": "100",
      "Unit": "Meters"
    }]
  },
  "OrbitCalculatedSpatialDomains": [{
    "OrbitalModelName": "OrbitalModelName",
    "BeginOrbitNumber": 99263,
    "EndOrbitNumber": 99263,
    "EquatorCrossingLongitude":88.92,
    "EquatorCrossingDateTime": "2018-08-16T16:22:21.000Z"
  }],
  "MeasuredParameters": [{
    "ParameterName": "Parameter Name",
    "QAStats": {
      "QAPercentMissingData": 10,
      "QAPercentOutOfBoundsData": 20,
      "QAPercentInterpolatedData": 30,
      "QAPercentCloudCover": 40
    },
    "QAFlags": {
      "AutomaticQualityFlag": "Passed",
      "AutomaticQualityFlagExplanation": "Automatic Quality Flag Explanation",
      "OperationalQualityFlag": "Passed",
      "OperationalQualityFlagExplanation": "Operational Quality Flag Explanation",
      "ScienceQualityFlag": "Passed",
      "ScienceQualityFlagExplanation": "Science Quality Flag Explanation"
    }
  }],
  "CloudCover": 60,
  "RelatedUrls": [{
    "URL": "https://daac.ornl.gov/daacdata/islscp_ii/vegetation/erbe_albedo_monthly_xdeg/data/erbe_albedo_1deg_1986.zip",
    "Type": "GET DATA",
    "Description": "This link provides direct download access to the granule.",
    "Format": "ZIP",
    "MimeType": "application/zip",
    "Size": 395.673,
    "SizeUnit": "KB"
  }, {
    "URL": "https://daac.ornl.gov/ISLSCP_II/guides/erbe_albedo_monthly_xdeg.html",
    "Type": "VIEW RELATED INFORMATION",
    "Subtype": "USER'S GUIDE",
    "Description": "ORNL DAAC Data Set Documentation",
    "Format": "HTML",
    "MimeType": "text/html"
  }, {
    "URL": "https://cdn.earthdata.nasa.gov/eui/latest/docs/assets/ed-logos/app-logo_hover_2x.png",
    "Type": "GET RELATED VISUALIZATION",
    "Description": "ISLSCP II EARTH RADIATION BUDGET EXPERIMENT (ERBE) MONTHLY ALBEDO, 1986-1990",
    "Format": "PNG",
    "MimeType": "image/png",
    "Size": 10,
    "SizeUnit": "MB"
  }, {
    "URL" : "https://f5eil01.edn.ecs.nasa.gov//opendap/DEV01/FS2/AIRS/AIRX3STD.006/2016.01.20/AIRS.2016.01.20.L3.RetStd001.v6.0.31.0.G16027144006.hdf",
    "Type" : "VIEW RELATED INFORMATION",
    "Description" : "The OPENDAP location for the granule.",
    "MimeType" : "application/x-hdf"
  }],
  "NativeProjectionNames": ["MODIS Sinusoidal System", "Sinusoidal"],
  "GridMappingNames": ["Sinusoidal", "Lambert Azimuthal Equal-Area"]
}

service = \
{
    "AncillaryKeywords" : [ "GIS", "Geographic Information Systems", "Arctic Data", "Arctic Projects" ],
    "RelatedURLs" : [ {
      "Description" : "A phony related url.",
      "URLContentType" : "DistributionURL",
      "Type" : "GET SERVICE",
      "Subtype" : "ACCESS MAP VIEWER",
      "URL" : "http://example.com"
    } ],
    "Type" : "WEB SERVICES",
    "ServiceKeywords" : [ {
      "ServiceCategory" : "EARTH SCIENCE SERVICES",
      "ServiceTopic" : "DATA ANALYSIS AND VISUALIZATION",
      "ServiceTerm" : "GEOGRAPHIC INFORMATION SYSTEMS",
      "ServiceSpecificTerm" : "WEB-BASED GEOGRAPHIC INFORMATION SYSTEMS"
    } ],
    "ServiceOrganizations" : [ {
      "Roles" : [ "SERVICE PROVIDER" ],
      "ShortName" : "Arcticdata",
      "LongName" : "Arctic Data Center",
      "ContactPersons" : [ {
        "Roles" : [ "SERVICE PROVIDER" ],
        "LastName" : "ARCTIC DATA CENTER SUPPORT"
      } ],
      "ContactInformation" : {
        "RelatedUrls" : [ {
          "URLContentType" : "DataCenterURL",
          "Type" : "HOME PAGE",
          "URL" : "https://cmr.earthdata.nasa.gov"
        } ],
        "ContactMechanisms" : [ {
          "Type" : "Email",
          "Value" : "support@earthdata.nasa.gov"
        } ]
      }
    } ],
    "ScienceKeywords" : [ {
      "Category" : "EARTH SCIENCE",
      "Topic" : "CRYOSPHERE",
      "Term" : "FROZEN GROUND",
      "VariableLevel1" : "GROUND ICE"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "CRYOSPHERE",
      "Term" : "SEA ICE"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "HUMAN DIMENSIONS",
      "Term" : "BOUNDARIES",
      "VariableLevel1" : "POLITICAL DIVISIONS",
      "VariableLevel2" : "COUNTRY BOUNDARIES"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "HUMAN DIMENSIONS",
      "Term" : "BOUNDARIES",
      "VariableLevel1" : "POLITICAL DIVISIONS",
      "VariableLevel2" : "STATE BOUNDARIES"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "LAND SURFACE",
      "Term" : "LAND USE/LAND COVER",
      "VariableLevel1" : "LAND USE/LAND COVER CLASSIFICATION"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "LAND SURFACE",
      "Term" : "TOPOGRAPHY",
      "VariableLevel1" : "TERRAIN ELEVATION"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "OCEANS",
      "Term" : "BATHYMETRY/SEAFLOOR TOPOGRAPHY",
      "VariableLevel1" : "BATHYMETRY"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "OCEANS",
      "Term" : "SEA ICE"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "TERRESTRIAL HYDROSPHERE",
      "Term" : "GLACIERS/ICE SHEETS"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "TERRESTRIAL HYDROSPHERE",
      "Term" : "SNOW/ICE"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "TERRESTRIAL HYDROSPHERE",
      "Term" : "SURFACE WATER",
      "VariableLevel1" : "SURFACE WATER FEATURES",
      "VariableLevel2" : "LAKES/RESERVOIRS"
    }, {
      "Category" : "EARTH SCIENCE",
      "Topic" : "TERRESTRIAL HYDROSPHERE",
      "Term" : "SURFACE WATER",
      "VariableLevel1" : "SURFACE WATER FEATURES",
      "VariableLevel2" : "RIVERS/STREAMS"
    } ],
    "AccessConstraints" : "None",
    "Description" : "The Advanced Cooperative Arctic Data and Information Service (ACADIS) is a joint effort by the National Snow and Ice Data Center (NSIDC), the University Corporation for Atmospheric Research (UCAR), UNIDATA, and the National Center for Atmospheric Research (NCAR) to provide data archival, preservation and access for all projects funded by NSF's Arctic Science Program (ARC). The map server provides access to various ACADIS projects.",
    "Version" : "NOT PROVIDED",
    "Name" : "test_cmr_ngap_0",
    "ContactPersons" : [ {
      "Roles" : [ "AUTHOR" ],
      "ContactInformation" : {
        "ContactMechanisms" : [ {
          "Type" : "Email",
          "Value" : "cayvon.hamidizadeh@nasa.gov"
        } ],
        "Addresses" : [ {
          "StreetAddresses" : [ "5700 Rivertech Court" ],
          "City" : "Riverdale",
          "StateProvince" : "MD",
          "Country" : "USA",
          "PostalCode" : "20737"
        } ]
      },
      "FirstName" : "CAYVON",
      "LastName" : "HAMIDIZADEH"
    } ],
    "LongName" : "Testing Service For NGAP 2.0 transition."
}

variable = \
{
"VariableType" : "SCIENCE_VARIABLE",
"DataType" : "float",
"Offset" : 0.0,
"ScienceKeywords" : [ {
  "Category" : "EARTH SCIENCE_5658",
  "Topic" : "ATMOSPHERE_5658",
  "Term" : "AEROSOLS_5658",
  "VariableLevel1" : "AEROSOL OPTICAL DEPTH/THICKNESS_5658",
  "VariableLevel2" : "level2_5658",
  "VariableLevel3" : "level3_5658",
  "DetailedVariable" : "details_5658"
} ],
"Scale" : 0.0010000000474974513,
"Sets" : [ {
  "Name" : "Name",
  "Type" : "Type",
  "Size" : 1,
  "Index" : 1
} ],
"Dimensions" : [ {
  "Name" : "Solution_3_Land",
  "Size" : 3,
  "Type" : "OTHER"
}, {
  "Size" : 676,
  "Name" : "Cell_Along_Swath",
  "Type" : "OTHER"
}, {
  "Size" : 451,
  "Name" : "Cell_Across_Swath",
  "Type" : "OTHER"
} ],
"Alias": "/test/cmr/ngap/0",
"Definition" : "Corrected Optical Depth Land, retrieved AOT at 0.47, 0.55,0.66   micron, unitless",
  "Name" : "test_cmr_ngap_0",
  "Units" : "None",
  "LongName" : "test_cmr_ngap_0",
  "AcquisitionSourceName": "test_cmr_ngap_0"
}

token_info = \
{"token":
 {"username": config.username,
  "password": config.password,
  "client_id": config.client_id,
  "user_ip_address": config.user_ip_address}}

acl = \
{"group_permissions":
 [{"user_type": "registered",
   "permissions": ["read"]}],
 "catalog_item_identity": {"name": "test_cmr_ngap_acl",
                           "provider_id": config.provider,
                           "collection_applicable": True}}

group = \
{"name": "test_cmr_ngap_group",
 "provider_id": config.provider,
 "description": "A testing group for CMR."}
