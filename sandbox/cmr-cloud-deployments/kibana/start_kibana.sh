#!/bin/bash

# Abort on failures
set -e

# Ensure environment variables are set
# ES_URL=protocol://xxx:port
export ES_URL="${ES_URL:?Need to set ES_URL.}"
echo "elasticsearch.hosts: [\"$ES_URL\"]" >> /usr/share/kibana/config/kibana.yml
/usr/share/kibana/bin/kibana
