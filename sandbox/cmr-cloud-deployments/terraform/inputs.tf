variable "environment_name" {
  description = "environment name, e.g. sit, uat, prod"
}

variable "aws_region" {
  description = "AWS region to deploy to"
  default     = "us-east-1"
}

variable "cmr_version" {
  description = "CMR version to deploy"
  default     = "latest"
}

variable "cmr_opensearch_version" {
  description = "CMR Opensearch version to deploy"
  default     = "latest"
}

variable "cmr_heritage_version" {
  description = "CMR Heritage version to deploy."
  default     = "latest"
}

variable "cmr_service_bridge_version" {
  description = "CMR Service Bridge version to deploy"
  default     = "latest"
}

variable "cmr_kibana_version" {
  description = "CMR Kibana version to deploy"
  default     = "latest"
}

variable "es_cluster_name" {
  description = "name of the ES cluster"
}

variable "browse_scaler_zip_path" {
  description = "Location of ZIP file to provision lambda with. Leave blank to use a dummy zip file."
  default     = ""
}

variable "kibana_port" {
  description = "Listening port of Kibana"
  default     = 5601
}

variable "envs" {
  description = "Settings for the custom enviroment or to override built-in settings"
  type = map(object({
    access_control_task_count                 = number
    bootstrap_task_count                      = number
    indexer_task_count                        = number
    ingest_task_count                         = number
    legacy_services_task_count                = number
    metadata_db_task_count                    = number
    search_task_count                         = number
    virtual_product_task_count                = number
    opensearch_task_count                     = number
    service_bridge_task_count                 = number
    kibana_task_count                         = number
    access_control_cpu_utilization_threshold  = number
    bootstrap_cpu_utilization_threshold       = number
    indexer_cpu_utilization_threshold         = number
    ingest_cpu_utilization_threshold          = number
    legacy_services_cpu_utilization_threshold = number
    metadata_db_cpu_utilization_threshold     = number
    opensearch_cpu_utilization_threshold      = number
    search_cpu_utilization_threshold          = number
    virtual_product_cpu_utilization_threshold = number
    service_bridge_cpu_utilization_threshold  = number
    kibana_cpu_utilization_threshold          = number
    virtual_product_queue_threshold           = number
    collection_all_revision_queue_threshold   = number
    indexing_queue_threshold                  = number
    access_control_queue_threshold            = number
    cmr_alerts_mailing_list                   = list(string)
  }))
  default = {}
}

variable "schedules" {
  description = "Map of scaling schedules for the custom environment or to override built-in settings"
  type = map(map(list(object({
    starting_cron = string
    ending_cron   = string
    task_count    = number
  }))))
  default = {}
}

variable "vpc_tag_name_filter" {
  type        = string
  description = "Filter to use on tag Name to select a VPC"
  default     = "Application VPC"
}

variable "subnet_tag_name_filter" {
  type        = string
  description = "Filter to use on tag Name to select a VPC"
  default     = "Private application *"
}

variable "permissions_boundary_name" {
  type        = string
  description = "Name of IAM permissions boundary to use for deploying. Use null if not needed"
  default     = "NGAPShRoleBoundary"
}

variable "use_splunk" {
  type    = bool
  default = true
}

variable "create_ecs_role" {
  type    = bool
  default = false
}

variable "cmr_sns_alerts_webhooks" {
  description = "Map of SNS webhook endpoints"
  type        = list(string)
  default     = []
}
