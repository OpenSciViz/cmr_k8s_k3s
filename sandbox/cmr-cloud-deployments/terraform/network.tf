resource "aws_security_group" "service" {
  name        = "service-${var.environment_name}"
  description = "Allow traffic from load balancer to microservices"
  vpc_id      = data.aws_vpc.app.id

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id]
  }

  ingress {
    from_port       = var.kibana_port
    to_port         = var.kibana_port
    protocol        = "tcp"
    security_groups = [data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb_listener" "services" {
  load_balancer_arn = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not found"
      status_code  = "404"
    }
  }
}

resource "aws_lb_listener_rule" "redirect_root_to_search" {
  listener_arn = aws_lb_listener.services.arn
  priority     = 2

  action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
      path        = "/search"
    }
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }
}

resource "aws_lb_listener_rule" "redirect_to_search" {
  listener_arn = aws_lb_listener.services.arn
  priority     = 1000

  action {
    type = "redirect"
    redirect {
      status_code = "HTTP_301"
      path        = "/search/#{path}"
    }
  }

  condition {
    path_pattern {
      values = ["*"]
    }
  }
}

resource "aws_lb_listener_rule" "redirect_robots_txt_to_search" {
  listener_arn = aws_lb_listener.services.arn
  priority     = 4

  action {
    type             = "forward"
    target_group_arn = module.search.aws_lb_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/robots.txt"]
    }
  }
}
