# Note that most Elasticsearch resources are managed in the
# cmr-cloud-elasticsearch repository. This will only set up forwarding
# of Elasticsearch logs from CloudWatch to Splunk.

resource "aws_cloudwatch_log_subscription_filter" "elasticsearch_splunk_forwarding" {
  count           = local.splunk_forwarding_arn != null ? 1 : 0
  name            = "Cloudwatch log filter to forward Elasticsearch logs to Splunk"
  log_group_name  = "/ec2/elasticsearch-${var.es_cluster_name == "" ? var.environment_name : var.es_cluster_name}"
  filter_pattern  = ""
  destination_arn = local.splunk_forwarding_arn
}
