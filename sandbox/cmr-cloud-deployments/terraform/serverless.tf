# Used to interface between terraform outputs with serverless.
# Define outputs and reference them in serverless/cloudformation with:
# ${cf:<environment_name>.<output_name>}
# Creates a CFOutputsInUse SSM parameter to get around the need for CF
# stacks requiring at least one resource to be created.

locals {
  logForwardingArnOutput = local.splunk_forwarding_arn != null ? { "logForwardingArn" : { "Value" : local.splunk_forwarding_arn } } : {}
}

resource "aws_cloudformation_stack" "serverless_outputs" {
  name = var.environment_name
  template_body = jsonencode({
    "Resources" : {
      "CFOutputsInUse" : {
        "Type" : "AWS::SSM::Parameter"
        "Properties" : {
          "Name" : "/${var.environment_name}/cf_outputs"
          "Type" : "String"
          "Value" : "Dummy parameter store value."
        }
      }
    }
    "Outputs" : merge({
      "servicesDnsName" : {
        "Value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
      }
      "servicesSecurityGroupId" : {
        "Value" : aws_security_group.service.id
      }
      "subnetIds" : {
        "Value" : join(",", data.aws_subnets.app.ids)
      }
      "servicesLbListenerArn" : {
        "Value" : aws_lb_listener.services.arn
      }
      "cmrStacDeploymentBucket" : {
        "Value" : aws_s3_bucket.cmr_stac_deployment_bucket.id
      }
      "cmrVdDeploymentBucket" : {
        "Value" : aws_s3_bucket.cmr_vd_deployment_bucket.id
      }
      "cmrServerlessDeploymentBucket" : {
        "Value" : aws_s3_bucket.cmr_serverless_deployment_bucket.id
      }
      "cmrAlertsSnsArn" : {
        "Value" : aws_sns_topic.cmr_alerts.arn
      }
      "redisHost" : {
        "Value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
      }
      "redisPort" : {
        "Value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
      }
    }, local.logForwardingArnOutput)
  })
}

resource "aws_s3_bucket" "cmr_stac_deployment_bucket" {
  bucket = "cmr-stac-deployment-bucket-${var.environment_name}"
}

resource "aws_s3_bucket" "cmr_vd_deployment_bucket" {
  bucket = "cmr-vd-deployment-bucket-${var.environment_name}"
}

resource "aws_s3_bucket" "cmr_serverless_deployment_bucket" {
  bucket = "cmr-serverless-deployment-bucket-${var.environment_name}"
}
