output "services_lb_dns_name" {
  value = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
}

output "cmr_core_repository_url" {
  value = aws_ecr_repository.cmr_core.repository_url
}

output "cmr_core_repository_name" {
  value = aws_ecr_repository.cmr_core.name
}

output "cmr_opensearch_repository_name" {
  value = aws_ecr_repository.cmr_opensearch.name
}

output "cmr_opensearch_repository_url" {
  value = aws_ecr_repository.cmr_opensearch.repository_url
}

output "cmr_service_bridge_repository_url" {
  value = aws_ecr_repository.cmr_service_bridge.repository_url
}

output "cmr_service_bridge_repository_name" {
  value = aws_ecr_repository.cmr_service_bridge.name
}

output "cmr_kibana_repository_url" {
  value = aws_ecr_repository.cmr_kibana.repository_url
}

output "cmr_kibana_repository_name" {
  value = aws_ecr_repository.cmr_kibana.name
}

output "rds_hostname" {
  description = "The FQDN for the CMR database instance."
  value       = data.terraform_remote_state.core_infrastructure.outputs.db_hostname
}

output "rds_url" {
  description = "The URL that can be used to access the database"
  value       = data.terraform_remote_state.core_infrastructure.outputs.db_url
}

output "cmr_legacy_services_repository_url" {
  value = aws_ecr_repository.cmr_legacy_services.repository_url
}

output "cmr_legacy_services_repository_name" {
  value = aws_ecr_repository.cmr_legacy_services.name
}

output "cmr_cluster_arn" {
  value = aws_ecs_cluster.services.arn
}

output "browse_scaler_function_name" {
  value = module.browse_scaler.browse_scaler_function_name
}

output "sns_alerts_arn" {
  value = aws_sns_topic.cmr_alerts.arn
}
