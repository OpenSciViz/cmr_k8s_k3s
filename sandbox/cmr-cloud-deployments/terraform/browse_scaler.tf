module "browse_scaler" {
  source                     = "./modules/browse_scaler"
  environment_name           = var.environment_name
  subnet_ids                 = data.aws_subnets.app.ids
  load_balancer_listener_arn = aws_lb_listener.services.arn
  vpc_id                     = data.aws_vpc.app.id
  security_group_ids         = [aws_security_group.service.id]
  lb_rule_priority           = 87
  services_lb_dns_name       = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
  splunk_forwarding_arn      = local.splunk_forwarding_arn
  # Use the dummy zip file if no zip path is provided.
  browse_scaler_zip_path    = var.browse_scaler_zip_path == "" ? data.archive_file.dummy_browse_scaler[0].output_path : var.browse_scaler_zip_path
  permissions_boundary_name = var.permissions_boundary_name
}
