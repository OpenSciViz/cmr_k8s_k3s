resource "aws_sns_topic" "cmr_alerts" {
  name = "cmr-alerts-${var.environment_name}"
}

resource "aws_sns_topic_policy" "cmr_alerts" {
  arn    = aws_sns_topic.cmr_alerts.arn
  policy = data.aws_iam_policy_document.cmr_alerts.json
}
resource "aws_cloudwatch_metric_alarm" "CMR_Virtual_Product_Queue" {
  alarm_name          = "CMR Virtual Product Queue-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "300"
  statistic           = "Average"
  threshold           = local.merged_envs[var.environment_name].virtual_product_queue_threshold
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]
  alarm_description   = "Virtual product queue - ${var.environment_name} is too large."
  datapoints_to_alarm = "1"
  dimensions = {
    QueueName = "gsfc-eosdis-cmr-${var.environment_name}-virtual_product_queue"
  }
  treat_missing_data        = "missing"
  insufficient_data_actions = []
}

resource "aws_cloudwatch_metric_alarm" "CMR_collection_all_revisions_index_queue" {
  alarm_name          = "CMR collection all revisions index queue-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "300"
  statistic           = "Average"
  threshold           = local.merged_envs[var.environment_name].collection_all_revision_queue_threshold
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]
  alarm_description   = "Collection all revisions index - ${var.environment_name} is too large."
  dimensions = {
    QueueName = "gsfc-eosdis-cmr-${var.environment_name}-index_all_revisions_queue"
  }
  treat_missing_data        = "missing"
  insufficient_data_actions = []
}
resource "aws_cloudwatch_metric_alarm" "CMR_indexing_queue" {
  alarm_name          = "CMR indexing queue-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "300"
  statistic           = "Average"
  threshold           = local.merged_envs[var.environment_name].indexing_queue_threshold
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]
  alarm_description   = "CMR indexing queue - ${var.environment_name} is too large."
  dimensions = {
    QueueName = "gsfc-eosdis-cmr-${var.environment_name}-index_queue"
  }
  treat_missing_data        = "missing"
  insufficient_data_actions = []
}

resource "aws_cloudwatch_metric_alarm" "Access_Control_Index_Queue" {
  alarm_name          = "CMR Access Control Index Queue-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ApproximateNumberOfMessagesVisible"
  namespace           = "AWS/SQS"
  period              = "300"
  statistic           = "Average"
  threshold           = local.merged_envs[var.environment_name].access_control_queue_threshold
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]
  alarm_description   = "Access control index queue - ${var.environment_name} is too large."
  dimensions = {
    QueueName = "gsfc-eosdis-cmr-${var.environment_name}-access_control_index_queue"
  }
  treat_missing_data        = "missing"
  insufficient_data_actions = []
}

resource "aws_cloudwatch_metric_alarm" "cpu_utilization_too_high" {
  alarm_name          = "CMR DB CPU utilization too high-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = 80
  alarm_description   = "Average database CPU utilization too high - ${var.environment_name}."
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]

  dimensions = {
    DBInstanceIdentifier = data.terraform_remote_state.core_infrastructure.outputs.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "free_storage_space_too_low" {
  alarm_name          = "CMR DB free storage space too low-${var.environment_name}"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "FreeStorageSpace"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = data.terraform_remote_state.core_infrastructure.outputs.db_allocated_storage * 0.10 * 1024 * 1024 * 1024 # In GB
  alarm_description   = "Average database free storage space too low - ${var.environment_name}."
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]

  dimensions = {
    DBInstanceIdentifier = data.terraform_remote_state.core_infrastructure.outputs.db_instance_id
  }
}

resource "aws_cloudwatch_metric_alarm" "swap_usage_too_high" {
  alarm_name          = "CMR DB swap usage too high-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "SwapUsage"
  namespace           = "AWS/RDS"
  period              = "300"
  statistic           = "Average"
  threshold           = 1073741824 # 1 gb in bytes.
  alarm_description   = "Average DB swap swap usage too high - ${var.environment_name}."
  alarm_actions       = [aws_sns_topic.cmr_alerts.arn]
  ok_actions          = [aws_sns_topic.cmr_alerts.arn]

  dimensions = {
    DBInstanceIdentifier = data.terraform_remote_state.core_infrastructure.outputs.db_instance_id
  }
}

resource "null_resource" "subscribe_emails" {
  count = length(local.merged_envs[var.environment_name].cmr_alerts_mailing_list) == 0 ? 0 : 1
  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "../scripts/subscribe_emails_to_sns_topic.sh ${aws_sns_topic.cmr_alerts.arn} update ${join(" ", local.merged_envs[var.environment_name].cmr_alerts_mailing_list)}"
  }
}

resource "aws_sns_topic_subscription" "alerts_webhooks" {
  for_each = toset(var.cmr_sns_alerts_webhooks)

  topic_arn = aws_sns_topic.cmr_alerts.arn
  protocol  = "https"
  endpoint  = each.value
}
