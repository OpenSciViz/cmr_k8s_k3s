resource "aws_iam_role" "browse_scaler" {
  name                 = "browse-scaler-${var.environment_name}"
  permissions_boundary = var.permissions_boundary_name != null ? "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/${var.permissions_boundary_name}" : null

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "browse_scaler_read_paramter_store_policy" {
  name        = "browseScalerReadParamterStorePolicy-${var.environment_name}"
  description = "Read parameter store parameters policy."

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssm:GetParameters",
        "ssm:GetParameter",
        "secretsmanager:GetSecretValue"
      ],

      "Resource": ["arn:aws:ssm:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:parameter/${var.environment_name}/*"]
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "browse_scaler" {
  role = aws_iam_role.browse_scaler.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

resource "aws_iam_role_policy_attachment" "browse_scaler_read_parameter_store" {
  role = aws_iam_role.browse_scaler.name
  policy_arn = aws_iam_policy.browse_scaler_read_paramter_store_policy.arn
}
