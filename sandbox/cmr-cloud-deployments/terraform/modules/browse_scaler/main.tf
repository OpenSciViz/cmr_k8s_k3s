# Lambda
resource "aws_lambda_function" "browse_scaler" {
  vpc_config {
    subnet_ids         = var.subnet_ids
    security_group_ids = var.security_group_ids
  }

  filename         = var.browse_scaler_zip_path
  source_code_hash = filebase64sha256(var.browse_scaler_zip_path)
  function_name    = "browse-scaler-${var.environment_name}"
  handler          = "index.handler"
  role             = aws_iam_role.browse_scaler.arn
  runtime          = "nodejs14.x"
  timeout          = 30

  environment {
    variables = {
      CMR_ENVIRONMENT          = var.environment_name
      CMR_ROOT                 = var.services_lb_dns_name
      EXTERNAL_REQUEST_TIMEOUT = 20000
      REDIS_URL                = aws_elasticache_cluster.browse_scaler.cache_nodes[0].address
      REDIS_PORT               = aws_elasticache_cluster.browse_scaler.cache_nodes[0].port
    }
  }
}

# Load Balancer
resource "aws_lb_target_group" "browse_scaler_target_group" {
  name        = "browse-scaler-${var.environment_name}"
  target_type = "lambda"
}

resource "aws_lb_listener_rule" "browse_scaler_listener_rule" {
  listener_arn = var.load_balancer_listener_arn
  priority     = var.lb_rule_priority

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.browse_scaler_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/browse-scaler*"]
    }
  }
}

resource "aws_lambda_permission" "with_lb" {
  statement_id  = "AllowExecutionFromLB"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.browse_scaler.function_name
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = aws_lb_target_group.browse_scaler_target_group.arn
}

resource "aws_lb_target_group_attachment" "browse_scaler_attachment" {
  target_group_arn = aws_lb_target_group.browse_scaler_target_group.arn
  target_id        = aws_lambda_function.browse_scaler.arn
}

resource "aws_cloudwatch_log_group" "browse_scaler" {
  name = "/aws/lambda/browse-scaler-${var.environment_name}"
}

resource "aws_cloudwatch_log_subscription_filter" "splunk_forwarding" {
  name            = "Cloudwatch log filter to forward browse-scaler logs to Splunk"
  log_group_name  = aws_cloudwatch_log_group.browse_scaler.name
  filter_pattern  = ""
  destination_arn = var.splunk_forwarding_arn
}
