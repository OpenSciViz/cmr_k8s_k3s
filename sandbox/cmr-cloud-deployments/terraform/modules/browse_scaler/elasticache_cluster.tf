resource "aws_elasticache_cluster" "browse_scaler" {
  cluster_id           = "browse-scaler-${var.environment_name}"
  engine               = "redis"
  node_type            = "cache.t2.medium"
  num_cache_nodes      = 1
  parameter_group_name = aws_elasticache_parameter_group.redis_params.id
  engine_version       = "3.2.10"
  port                 = 1521
  subnet_group_name    = aws_elasticache_subnet_group.browse_scaler.name
  security_group_ids   = [aws_security_group.elasticache.id]
}

resource "aws_elasticache_subnet_group" "browse_scaler" {
  name       = "browse-scaler-${var.environment_name}"
  subnet_ids = var.subnet_ids
}

resource "aws_security_group" "elasticache" {
  name        = "browse-scaler-elasticache-${var.environment_name}"
  description = "Allow access to Elasticache in ${var.environment_name}"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 1521
    to_port     = 1521
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elasticache_parameter_group" "redis_params" {
  name   = "browse-scaler-redis-params-${var.environment_name}"
  family = "redis3.2"

  parameter {
    name  = "maxmemory-policy"
    value = "allkeys-lru"
  }
}
