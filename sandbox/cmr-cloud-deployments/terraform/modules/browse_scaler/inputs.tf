variable "environment_name" {
  description = "Environment name, e.g. sit, uat, or prod"
}

variable "subnet_ids" {
  description = "Subnet ids of the VPC"
  type        = list(string)
}

variable "vpc_id" {
  description = "VPC in which service will run"
}

variable "load_balancer_listener_arn" {
  description = "arn of the top-level load-balancer listener"
}

variable "security_group_ids" {
  description = "security group used by services"
}

variable "lb_rule_priority" {
  description = "load balancer path-based routing rule priority number"
}

variable "services_lb_dns_name" {
  description = "Internal CMR services load balancer."
}

variable "browse_scaler_zip_path" {
  description = "Location of ZIP file to provision lambda with."
}

variable "splunk_forwarding_arn" {
  description = "Destination ARN to use to forward cloudwatch logs to Splunk."
  type = string
  default = null
}

variable "permissions_boundary_name" {
  type = string
  description = "Name of IAM permissions boundary to use for deploying. Use null if not needed"
  default = "NGAPShRoleBoundary"
}
