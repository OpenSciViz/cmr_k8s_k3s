locals {
  db_engine         = "oracle-se2"
  db_engine_version = "19"
}

resource "aws_db_subnet_group" "private_subnets" {
  name       = "cmr-${var.environment_name}-private-subnets"
  subnet_ids = var.subnet_ids

  tags = {
    Name = "Private subnets DB subnet group"
  }
}

resource "aws_security_group" "rds_access" {
  name        = "cmr-${var.environment_name}-rds-sg"
  description = "Allow access to RDS"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 1521
    to_port     = 1521
    protocol    = "tcp"
    cidr_blocks = [data.aws_vpc.app.cidr_block_associations[0].cidr_block]
  }
}

resource "aws_db_option_group" "cmr_db_s3_integration" {
  name                     = "s3-integration-${var.environment_name}-${local.db_engine_version}"
  option_group_description = "Terraform Option Group to set S3 integration"
  engine_name              = local.db_engine
  major_engine_version     = local.db_engine_version

  option {
    option_name = "S3_INTEGRATION"
    port = 0
    version = "1.0"
  }
}

resource "aws_db_instance" "cmr_db" {
  allocated_storage           = var.snapshot_id == null ? 20 : null
  engine                      = var.snapshot_id == null ? local.db_engine : null
  engine_version              = var.snapshot_id == null ? local.db_engine_version : null
  username                    = var.snapshot_id == null ? var.master_db_username : null
  password                    = var.snapshot_id == null ? var.master_db_password : null
  license_model               = var.snapshot_id == null ? "license-included" : null
  instance_class              = var.instance_type
  identifier                  = "cmr-${var.environment_name}"
  db_subnet_group_name        = aws_db_subnet_group.private_subnets.id
  vpc_security_group_ids      = [aws_security_group.rds_access.id]
  allow_major_version_upgrade = false
  auto_minor_version_upgrade  = false
  max_allocated_storage       = var.max_allocated_storage
  deletion_protection         = var.delete_protection
  option_group_name           = aws_db_option_group.cmr_db_s3_integration.name
  backup_retention_period     = var.backup_retention_period
  snapshot_identifier         = var.snapshot_id != null ? data.aws_db_snapshot.db_snapshot[0].id : null
  # This must always be set to true. Terraform bug since 2016: https://github.com/hashicorp/terraform/issues/5417
  skip_final_snapshot = true

  # This block is added for safety. Will ignore changes to snapshot_identifier
  # after initial creation. If you intend to restore an existing database
  # from a snapshot you must temporarily remove this block.
  # Reject any commits to permanently remove this block.
  lifecycle {
    ignore_changes = [snapshot_identifier, performance_insights_enabled]
  }
}

resource "aws_iam_role" "cmr_db_s3_integration" {
  name                 = "cmr-db-${var.environment_name}"
  permissions_boundary = var.permissions_boundary_name != null ? "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/${var.permissions_boundary_name}" : null

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "rds.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cmr_db_s3_integration" {
  name = "rds-s3-${var.environment_name}"
  description = "Allows RDS instance to access AWS S3"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cmr_db_s3_integration" {
  role       = aws_iam_role.cmr_db_s3_integration.name
  policy_arn = aws_iam_policy.cmr_db_s3_integration.arn
}

resource "aws_db_instance_role_association" "cmr_db_s3_integration" {
  db_instance_identifier = aws_db_instance.cmr_db.id
  feature_name           = "S3_INTEGRATION"
  role_arn               = aws_iam_role.cmr_db_s3_integration.arn
}
