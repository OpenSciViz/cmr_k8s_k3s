variable "environment_name" {
  description = "environment name, e.g. sit, uat, prod"
}

variable "vpc_id" {
  description = "VPC in which database will run"
}

variable "subnet_ids" {
  type        = list(string)
  description = "Subnet IDs to use for the DB subnet group"
}

variable "snapshot_id" {
  description = "Snapshot identifier to restore database from. Leave blank if you do not want to restore."
  type        = string
}

variable "instance_type" {
  description = "RDS instance type to use for the database."
}

variable "backup_retention_period" {
  description = "The number of days to retain an RDS backup."
  type        = number
}

variable "delete_protection" {
  description = "Whether or not to prevent database deletions."
  type        = bool
}

variable "max_allocated_storage" {
  description = "The maximum amount of storage that can be allocated by autoscaling."
  type        = number
}

variable "permissions_boundary_name" {
  type = string
  description = "Name of IAM permissions boundary to use for deploying. Use null if not needed"
  default = "NGAPShRoleBoundary"
}

variable "master_db_username" {
  type        = string
  default     = null
}

variable "master_db_password" {
  type        = string
  default     = null
}
