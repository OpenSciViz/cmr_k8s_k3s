data "aws_db_snapshot" "db_snapshot" {
  count                  = var.snapshot_id != null ? 1 : 0
  db_snapshot_identifier = var.snapshot_id
  include_shared         = true
}

data "aws_vpc" "app" {
  id = var.vpc_id
}

data "aws_caller_identity" "current" {
}
