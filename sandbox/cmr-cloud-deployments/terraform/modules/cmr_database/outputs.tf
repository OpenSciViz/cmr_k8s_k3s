output "db_hostname" {
  description = "The FQDN for the database instance."
  value       = aws_db_instance.cmr_db.address
}

output "db_url" {
  description = "The URL to use to connect to the database."
  value       = "thin:@${aws_db_instance.cmr_db.endpoint}:${aws_db_instance.cmr_db.name}"
}

output "db_instance_id" {
  description = "The database instance ID."
  value       = aws_db_instance.cmr_db.id
}

output "db_allocated_storage" {
  description = "DB allocated storage."
  value       = aws_db_instance.cmr_db.allocated_storage
}
