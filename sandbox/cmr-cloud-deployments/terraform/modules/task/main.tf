resource "aws_cloudwatch_log_group" "task" {
  name = "cmr-${var.task_name}-${var.environment_name}"
}

resource "aws_ecr_repository" "repository" {
  name = "cmr-${var.task_name}-${var.environment_name}"
}

resource "aws_ssm_parameter" "additional_envs" {
  count     = length(var.additional_env)
  name      = "/${var.environment_name}/${var.task_name}/${var.additional_env[count.index]["name"]}"
  value     = "${var.additional_env[count.index]["value"]}"
  type      = "String"
  overwrite = true
}

resource "null_resource" "push_image_to_ecr" {
  count = var.dockerfile_path == "" ? 0 : 1
  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "${path.module}/../../../scripts/ecr.sh ${var.dockerfile_path} ${var.task_version} ${aws_ecr_repository.repository.name} ${aws_ecr_repository.repository.repository_url}"
  }
}

resource "null_resource" "ecs_run_task" {
  count = var.exec_task ? 1 : 0
  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "aws ecs run-task --cli-input-json '${jsonencode(local.run_task_cli)}'"
  }
}

resource "aws_ecs_task_definition" "task" {
  depends_on               = [null_resource.push_image_to_ecr]
  family                   = "${var.task_name}-${var.environment_name}"
  execution_role_arn       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/ecsTaskExecutionRole"
  task_role_arn            = var.task_role_arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.cpu
  memory                   = var.memory
  container_definitions    = <<ENDDEF
[
  {
    "name": "app",
    "image": "${aws_ecr_repository.repository.repository_url}:${var.task_version}",
    "essential": true,
    "secrets": ${data.external.secret_parameters.result["ENVS"]},
    "environment": ${data.external.non_secret_parameters.result["ENVS"]},
    "command": ${jsonencode(var.command)},
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.task.name}",
        "awslogs-region": "${data.aws_region.current.name}",
        "awslogs-stream-prefix": "${var.environment_name}"
      }
    }
  }
]
ENDDEF
}
