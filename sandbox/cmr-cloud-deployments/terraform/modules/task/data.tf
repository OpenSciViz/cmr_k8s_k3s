data "aws_caller_identity" "current" {
}

data "aws_region" "current" {
}

data "external" "secret_parameters" {
  program = ["bash", "${path.module}/../../../scripts/parameter_store.sh", "/${var.environment_name}/${var.task_name}/", "SecureString"]
}

data "external" "non_secret_parameters" {
  program    = ["bash", "${path.module}/../../../scripts/parameter_store.sh", "/${var.environment_name}/${var.task_name}/", "String"]
  depends_on = [aws_ssm_parameter.additional_envs]
}
