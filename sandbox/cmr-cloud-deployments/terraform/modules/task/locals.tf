locals {
  run_task_cli = {
    "cluster"        = var.cluster_name
    "taskDefinition" = aws_ecs_task_definition.task.arn
    "launchType"     = "FARGATE"
    "networkConfiguration" = {
      "awsvpcConfiguration" = {
        "subnets"        = var.subnet_ids
        "securityGroups" = [var.security_group_id]
        "assignPublicIp" = "DISABLED"
      }
    }
  }
}
