variable "environment_name" {
  description = "Environment name, e.g. sit, uat, or prod"
  type        = string
}

variable "task_name" {
  description = "A unique name for the task"
  type        = string
}

variable "additional_env" {
  description = "JSON fragment for adding new env vars or overriding env vars read from Parameter Store"
  type        = list(map(string))
  default     = []
}

variable "task_version" {
  description = "Version to start task"
  type        = string
}

variable "dockerfile_path" {
  description = "Absolute dockerfile path to deploy. Leave blank to not push to ECR."
  type        = string
  default     = ""
}

variable "command" {
  description = "The command to run against the image"
  type        = list(string)
}

variable "subnet_ids" {
  description = "List of subnet ids to run in"
  type        = list(string)
}

variable "cpu" {
  description = "CPU allocation for ECS task"
  type        = number
}

variable "memory" {
  description = "Memory allocation for ECS task"
  type        = number
}

variable "task_role_arn" {
  description = "The arn of the task role that allows the service to access AWS services"
  type        = string
}

variable "security_group_id" {
  description = "Security group for ECS tasks"
  type        = string
}

variable "cluster_name" {
  description = "Name of ECS cluster in which the task will run"
  type        = string
}

variable "exec_task" {
  description = "Execute task after task definition creation."
  type        = bool
  default     = true
}
