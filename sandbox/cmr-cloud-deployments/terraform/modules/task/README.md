# Tasks

This module is for one off tasks using fargate that do not need to be attached to a service.
They will run until completion and no attempt will be made to maintain a specific task count.
These tasks are not associated with a load balancer.

The provided dockerfile path and version will be used to push to ECR and run the task with.
If the version already exists in ECR the image will not be pushed again.

NOTE: If a task requires shutdown prior to completion terraform destroy will NOT stop the task.
You must run `aws ecs stop-task --task <value>` to stop the task.
