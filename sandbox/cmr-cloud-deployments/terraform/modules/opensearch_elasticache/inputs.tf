variable "environment_name" {
  description = "Environment name, e.g. sit, uat, or prod"
}

variable "subnet_ids" {
  description = "Subnet ids of the VPC"
  type        = list(string)
}

variable "vpc_id" {
  description = "VPC in which service will run"
}