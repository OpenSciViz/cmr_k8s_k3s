output "opensearch_redis_url" {
  description = "Address of Redis instance."
  value       = aws_elasticache_cluster.opensearch.cache_nodes[0].address
}

output "opensearch_redis_port" {
  description = "Port of Redis instance."
  value       = aws_elasticache_cluster.opensearch.cache_nodes[0].port
}
