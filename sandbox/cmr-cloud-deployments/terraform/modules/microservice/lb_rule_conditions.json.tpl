[
    {
        "Field": "path-pattern",
        "Values": ["/${service_name}", "/${service_name}/*"]
    },
    {
        "Field": "source-ip",
        "SourceIpConfig": {
            "Values": ["${cidr_block}"]
        }
    }
]