resource "aws_cloudwatch_metric_alarm" "CMR_insufficent_tasks_in_service" {
  count               = var.task_count <= 1 ? 0 : 1
  alarm_name          = "CMR insufficent tasks in service ${var.service_name}-${var.environment_name}"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "300"
  statistic           = "SampleCount"
  threshold           = var.task_count
  alarm_actions       = [var.sns_alarm_topic_arn]
  ok_actions          = [var.sns_alarm_topic_arn]
  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = "${var.service_name}-${var.environment_name}"
  }
  treat_missing_data        = "missing"
  insufficient_data_actions = []
}

resource "aws_cloudwatch_metric_alarm" "CMR_cpu_utilization_high" {
  alarm_name          = "CPU utilization is too high in service ${var.service_name}-${var.environment_name}"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "30"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = "60"
  statistic           = "Average"
  threshold           = var.cpu_utilization_threshold
  alarm_description   = "${var.service_name}-${var.environment_name} CPU utilization is too high."
  alarm_actions       = [var.sns_alarm_topic_arn]
  ok_actions          = [var.sns_alarm_topic_arn]
  dimensions = {
    ClusterName = var.cluster_name
    ServiceName = "${var.service_name}-${var.environment_name}"
  }
  treat_missing_data        = "missing"
  insufficient_data_actions = []
}

