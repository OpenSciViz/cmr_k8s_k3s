locals {
  command = jsonencode(var.command)
}

resource "aws_cloudwatch_log_group" "service" {
  name = "cmr-${var.service_name}-${var.environment_name}"
}

resource "aws_cloudwatch_log_subscription_filter" "splunk_forwarding" {
  count           = var.splunk_forwarding_arn != null ? 1 : 0
  name            = "Cloudwatch log filter to forward to Splunk"
  log_group_name  = aws_cloudwatch_log_group.service.name
  filter_pattern  = ""
  destination_arn = var.splunk_forwarding_arn
}

resource "aws_ecs_task_definition" "service" {
  family                   = "${var.service_name}-${var.environment_name}"
  execution_role_arn       = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.ecs_role_name}"
  task_role_arn            = var.task_role_arn
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.cpu
  memory                   = var.memory
  container_definitions    = <<ENDDEF
[
  {
    "name": "app",
    "image": "${var.image}",
    "essential": true,
    "secrets": ${data.external.secret_parameters.result["ENVS"]},
    "environment": ${data.external.non_secret_parameters.result["ENVS"]},
    "command": ${local.command},
    "portMappings": [{
      "hostPort": ${var.service_port},
      "protocol": "tcp",
      "containerPort": ${var.service_port}
    }],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.service.name}",
        "awslogs-region": "${data.aws_region.current.name}",
        "awslogs-stream-prefix": "${var.environment_name}",
        "awslogs-multiline-pattern": "^[\\d\\[]"
      }
    }
  }
]
ENDDEF

}
resource "aws_ecs_service" "service" {
  name = "${var.service_name}-${var.environment_name}"
  cluster = data.aws_ecs_cluster.cluster.arn
  launch_type = "FARGATE"
  task_definition = aws_ecs_task_definition.service.id
  desired_count = var.task_count
  health_check_grace_period_seconds = 180

  # TODO look into multi-AZ options, make sure containers are distributed among subnets

  network_configuration {
    subnets = var.subnet_ids
    security_groups = [var.security_group_id]
  }
  load_balancer {
    container_name = "app"
    target_group_arn = aws_lb_target_group.service.id
    container_port = var.service_port
  }
}

# Load Balancer

resource "aws_lb_target_group" "service" {
  name = "${var.service_name}-${var.environment_name}"
  port = var.service_port
  protocol = "HTTP"
  target_type = "ip"
  vpc_id = var.vpc_id
  deregistration_delay = 120

  health_check {
    path = "/${var.service_name}/"
    interval = 20
    unhealthy_threshold = 6
    timeout = 10
    matcher = "200,302"
  }
}

resource "aws_lb_listener_rule" "service" {
  listener_arn = var.lb_listener_arn
  priority = var.lb_rule_priority

  action {
    type = "forward"
    target_group_arn = aws_lb_target_group.service.arn
  }

  condition {
    path_pattern {
      values = ["/${var.service_name}", "/${var.service_name}/*"]
    }
  }
  lifecycle {
    ignore_changes = [condition]
  }
}

resource "null_resource" "modify_listener_rule" {
  count = var.is_private

  triggers = {
    build_number = timestamp()
  }

  provisioner "local-exec" {
    command = "aws elbv2 modify-rule --rule-arn ${aws_lb_listener_rule.service.arn} --conditions '${templatefile("${path.module}/lb_rule_conditions.json.tpl", { cidr_block   = data.aws_vpc.app.cidr_block_associations[0].cidr_block, service_name = var.service_name})}'"
  }
}

resource "aws_ssm_parameter" "additional_envs" {
  count = length(var.additional_env)
  name = "/${var.environment_name}/${var.service_name}/${var.additional_env[count.index]["name"]}"
  value = var.additional_env[count.index]["value"]
  type = "String"
  overwrite = true
}
