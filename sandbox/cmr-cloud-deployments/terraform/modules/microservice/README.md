# Clojure app

Sets up the general resources required to define a CMR Clojure application.

# Setting up Parameter Store

## IMPORTANT
ENSURE THAT `cli_follow_urlparam = false` IS INSIDE YOUR AWS CONFIG FILE FOR THE PROFILE YOU ARE USING.
Otherwise AWS will try to follow that url and upload the entire contents of that url into parameter
store.

## Create configuration JSON for each CMR service based on configurations from parameter store in SIT

    ` ./scripts/get_all_parameters.sh sit /Users/yliu10/config/test`
To capture the EECS settings in JSON files for each environment/service

1. Download existing SIT configuration (in JSON format) from parameter store:

```
cd cmr-cloud-deployments
./scripts/get_all_parameters.sh sit <Absoulte Directory Path for configurations>
e.g. ./scripts/get_all_parameters.sh sit /Users/yliu10/config/test
```

2. Make necessary configuration changes for your environment
Make the necessary configuration changes in the JSON configuration files created in step above.

## Write configs to AWS Parameter Store
1. Clone the `populate-parameter-store` repository from BitBucket
`git clone ssh://git@git.earthdata.nasa.gov:7999/cmr/populate-parameter-store.git`
2. Change to the repository directory
`cd populate-parameter-store`
3. Execute the Clojure app to write to parameter store
`lein run -e <ENVIRONMENT> -u <AWS USER PROFILE> -p <Absoulte Directory Path for configurations>`
e.g. lein run -e yliu -u cmr-sit -p /Users/yliu10/config/test
