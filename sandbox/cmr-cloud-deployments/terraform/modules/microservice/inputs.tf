variable "environment_name" {
  description = "Environment name, e.g. sit, uat, or prod"
}

variable "service_name" {
  description = "A unique name for the microservice e.g. 'search'"
}

variable "is_private" {
  description = "Set to 0 to allow traffic from outside the VPC"
  default     = 1
}

variable "additional_env" {
  description = "JSON fragment for adding new env vars or overriding env vars read from Parameter Store"
  type        = list(map(string))
  default     = []
}

variable "image" {
  description = "Docker image to run"
}

variable "command" {
  description = "The command to run against the image"
  type        = list(string)
}

variable "cluster_name" {
  description = "Name of ECS cluster in which the microservice will run"
}

variable "subnet_ids" {
  type = list(string)
}

variable "cpu" {
  description = "CPU allocation for ECS service"
}

variable "memory" {
  description = "Memory allocation for ECS service"
}

variable "task_count" {
  description = "Number of tasks to start for service"
  default     = 2
}

variable "cpu_utilization_threshold" {
  description = "Threshold of CPU utilization that will raise an alarm"
  default = 80
}

variable "vpc_id" {
  description = "VPC in which service will run"
}

variable "security_group_id" {
  description = "Security group for ECS services"
}

variable "lb_listener_arn" {
  description = "The microservice ALB listener ARN which handles all microservice traffic"
}

variable "lb_rule_priority" {
  description = "load balancer path-based routing rule priority number"
}

variable "service_port" {
  description = "listing port of the microservice"
}

variable "task_role_arn" {
  description = "The arn of the task role that allows the service to access AWS services"
}

variable "schedule" {
  description = "List of schedule objects to scale service with"
  type = list(object({
    task_count    = number
    starting_cron = string
    ending_cron   = string
  }))
  default = []
}

variable "service_scaler_arn" {
  description = "Lambda function scaler arn"
  type        = string
}

variable "service_scaler_name" {
  description = "Lambda function scaler name"
  type        = string
}

variable "splunk_forwarding_arn" {
  description = "Destination ARN to use to forward cloudwatch logs to Splunk."
  type        = string
  default     = null
}

variable "sns_alarm_topic_arn" {
  description = "SNS Topic to place Cloudwatch alarms on"
  type        = string
}

variable "vpc_tag_name_filter" {
  type = string
  default = "Application vpc"
}

variable "ecs_role_name" {
  description = "IAM role name to use for ECS services"
  type = string
  default = "ecsTaskExecutionRole"
}

variable "health_check_path" {
  description = "Path to use for health check if not default"
  type = string
  default = null
}
