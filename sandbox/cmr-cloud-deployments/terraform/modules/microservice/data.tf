data "aws_caller_identity" "current" {
}

data "aws_region" "current" {
}

data "aws_ecs_cluster" "cluster" {
  cluster_name = var.cluster_name
}

data "aws_vpc" "app" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_tag_name_filter]
  }
}

data "external" "secret_parameters" {
  program = ["bash", "${path.module}/../../../scripts/parameter_store.sh", "/${var.environment_name}/${var.service_name}/", "SecureString"]
}

data "external" "non_secret_parameters" {
  program    = ["bash", "${path.module}/../../../scripts/parameter_store.sh", "/${var.environment_name}/${var.service_name}/", "String"]
  depends_on = [aws_ssm_parameter.additional_envs]
}
