resource "aws_cloudwatch_event_rule" "start_rule" {
  count               = length(var.schedule)
  name                = "${var.service_name}-scale-start-${count.index}-${var.environment_name}"
  description         = "Time based scaling rule"
  schedule_expression =  var.schedule[count.index]["starting_cron"]
}

resource "aws_cloudwatch_event_rule" "end_rule" {
  count               = length(var.schedule)
  name                = "${var.service_name}-scale-end-${count.index}-${var.environment_name}"
  description         = "Time based scaling rule"
  schedule_expression = var.schedule[count.index]["ending_cron"]
}

resource "aws_lambda_permission" "start_permission" {
  count         = length(var.schedule)
  statement_id  = "${var.service_name}-scale-start-perm-${count.index}-${var.environment_name}"
  action        = "lambda:InvokeFunction"
  function_name = var.service_scaler_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.start_rule[count.index].arn
}

resource "aws_lambda_permission" "end_permission" {
  count         = length(var.schedule)
  statement_id  = "${var.service_name}-scale-end-perm-${count.index}-${var.environment_name}"
  action        = "lambda:InvokeFunction"
  function_name = var.service_scaler_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.end_rule[count.index].arn
}

resource "aws_cloudwatch_event_target" "start_target" {
  count = length(var.schedule)
  rule  = aws_cloudwatch_event_rule.start_rule[count.index].name
  arn   = var.service_scaler_arn
  input = <<EOF
{
  "cluster": "${data.aws_ecs_cluster.cluster.arn}",
  "service": "${aws_ecs_service.service.name}",
  "task_count": ${var.schedule[count.index]["task_count"]}
}
EOF
}

resource "aws_cloudwatch_event_target" "end_target" {
  count = length(var.schedule)
  rule  = aws_cloudwatch_event_rule.end_rule[count.index].name
  arn   = var.service_scaler_arn
  input = <<EOF
{
  "cluster": "${data.aws_ecs_cluster.cluster.arn}",
  "service": "${aws_ecs_service.service.name}",
  "task_count": ${var.task_count}
}
EOF
}
