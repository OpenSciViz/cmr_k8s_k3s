output "service_ecs_task_definition_arn" {
  value = aws_ecs_task_definition.service.arn
}

output "aws_lb_target_group" {
  value = aws_lb_target_group.service
}
