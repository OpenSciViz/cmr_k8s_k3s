
terraform {
  required_version = "1.0.11"
  required_providers {
    archive = {
      source = "hashicorp/archive"
      version = "2.2.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "4.12.0"
    }
    external = {
      source  = "hashicorp/external"
      version = "2.2.0"
    }
    null = {
      source = "hashicorp/null"
      version = "3.1.1"
    }
  }
}