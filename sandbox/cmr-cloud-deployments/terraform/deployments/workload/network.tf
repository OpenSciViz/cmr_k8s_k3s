resource "aws_security_group" "workload" {
  name        = "cmr-workload-${var.environment_name}"
  description = "Allow outbound traffic for workload."
  vpc_id      = data.aws_vpc.app.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
