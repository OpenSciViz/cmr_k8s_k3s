data "aws_caller_identity" "current" {
}

data "aws_vpc" "app" {
  filter {
    name   = "tag:Name"
    values = ["Application VPC"]
  }
}

data "aws_subnets" "app" {
  filter {
    name = "vpc-id"
    values = [data.aws_vpc.app.id]
  }
  
  tags = {
    Name = "Private application *"
  }
}
