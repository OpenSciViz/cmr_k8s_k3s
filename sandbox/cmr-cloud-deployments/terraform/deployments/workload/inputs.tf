variable "environment_name" {
  description = "environment name, e.g. sit, uat, prod"
}

variable "aws_region" {
  description = "AWS region to deploy to"
  default = "us-east-1"
}

variable "ingest_driver_command" {
  description = "Ingest driver run command. Do not include -u <url>. Will append load balancer to run command."
  type        = string
  default     = ""
}

variable "log_miner_command" {
  description = "Log miner run command. Do not include -u <url>. Will append load balancer to run command."
  type        = string
  default     = ""
}

variable "log_miner_dockerfile_path" {
  description = "Log miner dockerfile path to build and push to ECR with."
  type        = string
  default     = ""
}

variable "ingest_driver_dockerfile_path" {
  description = "Ingest driver dockerfile path to build and push to ECR with."
  type        = string
  default     = ""
}

variable "driver_version" {
  description = "Driver versions to deploy"
  default     = "latest"
}

variable "exec_task" {
  description = "Exec task with apply."
  type        = bool
  default     = true
}

variable "permissions_boundary_name" {
  type = string
  description = "Name of IAM permissions boundary to use"
  default = "NGAPShRoleBoundary"
}
