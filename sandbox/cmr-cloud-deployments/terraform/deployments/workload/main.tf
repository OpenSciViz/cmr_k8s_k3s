resource "aws_ecs_cluster" "workload" {
  name = "cmr-workload-drivers-${var.environment_name}"
}

module "log_miner" {
  source            = "../../modules/task"
  environment_name  = var.environment_name
  cluster_name      = aws_ecs_cluster.workload.name
  task_name         = "log-miner"
  dockerfile_path   = var.log_miner_dockerfile_path
  command           = split(" ", var.log_miner_command)
  subnet_ids        = data.aws_subnets.app.ids
  task_version      = var.driver_version
  cpu               = 2048
  memory            = 5120
  task_role_arn     = aws_iam_role.task.arn
  security_group_id = aws_security_group.workload.id
  exec_task         = var.exec_task
}

module "ingest_driver" {
  source            = "../../modules/task"
  cluster_name      = aws_ecs_cluster.workload.name
  environment_name  = var.environment_name
  task_name         = "ingest-driver"
  dockerfile_path   = var.ingest_driver_dockerfile_path
  task_version      = var.driver_version
  command           = split(" ", var.ingest_driver_command)
  subnet_ids        = data.aws_subnets.app.ids
  cpu               = 2048
  memory            = 5120
  task_role_arn     = aws_iam_role.task.arn
  security_group_id = aws_security_group.workload.id
  exec_task         = var.exec_task
}
