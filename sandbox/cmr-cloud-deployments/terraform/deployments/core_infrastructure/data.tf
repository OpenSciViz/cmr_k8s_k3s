data "aws_vpc" "app" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_tag_name_filter]
  }
}

data "aws_subnets" "app" {
  filter {
    name = "vpc-id"
    values = [data.aws_vpc.app.id]
  }

  tags = {
    Name = var.subnet_tag_name_filter
  }
}
