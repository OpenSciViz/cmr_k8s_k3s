locals {
  envs = {
    sit = {
      rds_instance_type       = "db.m5.large"
      backup_retention_period = 3
      max_allocated_storage   = 400
      delete_protection       = true
    }
    wl = {
      rds_instance_type       = "db.m5.2xlarge"
      backup_retention_period = 0
      max_allocated_storage   = 10000 
      delete_protection       = false
    }
    smwl = {
      rds_instance_type       = "db.m5.2xlarge"
      backup_retention_period = 0
      max_allocated_storage   = 10000 
      delete_protection       = false
    }
    uat = {
      rds_instance_type       = "db.m5.xlarge"
      backup_retention_period = 3
      max_allocated_storage   = 800
      delete_protection       = true
    }
    prod = {
      rds_instance_type       = "db.m5.2xlarge"
      backup_retention_period = 7
      max_allocated_storage   = 10000
      delete_protection       = true
    }
  }

  internet_services_account_cidr_block = "10.4.144.0/20"

  merged_envs = merge(local.envs, var.envs)
}
