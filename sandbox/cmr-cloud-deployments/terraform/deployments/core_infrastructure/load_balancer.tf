resource "aws_security_group" "load_balancer" {
  name        = "internal_services-${var.environment_name}"
  description = "Allow inbound traffic within the VPC to microservices"
  vpc_id      = data.aws_vpc.app.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = [
      data.aws_vpc.app.cidr_block_associations[0].cidr_block,
      local.internet_services_account_cidr_block
    ]
  }

  ingress {
    from_port = 5601
    to_port   = 5601
    protocol  = "tcp"
    cidr_blocks = [
      data.aws_vpc.app.cidr_block_associations[0].cidr_block,
      local.internet_services_account_cidr_block
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "services" {
  name                       = "cmr-services-${var.environment_name}"
  internal                   = true
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.load_balancer.id]
  subnets                    = data.aws_subnets.app.ids
  enable_deletion_protection = local.merged_envs[var.environment_name].delete_protection
  idle_timeout               = 180
}
