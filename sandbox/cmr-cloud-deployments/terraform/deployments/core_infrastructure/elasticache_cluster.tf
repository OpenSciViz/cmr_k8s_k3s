resource "aws_elasticache_cluster" "elasticache" {
  cluster_id           = "services-cache-${var.environment_name}"
  engine               = "redis"
  node_type            = "cache.t2.medium"
  num_cache_nodes      = 1
  parameter_group_name = aws_elasticache_parameter_group.redis_params.id
  engine_version       = "3.2.10"
  port                 = 1521
  subnet_group_name    = aws_elasticache_subnet_group.elasticache.name
  security_group_ids   = [aws_security_group.elasticache.id]
}

resource "aws_elasticache_subnet_group" "elasticache" {
  name       = "cmr-cache-subg-${var.environment_name}"
  subnet_ids = data.aws_subnets.app.ids
}

resource "aws_security_group" "elasticache" {
  name        = "cmr-cache-sg-${var.environment_name}"
  description = "Allow access to Elasticache in ${var.environment_name}"
  vpc_id      = data.aws_vpc.app.id

  ingress {
    from_port = 1521
    to_port   = 1521
    protocol  = "tcp"
    cidr_blocks = [
      data.aws_vpc.app.cidr_block_associations[0].cidr_block,
      local.internet_services_account_cidr_block
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elasticache_parameter_group" "redis_params" {
  name   = "cmr-services-redis-params-${var.environment_name}"
  family = "redis3.2"

  parameter {
    name  = "maxmemory-policy"
    value = "volatile-lru"
  }
}
