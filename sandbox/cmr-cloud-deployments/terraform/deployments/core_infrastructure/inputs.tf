variable "environment_name" {
  description = "environment name, e.g. sit, uat, prod"
}

variable "aws_region" {
  description = "aws region to deploy to"
  type        = string
  default     = "us-east-1"
}

# NOTE: If snapshot_id is given for a database that has already been created
# then you must temporarily remove the lifecycle block from modules/cmr_database/db.tf
# in order for terraform to realize those changes.
variable "snapshot_id" {
  description = "Snapshot identifier to restore database from. Leave blank if you do not want to restore. Must be provided on intial creation."
  type        = string
  default     = null
}

variable "envs" {
  description = "Settings for the custom enviroment or to override built-in settings"
  type = map(object({
    rds_instance_type       = string
    backup_retention_period = number
    delete_protection       = bool
    max_allocated_storage   = number

  }))
  default = {}
}

variable "permissions_boundary_name" {
  type        = string
  description = "Name of IAM permissions boundary to use"
  default     = "NGAPShRoleBoundary"
}

variable "vpc_tag_name_filter" {
  type    = string
  default = "Application VPC"
}

variable "subnet_tag_name_filter" {
  type    = string
  default = "Private application *"
}

variable "master_db_username" {
  type    = string
  default = null
}

variable "master_db_password" {
  type    = string
  default = null
}
