output "db_hostname" {
  description = "The FQDN for the database instance."
  value       = module.cmr_database.db_hostname
}

output "db_url" {
  description = "The URL to use to connect to the database."
  value       = module.cmr_database.db_url
}

output "db_instance_id" {
  description = "DB Instance ID."
  value       = module.cmr_database.db_instance_id
}

output "db_allocated_storage" {
  description = "DB allocated storage."
  value       = module.cmr_database.db_allocated_storage
}

output "app_load_balancer_id" {
  description = "The application load balancer ID."
  value       = aws_lb.services.id
}

output "app_load_balancer_dns_name" {
  description = "The application load balancer DNS name."
  value       = aws_lb.services.dns_name
}

output "app_load_balancer_arn" {
  description = "The application load balancer ARN."
  value       = aws_lb.services.arn
}

output "app_load_balancer_security_group_id" {
  description = "The security group ID for the application load balancer."
  value       = aws_security_group.load_balancer.id
}

output "redis_host" {
  description = "Host name of Redis instance."
  value       = aws_elasticache_cluster.elasticache.cache_nodes[0].address
}

output "redis_port" {
  description = "Port of Redis instance."
  value       = aws_elasticache_cluster.elasticache.cache_nodes[0].port
}
