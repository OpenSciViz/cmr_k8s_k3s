module "cmr_database" {
  source                  = "../../modules/cmr_database"
  environment_name        = var.environment_name
  vpc_id                  = data.aws_vpc.app.id
  subnet_ids              = data.aws_subnets.app.ids
  snapshot_id             = var.snapshot_id
  master_db_username      = var.master_db_username
  master_db_password      = var.master_db_password
  instance_type           = local.merged_envs[var.environment_name].rds_instance_type
  backup_retention_period = local.merged_envs[var.environment_name].backup_retention_period
  delete_protection       = local.merged_envs[var.environment_name].delete_protection
  max_allocated_storage   = local.merged_envs[var.environment_name].max_allocated_storage
  permissions_boundary_name = var.permissions_boundary_name
}
