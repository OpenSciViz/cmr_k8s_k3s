terraform {
  required_version = "1.0.11"
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.12.0"
    }
    external = {
      source = "hashicorp/external"
      version = "2.2.0"
    }
  }
}

provider "aws" {
  region = var.aws_region

  ignore_tags {
    key_prefixes = ["gsfc-ngap"]
  }
}
