variable "environment_name" {
  description = "environment name, e.g. sit, uat, prod"
}

variable "aws_region" {
  description = "aws region to deploy to"
  default = "us-east-1"
}

variable "use_workload" {
  description = "true to setup workload backend. false to ignore."
  type        = bool
}
