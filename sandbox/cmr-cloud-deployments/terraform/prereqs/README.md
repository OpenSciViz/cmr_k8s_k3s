# Pre-requisites

This directory documents the resources that need to exist before attempting to
run terraform for the first time. We need to define an S3 bucket and a DynamoDB
table to use for remote Terraform state and locking so that only one deployment
can run at a time.
