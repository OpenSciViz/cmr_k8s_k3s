resource "aws_s3_bucket" "tf_state" {
  bucket = "tf-state-cmr-${var.environment_name}"
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name         = "terraform-state-lock-cmr-${var.environment_name}"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "DynamoDB Terraform State Lock Table for CMR ${var.environment_name}"
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-core-infrastructure-state-lock" {
  name         = "terraform-core-infrastructure-state-lock-cmr-${var.environment_name}"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "DynamoDB Terraform State Lock Table for CMR core infrastructure in ${var.environment_name}"
  }
}

resource "aws_dynamodb_table" "dynamodb-terraform-workload-state-lock" {
  count        = var.use_workload ? 1 : 0
  name         = "terraform-workload-state-lock-cmr-${var.environment_name}"
  hash_key     = "LockID"
  billing_mode = "PAY_PER_REQUEST"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name = "DynamoDB Terraform State Lock Table for CMR workload in ${var.environment_name}"
  }
}
