data "aws_caller_identity" "current" {
}

data "aws_region" "current" {
}

data "aws_vpc" "app" {
  filter {
    name   = "tag:Name"
    values = [var.vpc_tag_name_filter]
  }
}

data "aws_subnets" "app" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.app.id]
  }

  tags = {
    Name = var.subnet_tag_name_filter
  }
}

data "aws_security_group" "default" {
  vpc_id = data.aws_vpc.app.id
  name   = "default"
}

# If no zip path is given, create a dummy zip file to provision the lambda with.
data "archive_file" "dummy_browse_scaler" {
  count       = var.browse_scaler_zip_path == "" ? 1 : 0
  type        = "zip"
  output_path = "${path.module}/dummy_browse_scaler.zip"
  source {
    content  = "Dummy ZIP file to provision the lambda function."
    filename = "dummy_browse_scaler.txt"
  }
}

data "aws_iam_policy_document" "cmr_alerts" {
  policy_id = "CMR Alerts policy ID"

  statement {
    sid = "CMR Alerts account permissions"

    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:AddPermission",
    ]

    effect    = "Allow"
    resources = [aws_sns_topic.cmr_alerts.arn]

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }

    condition {
      test     = "StringEquals"
      variable = "AWS:SourceOwner"

      values = [
        data.aws_caller_identity.current.account_id,
      ]
    }
  }

  statement {
    sid       = "Allow CloudwatchEvents"
    actions   = ["sns:Publish"]
    resources = [aws_sns_topic.cmr_alerts.arn]

    principals {
      type        = "Service"
      identifiers = ["events.amazonaws.com"]
    }
  }

  statement {
    sid       = "Allow RDS Event Notification"
    actions   = ["sns:Publish"]
    resources = [aws_sns_topic.cmr_alerts.arn]

    principals {
      type        = "Service"
      identifiers = ["rds.amazonaws.com"]
    }
  }
}
