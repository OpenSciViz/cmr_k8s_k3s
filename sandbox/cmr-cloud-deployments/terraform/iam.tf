#
#
# Microservice related roles/policies
#

resource "aws_iam_policy" "ecsReadParameterStorePolicy" {
  name        = "ecsReadParameterStorePolicy-${var.environment_name}"
  description = "Read parameter store parameters policy."

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ssm:GetParameters",
        "secretsmanager:GetSecretValue"
      ],

      "Resource": ["arn:aws:ssm:*:*:parameter/${var.environment_name}/*"]
    }
  ]
}
EOF

}

resource "aws_iam_role" "ecs-role" {
  count = var.create_ecs_role ? 1 : 0
  name  = "${var.environment_name}-ecs-role"

  assume_role_policy = jsonencode(
    {
      Version : "2012-10-17",
      Statement : [
        {
          Action : "sts:AssumeRole",
          Principal : {
            Service : ["ecs-tasks.amazonaws.com", "ec2.amazonaws.com"]
          },
          Effect : "Allow",
          Sid : ""
        }
      ]
    }
  )
  permissions_boundary = local.permissions_boundary
}

locals {
  ecs_role_name = var.create_ecs_role ? aws_iam_role.ecs-role[0].name : "ecsTaskExecutionRole"
}

resource "aws_iam_role_policy_attachment" "attachEcsPolicy" {
  role       = local.ecs_role_name
  policy_arn = aws_iam_policy.ecsReadParameterStorePolicy.arn
}

resource "aws_iam_role_policy_attachment" "s3-policy-attach" {
  count      = var.create_ecs_role ? 1 : 0
  role       = local.ecs_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
}

resource "aws_iam_role_policy_attachment" "ssm-policy-attach" {
  count      = var.create_ecs_role ? 1 : 0
  role       = local.ecs_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMFullAccess"
}

resource "aws_iam_role_policy_attachment" "ecs-task-policy-attach" {
  count      = var.create_ecs_role ? 1 : 0
  role       = local.ecs_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}

resource "aws_iam_role_policy_attachment" "ecr-task-policy-attach" {
  count      = var.create_ecs_role ? 1 : 0
  role       = local.ecs_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}

resource "aws_iam_role_policy_attachment" "ec2-task-policy-attach" {
  count      = var.create_ecs_role ? 1 : 0
  role       = local.ecs_role_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
}

resource "aws_iam_role" "service" {
  name                 = "service-${var.environment_name}"
  permissions_boundary = local.permissions_boundary

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "service" {
  name        = "serivce-${var.environment_name}"
  description = "Allows microservices to access AWS servcices"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:*",
        "rds:*",
        "sns:*",
        "sqs:*",
        "elasticache:*"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "service" {
  role       = aws_iam_role.service.name
  policy_arn = aws_iam_policy.service.arn
}
