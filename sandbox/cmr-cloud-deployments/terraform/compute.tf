resource "aws_ecs_cluster" "services" {
  name = "cmr-service-${var.environment_name}"
}

resource "aws_ecr_repository" "cmr_core" {
  name = "cmr-core-${var.environment_name}"
}

resource "aws_ecr_repository" "cmr_opensearch" {
  name = "cmr-opensearch-${var.environment_name}"
}

resource "aws_ecr_repository" "cmr_legacy_services" {
  name = "cmr-legacy-services-${var.environment_name}"
}

resource "aws_ecr_repository" "cmr_service_bridge" {
  name = "cmr-service-bridge-${var.environment_name}"
}

resource "aws_ecr_repository" "cmr_kibana" {
  name = "cmr-kibana-${var.environment_name}"
}

module "opensearch_elasticache" {
  source           = "./modules/opensearch_elasticache"
  environment_name = var.environment_name
  subnet_ids       = data.aws_subnets.app.ids
  vpc_id           = data.aws_vpc.app.id
}

module "opensearch" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "opensearch"
  image                     = local.cmr_opensearch_image_url
  command                   = ["bundle", "exec", "rails", "s", "-p", "80", "-b", "0.0.0.0"]
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 1024
  memory                    = 4096
  task_count                = local.merged_envs[var.environment_name].opensearch_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].opensearch_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 86
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 0
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "opensearch", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "echo_rest_endpoint",
      "value" : "${data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name}/legacy-services/rest/"
    },
    {
      "name" : "REDIS_URL",
      "value" : module.opensearch_elasticache.opensearch_redis_url
    },
    {
      "name" : "REDIS_PORT",
      "value" : module.opensearch_elasticache.opensearch_redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "access_control" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "access-control"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 256
  memory                    = 1024
  command                   = ["java", "-Xmx512m", "-Xms512m", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.access-control.runner"]
  task_count                = local.merged_envs[var.environment_name].access_control_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].access_control_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 98
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 0
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "access_control", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_APP_ENVIRONMENT",
      "value" : var.environment_name
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "metadata_db" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "metadata-db"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 2048
  memory                    = 8192
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx6g", "-Xms6g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.metadata-db.runner"]
  task_count                = local.merged_envs[var.environment_name].metadata_db_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].metadata_db_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 99
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 1
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "metadata_db", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_DB_URL",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.db_url
    },
    {
      "name" : "CMR_APP_ENVIRONMENT",
      "value" : var.environment_name
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "bootstrap" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "bootstrap"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 1024
  memory                    = 2048
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx1536m", "-Xms1536m", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.bootstrap.runner"]
  task_count                = local.merged_envs[var.environment_name].bootstrap_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].bootstrap_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 93
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 1
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "bootstrap", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEX_SET_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEXER_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SEARCH_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_DB_URL",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.db_url
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "indexer" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "indexer"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 2048
  memory                    = 7168
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx6g", "-Xms6g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.indexer.runner"]
  task_count                = local.merged_envs[var.environment_name].indexer_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].indexer_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 95
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 1
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "indexer", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEX_SET_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SEARCH_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "search" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "search"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 2048
  memory                    = 7168
  command                   = ["java", "-Dhsqldb.method_class_names=\"abc\"", "-XX:-OmitStackTraceInFastThrow", "-Xmx6g", "-Xms6g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.search.runner"]
  task_count                = local.merged_envs[var.environment_name].search_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].search_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 94
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 0
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "search", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEX_SET_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEXER_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SEARCH_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_DB_URL",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.db_url
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "ingest" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "ingest"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 1024
  memory                    = 5120
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx4g", "-Xms4g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.ingest.runner"]
  task_count                = local.merged_envs[var.environment_name].ingest_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].ingest_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 92
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 0
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "ingest", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEX_SET_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEXER_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SEARCH_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_DB_URL",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.db_url
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

## Mock Echo can be used in NON-PRODUCTION environments
module "mock_echo" {
  source                = "./modules/microservice"
  environment_name      = var.environment_name
  service_name          = "mock-echo"
  health_check_path     = "/availability"
  image                 = local.cmr_core_image_url
  cluster_name          = aws_ecs_cluster.services.name
  subnet_ids            = data.aws_subnets.app.ids
  cpu                   = 1024
  memory                = 5120
  command               = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx4g", "-Xms4g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.mock-echo.runner"]
  task_count            = 1
  vpc_id                = data.aws_vpc.app.id
  security_group_id     = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn       = aws_lb_listener.services.arn
  lb_rule_priority      = 97
  service_port          = 80
  task_role_arn         = aws_iam_role.service.arn
  is_private            = 0
  service_scaler_name   = aws_lambda_function.service_scaler.function_name
  service_scaler_arn    = aws_lambda_function.service_scaler.arn
  schedule              = lookup(lookup(local.merged_schedules, var.environment_name, {}), "mock-echo", [])
  splunk_forwarding_arn = local.splunk_forwarding_arn
  sns_alarm_topic_arn   = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter   = var.vpc_tag_name_filter
  ecs_role_name         = local.ecs_role_name
  depends_on            = [aws_ecs_cluster.services]
}

module "legacy_services" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "legacy-services"
  image                     = local.cmr_legacy_services_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 2048
  memory                    = 6144
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx4g", "-Xms4g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.legacy-services.runner"]
  task_count                = local.merged_envs[var.environment_name].legacy_services_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].legacy_services_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 90
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 0
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "legacy_services", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_LEGACY_SERVICES_INTERNAL_SERVER_URL",
      "value" : "http://${data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name}"
    },
    {
      "name" : "CMR_CMR_VIRTUAL_PRODUCT_ENDPOINT",
      "value" : "http://${data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name}/virtual-product"
    },
    {
      "name" : "CMR_CMR_SEARCH_ENDPOINT",
      "value" : "http://${data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name}/search"
    },
    {
      "name" : "CMR_SOAP_ENDPOINT",
      "value" : "http://${data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name}/legacy-services"
    },
    {
      "name" : "CMR_CMR_PROVIDER_REST_ROOT_URL",
      "value" : "http://${data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name}/ingest"
    },
    {
      "name" : "CMR_DB_URL",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.db_url
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "virtual_product" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "virtual-product"
  image                     = local.cmr_core_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 512
  memory                    = 1024
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx512m", "-Xms512m", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.virtual-product.runner"]
  task_count                = local.merged_envs[var.environment_name].virtual_product_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].virtual_product_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 91
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 1
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "virtual_product", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INGEST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_INDEXER_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_METADATA_DB_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SEARCH_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_REDIS_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_host
    },
    {
      "name" : "CMR_REDIS_PORT",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.redis_port
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "service_bridge" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "service-bridge"
  image                     = local.cmr_service_bridge_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 2048
  memory                    = 5120
  command                   = ["java", "-XX:-OmitStackTraceInFastThrow", "-Xmx4g", "-Xms4g", "-cp", "cmr-standalone.jar", "clojure.main", "-m", "cmr.opendap.core"]
  task_count                = local.merged_envs[var.environment_name].service_bridge_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].service_bridge_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 88
  service_port              = 80
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 0
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "service_bridge", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  additional_env = [
    {
      "name" : "CMR_ACCESS_CONTROL_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SERVICE_BRIDGE_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_ECHO_REST_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    },
    {
      "name" : "CMR_SEARCH_HOST",
      "value" : data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_dns_name
    }
  ]
  depends_on = [aws_ecs_cluster.services]
}

module "kibana" {
  source                    = "./modules/microservice"
  environment_name          = var.environment_name
  service_name              = "kibana"
  image                     = local.cmr_kibana_image_url
  cluster_name              = aws_ecs_cluster.services.name
  subnet_ids                = data.aws_subnets.app.ids
  cpu                       = 512
  memory                    = 2048
  command                   = ["./start_kibana.sh"]
  task_count                = local.merged_envs[var.environment_name].kibana_task_count
  cpu_utilization_threshold = local.merged_envs[var.environment_name].kibana_cpu_utilization_threshold
  vpc_id                    = data.aws_vpc.app.id
  security_group_id         = data.terraform_remote_state.core_infrastructure.outputs.app_load_balancer_security_group_id
  lb_listener_arn           = aws_lb_listener.services.arn
  lb_rule_priority          = 70
  service_port              = var.kibana_port
  task_role_arn             = aws_iam_role.service.arn
  is_private                = 1
  service_scaler_name       = aws_lambda_function.service_scaler.function_name
  service_scaler_arn        = aws_lambda_function.service_scaler.arn
  schedule                  = lookup(lookup(local.merged_schedules, var.environment_name, {}), "kibana", [])
  splunk_forwarding_arn     = local.splunk_forwarding_arn
  sns_alarm_topic_arn       = aws_sns_topic.cmr_alerts.arn
  additional_env            = []
  vpc_tag_name_filter       = var.vpc_tag_name_filter
  ecs_role_name             = local.ecs_role_name
  depends_on                = [aws_ecs_cluster.services]
}
