locals {
  cmr_core_image_url            = "${aws_ecr_repository.cmr_core.repository_url}:${var.cmr_version}"
  cmr_opensearch_image_url      = "${aws_ecr_repository.cmr_opensearch.repository_url}:${var.cmr_opensearch_version}"
  cmr_legacy_services_image_url = "${aws_ecr_repository.cmr_legacy_services.repository_url}:${var.cmr_heritage_version}"
  cmr_service_bridge_image_url  = "${aws_ecr_repository.cmr_service_bridge.repository_url}:${var.cmr_service_bridge_version}"
  cmr_kibana_image_url          = "${aws_ecr_repository.cmr_kibana.repository_url}:${var.cmr_kibana_version}"
  permissions_boundary          = var.permissions_boundary_name != null ? "arn:aws:iam::${data.aws_caller_identity.current.account_id}:policy/${var.permissions_boundary_name}" : null

  envs = {
    sit = {
      access_control_task_count                 = 1
      bootstrap_task_count                      = 1
      indexer_task_count                        = 1
      ingest_task_count                         = 1
      legacy_services_task_count                = 1
      metadata_db_task_count                    = 2
      opensearch_task_count                     = 1
      search_task_count                         = 1
      virtual_product_task_count                = 1
      service_bridge_task_count                 = 1
      kibana_task_count                         = 1
      access_control_cpu_utilization_threshold  = 80
      bootstrap_cpu_utilization_threshold       = 80
      indexer_cpu_utilization_threshold         = 95
      ingest_cpu_utilization_threshold          = 80
      legacy_services_cpu_utilization_threshold = 80
      metadata_db_cpu_utilization_threshold     = 80
      opensearch_cpu_utilization_threshold      = 80
      search_cpu_utilization_threshold          = 95
      virtual_product_cpu_utilization_threshold = 80
      service_bridge_cpu_utilization_threshold  = 80
      kibana_cpu_utilization_threshold          = 80
      virtual_product_queue_threshold           = 50000
      collection_all_revision_queue_threshold   = 50000
      indexing_queue_threshold                  = 50000
      access_control_queue_threshold            = 50000
      cmr_alerts_mailing_list                   = []
    }
    uat = {
      access_control_task_count                 = 2
      bootstrap_task_count                      = 1
      indexer_task_count                        = 2
      ingest_task_count                         = 2
      legacy_services_task_count                = 2
      metadata_db_task_count                    = 2
      opensearch_task_count                     = 2
      search_task_count                         = 2
      virtual_product_task_count                = 2
      service_bridge_task_count                 = 1
      kibana_task_count                         = 1
      access_control_cpu_utilization_threshold  = 80
      bootstrap_cpu_utilization_threshold       = 80
      indexer_cpu_utilization_threshold         = 95
      ingest_cpu_utilization_threshold          = 80
      legacy_services_cpu_utilization_threshold = 80
      metadata_db_cpu_utilization_threshold     = 95
      opensearch_cpu_utilization_threshold      = 80
      search_cpu_utilization_threshold          = 95
      virtual_product_cpu_utilization_threshold = 80
      service_bridge_cpu_utilization_threshold  = 80
      kibana_cpu_utilization_threshold          = 80
      virtual_product_queue_threshold           = 50000
      collection_all_revision_queue_threshold   = 50000
      indexing_queue_threshold                  = 50000
      access_control_queue_threshold            = 50000
      cmr_alerts_mailing_list                   = ["cmr-dev@lists.nasa.gov"]
    }
    wl = {
      access_control_task_count                 = 3
      bootstrap_task_count                      = 0
      indexer_task_count                        = 5
      ingest_task_count                         = 5
      legacy_services_task_count                = 5
      metadata_db_task_count                    = 3
      opensearch_task_count                     = 0
      search_task_count                         = 5
      virtual_product_task_count                = 2
      service_bridge_task_count                 = 0
      kibana_task_count                         = 1
      access_control_cpu_utilization_threshold  = 80
      bootstrap_cpu_utilization_threshold       = 80
      indexer_cpu_utilization_threshold         = 80
      ingest_cpu_utilization_threshold          = 80
      legacy_services_cpu_utilization_threshold = 80
      metadata_db_cpu_utilization_threshold     = 80
      opensearch_cpu_utilization_threshold      = 80
      search_cpu_utilization_threshold          = 80
      virtual_product_cpu_utilization_threshold = 80
      service_bridge_cpu_utilization_threshold  = 80
      kibana_cpu_utilization_threshold          = 80
      virtual_product_queue_threshold           = 50000
      collection_all_revision_queue_threshold   = 50000
      indexing_queue_threshold                  = 50000
      access_control_queue_threshold            = 50000
      cmr_alerts_mailing_list                   = []
    }
    smwl = {
      access_control_task_count                 = 1
      bootstrap_task_count                      = 0
      indexer_task_count                        = 1
      ingest_task_count                         = 1
      legacy_services_task_count                = 1
      metadata_db_task_count                    = 1
      opensearch_task_count                     = 0
      search_task_count                         = 1
      virtual_product_task_count                = 1
      service_bridge_task_count                 = 0
      kibana_task_count                         = 1
      access_control_cpu_utilization_threshold  = 80
      bootstrap_cpu_utilization_threshold       = 80
      indexer_cpu_utilization_threshold         = 80
      ingest_cpu_utilization_threshold          = 80
      legacy_services_cpu_utilization_threshold = 80
      metadata_db_cpu_utilization_threshold     = 80
      opensearch_cpu_utilization_threshold      = 80
      search_cpu_utilization_threshold          = 80
      virtual_product_cpu_utilization_threshold = 80
      service_bridge_cpu_utilization_threshold  = 80
      kibana_cpu_utilization_threshold          = 80
      virtual_product_queue_threshold           = 50000
      collection_all_revision_queue_threshold   = 50000
      indexing_queue_threshold                  = 50000
      access_control_queue_threshold            = 50000
      cmr_alerts_mailing_list                   = []
    }
    prod = {
      access_control_task_count                 = 3
      bootstrap_task_count                      = 1
      indexer_task_count                        = 5
      ingest_task_count                         = 5
      legacy_services_task_count                = 5
      metadata_db_task_count                    = 3
      opensearch_task_count                     = 2
      search_task_count                         = 5
      virtual_product_task_count                = 2
      service_bridge_task_count                 = 1
      kibana_task_count                         = 1
      access_control_cpu_utilization_threshold  = 80
      bootstrap_cpu_utilization_threshold       = 80
      indexer_cpu_utilization_threshold         = 95
      ingest_cpu_utilization_threshold          = 80
      legacy_services_cpu_utilization_threshold = 80
      metadata_db_cpu_utilization_threshold     = 80
      opensearch_cpu_utilization_threshold      = 80
      search_cpu_utilization_threshold          = 95
      virtual_product_cpu_utilization_threshold = 80
      service_bridge_cpu_utilization_threshold  = 80
      kibana_cpu_utilization_threshold          = 80
      virtual_product_queue_threshold           = 100000
      collection_all_revision_queue_threshold   = 50000
      indexing_queue_threshold                  = 50000
      access_control_queue_threshold            = 50000
      cmr_alerts_mailing_list                   = ["cmr-dev@lists.nasa.gov"]
    }
  }

  splunk_forwarding_arn = var.use_splunk ? "arn:aws:logs:us-east-1:353585529927:destination:/gsfc-ngap-managed/application_logs_destination/${data.aws_caller_identity.current.account_id}" : null


  backend_type = fileexists("${path.module}/terraform_backend.tf") ? "s3" : "local"

  # The schedule expression should be in cron format. See:
  # https://docs.aws.amazon.com/lambda/latest/dg/tutorial-scheduled-events-schedule-expressions.html
  schedules = {
    sit = {
      # We spin up an extra MDB instance in SIT to workoff the daily re-indexing job.
      # A single MDB instance is overwhelmed causing many indexing exceptions.
      metadata_db = [
        {
          task_count    = 2
          starting_cron = "cron(0 12 * * ? *)"
          ending_cron   = "cron(0 14 * * ? *)"
        }
      ]
    }
  }

  # Terraform is complaining when using lookup() with a map variable.
  # Encoding then decoding seems to solve the issue.
  merged_schedules = jsondecode(jsonencode(merge(local.schedules, var.schedules)))

  merged_envs = merge(local.envs, var.envs)
}
