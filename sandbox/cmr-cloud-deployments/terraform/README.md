# Terraform

We'll initially organize our Terraform code to use modules and deployments.
Modules will contain the code to setup logical self-contained components, which
will then be composed in deployments to define a deployment of those modules.

We will store the Terraform state in a central location to coordinate any
updates to resources.

As we identify best practices we'll document them in this README.

# Dynamic configuration for microservices

Some of the parameters for the microservices module can be set for each
environment by creating a `services.auto.tfvars` file in `terraform` directory
(this file is in `.gitignore`).
For example:

```
envs = {
  sit = {
    metadata_db_task_count = 1
  }
  foo = {
    metadata_db_task_count = 2
  }
}
```

would set the task count to 1 for the `sit` deployment and to 2 for the `foo`
deployment.

# Environment variable overrides for microservices

The microservices read their environment variables from AWS Parameter Store, but
sometimes you may want to override these. This can be accomplished by adding an
`additional_env` parameter to the service instance definition as follows:

```
additional_env = [
  {
    "name": "CMR_ACCESS_CONTROL_HOST",
    "value": "${aws_lb.services.dns_name}"
  },
  {
    "name": "CMR_DB_URL",
    "value": "${module.cmr_database.db_url}"
  }
]
```

# Deployments

There are two deployments in this repo. Within the deployments directory we
have a core infrastructure deployment. This deployment is separate because it
contains resources that are critical to not destroy (such as the RDS database
and application load balancer that is referenced from other accounts).

The other deployment is at the top level of the repo, which deploys all of the
other resources in our account.

# Moving Terraform state

We may want to refactor our deployments at some point in time. Here's the best
way to do this such that the state is preserved.

Make sure to run the generate-backend.sh to use remote states.

Get the top level deployment terraform state
```
(cd terraform && terraform state pull > ../applications.tfstate)
```

Get the core infrastructure deployment state
```
(cd terraform/deployments/core_infrastructure && terraform state pull > ../../../core_infrastructure.tfstate)
```

Move the cmr_database module from the applications state to the core
infrastructure state
```
terraform state mv -state=applications.tfstate -state-out=core_infrastructure.tfstate module.cmr_database module.cmr_database
```

Push the changes to the S3 remote state for core infrastructure
```
(cd terraform/deployments/core_infrastructure && terraform state push ../../../core_infrastructure.tfstate)
```

Verify state was moved successfully
```
(cd terraform/deployments/core_infrastructure && terraform state list | grep cmr_database)
```

Push up the changes to the S3 remote state for the top level deployment
```
(cd terraform && terraform state push ../applications.tfstate)
```

Run `terraform plan` from both deployments to ensure that the state move worked
correctly.

# Workload

To start workload drivers:

1. Setup core infrastructure. `(cd terraform/deployments/core_infrastructure && terraform apply)`
2. Setup core apps: `(cd terraform && terraform apply)`
3. Setup workload: `(cd terraform/deployments/workload && terraform apply)`

To destroy workload drivers:

1. Destroy workload: `(cd terraform/deployments/workload && terraform destroy)`
2. Destroy core apps: `(cd terraform && terraform destroy)`
3. Destroy core infrastructure: `(cd terraform/deployments/core_infrastructure && terraform destroy)`

# Task Module

The task module is used to deploy one-off fargate tasks. These tasks are not associated
with a service and do not utilize auto scaling features.

# Cron style scaling

Services can have their desired task count increased based on a cron-style schedule
by including a schedule in locals.tf or as an input variable. The schedule takes
the form of:

```
schedules = {
  <environment> = {
    <service_name> = [
      {
        task_count = <desired_task_count_for_time_period>
        starting_cron = cron(<cron-style-start-time>)
        ending_cron = cron(<cron-style-end-time>)
      }
      {
        task_count = ...
        ...
      }
    ]
  }
}
```

Start times / end times in UTC. For more information on cron style scheduling see:
https://docs.aws.amazon.com/lambda/latest/dg/tutorial-scheduled-events-schedule-expressions.html

NOTE: There is no sanity checking that start times / end times are reasonable.
The start time sets the task count where the end time returns the task count to the default.
If the start / end times are misconfigured or overlap with another schedule then this will cause poor side effects.

# Steps to bringing up your own environment

With all these steps please read the terraform planned changes before approving. If you are
creating an environment from scratch and you see terraform is trying to destroy or replace
things when running a fresh `terraform apply` stop what you are doing and make sure you're not
clobbering some other existing environment.

Also please note this guide does not set up elasticsearch. See cmr-cloud-elasticsearch repo for details.

1. It is best to ensure you are starting with a clean environment. To do so
remove any `.terraform` directories and `terraform_backend.tf`
files. From the top level directory you can simply run:  
`find . \( -name .terraform -o -type f -name terraform_backend.tf \) -exec rm -rf {} +`
2. Configure your AWS environment variables to the correct account (set `AWS_PROFILE`).
3. Decide on whether you want to run via local state or remote state. If local,
proceed to step 4. If you want to run a remote state:
    * `cd terraform/prereqs && terraform init && terraform apply && cd -`
    Fill in the input prompts with the required information. If you intend to deploy
    workload drivers input `true` for `use_workload`.
    * `./scripts/generate-backend.sh <ENVIRONMENT_NAME_FROM_ABOVE>`
4. Populate parameter store with any environment variables needed to run CMR. The environment
variables should be in the form of `/ENVIRONMENT_NAME/APP_NAME/ENVIRONMENT_VARIABLE_NAME`.
See `populate-parameter-store` project for more details.
    * `CMR_SQS_ENDPOINT` and `CMR_SNS_ENDPOINT` should be set explicitly or [queues and topics will default](https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/java-dg-region-selection.html#region-selection-choose-endpoint) to __us-east-1__
      * e.g. if deploying to __us-west-2__ `https://sqs.us-west-2.amazonaws.com` and `https://sns.us-west-2.amazonaws.com` respectively
5. Deploy the database and create the load balancer:  
    * `cd terraform/deployments/core_infrastructure`
    * Create a `terraform.tfvars` file. This file should contain `environment_name`, an
    `envs` variable map, and a `snapshot_id`. See `locals.tf` for an example of `envs`.
    Refer to `inputs.tf` for any other required or optional inputs.
    * `terraform init && terraform apply && cd -`
6. Deploy app infrastructure:
    * `cd terraform`
    * Create a `terraform.tfvars` file. Similarly to step 5 this file should contain
    `environment_name` as wells as an `envs` variable map. See `locals.tf` for an example.
    And refer to `inputs.tf` for any other required or optional inputs.
    * `terraform init && terraform apply && cd -`
7. Push images to ECR:
    * `./scripts/ecr.sh <DOCKERFILE_PATH> <RELEASE_VERSION> <ECR_REPO_NAME> <ECR_REPO_URL>`
    * You can get the above ECR repo name and URL from the associated terraform output. Ex for
    cmr_core it would be `terraform output cmr_core_repository_name` and `terraform output cmr_core_repository_url`.
8. If you did not change `cmr_*version` terraform input variables when doing step 6 and
pushed step 7 images with a RELEASE_VERSION as `latest` your apps will eventually start.
However if you want to / pushed with a different version name repeat step 6 but set the
version names in your `terraform.tfvars` for the images you pushed in step 7. Example:  
If you pushed an image for cmr_core with release version `RELEASE_1` you would set
`cmr_version = "RELEASE_1"` in your `terraform.tfvars` and then re-run `terraform apply`.
Or `terraform apply -target=module.<module_name>` if you only care about certain apps.
