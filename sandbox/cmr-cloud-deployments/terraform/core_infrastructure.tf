data "terraform_remote_state" "core_infrastructure" {
  backend = local.backend_type
  config = {
    for key, value in {
      bucket         = local.backend_type == "s3" ? "tf-state-cmr-${var.environment_name}" : null
      key            = local.backend_type == "s3" ? "core-infrastructure-state-${var.environment_name}" : null
      region         = local.backend_type == "s3" ? var.aws_region : null
      dynamodb_table = local.backend_type == "s3" ? "terraform-core-infrastructure-state-lock-cmr-${var.environment_name}" : null
      path           = local.backend_type == "local" ? "${path.module}/deployments/core_infrastructure/terraform.tfstate" : null
    } :
    key => value
    if value != null
  }
}
