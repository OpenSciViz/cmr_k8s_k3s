resource "aws_lambda_function" "service_scaler" {
  vpc_config {
    subnet_ids         = data.aws_subnets.app.ids
    security_group_ids = [aws_security_group.service.id]
  }

  filename         = data.archive_file.service_scaler.output_path
  source_code_hash = filebase64sha256(data.archive_file.service_scaler.output_path)
  function_name    = "service-scaler-${var.environment_name}"
  handler          = "service_scaler.handler"
  role             = aws_iam_role.service_scaler.arn
  runtime          = "python3.7"
}

data "archive_file" "service_scaler" {
  type        = "zip"
  output_path = "${path.module}/service_scaler.zip"
  source {
    filename = "service_scaler.py"
    content  = <<EOF
import boto3

def handler(event, context):
    cluster = event['cluster']
    service = event['service']
    task_count = event['task_count']
    ecs_client = boto3.client('ecs', region_name='${data.aws_region.current.name}')
    response=ecs_client.update_service(cluster=cluster, service=service, desiredCount=int(task_count))
    print(response)
EOF
  }
}

resource "aws_iam_role" "service_scaler" {
  name                 = "service-scaler-${var.environment_name}"
  permissions_boundary = local.permissions_boundary

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "service_scaler" {
  name        = "service-scaler-${var.environment_name}"
  description = "Allows lambda to update ecs service"
  policy      = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "ecs:UpdateService",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "service_scaler" {
  role       = aws_iam_role.service_scaler.name
  policy_arn = aws_iam_policy.service_scaler.arn
}

resource "aws_iam_role_policy_attachment" "service_scaler_vpc" {
  role       = aws_iam_role.service_scaler.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
