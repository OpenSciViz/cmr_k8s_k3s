import * as AWS from 'aws-sdk'; import { env } from 'process';
import { promisify } from 'util';
import {
  keyExists,
  setKeyWithExpire
} from './cache'

const sns = new AWS.SNS();
const publish = promisify(sns.publish.bind(sns));
const REDIS_THROTTLE_PERIOD = Number.parseInt(env.REDIS_THROTTLE_PERIOD || '3600', 10);

/**
 * Publish to SNS with throttle. If msg does not exists in cache, publish it and
 * set msg in cache with expire REDIS_THROTTLE_PERIOD. Otherwise do not publish.
 * @param msg - Message to publish
 */
 export async function sendAlert(event: any): Promise<void> {

    // Build SNS Email message from CloudWatch Event json body
    const detail = event.detail;
    const container = event.detail.containers[0];
    const msg = "App container for " + detail.group +
                " has status = " + container.lastStatus +
                ", with code = " + detail.stopCode +
                ", reason = " + detail.stoppedReason +
                ((container.reason) ? " " + container.reason : ".");

    // Create key name from service name, check if key exists in REDIS
    const key = "container-alert: " + detail.group;
    const exists = await keyExists(key);

    // If we don't see a key, publish message to SNS then create entry in REDIS with ttl set.
    if (!exists) {
      await publish({
        Message: msg,
        TopicArn: env.SNS_TOPIC_ARN
      });
      await setKeyWithExpire(key, '', REDIS_THROTTLE_PERIOD);
    }
}
