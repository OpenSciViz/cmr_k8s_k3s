import * as redis from 'redis';
import { promisify } from 'util';

const client = redis.createClient({
  // eslint-disable-next-line @typescript-eslint/camelcase
  return_buffers: true,
  host: process.env.REDIS_HOST || '127.0.0.1',
  port: Number.parseInt(process.env.REDIS_PORT || '6379', 10),
});

const existsAsync = promisify(client.exists).bind(client);
const setexAsync = promisify(client.setex).bind(client);

/**
 * Check if key exists in cache
 * @param key - The key to check
 */
export const keyExists = async (key: string): Promise<boolean> => {
  return (await existsAsync(key)) == '1';
};

/**
 * Set key with expire.
 * @param key - key to set
 * @param value - value to set for key
 * @param expire - expire time of key in seconds
 */
export const setKeyWithExpire = async (key: string, value: string, expire: number): Promise<void> => {
  setexAsync(key, expire, value);
};
