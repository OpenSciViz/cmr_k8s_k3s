# Container Alerts Lambda

This is a [serverless](https://serverless.com) project that provides an AWS Lambda function that sends a message to an SNS topic when a container stops unexpectedly, throttling those messages in the process.
# Building the project

Run the following command:

```
npm install
```

# Deploying

Use serverless to deploy to AWS

```
export AWS_PROFILE=<YOUR PROFILE>
npm run deploy -- --stage <sit|uat|prod>
```

The deployment makes use of outputs from the main CMR Terraform deployments (via a CloudFormation stack).
