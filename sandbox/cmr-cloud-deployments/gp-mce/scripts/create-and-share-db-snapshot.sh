#!/bin/bash

# This script creates a manual database snapshot for the provided database ID
# and shares it with another account.
# Requires aws client and jq to be installed
# Requires setting AWS configuration environment variables as well as:
# SHARED_ACCOUNT_ID - the account ID to share the snapshot with
# DATABASE_ID - the database to create the snapshot for
if [[ -z "$SHARED_ACCOUNT_ID" ]] || [[ -z "$DATABASE_ID" ]]; then
  printf "$(date) ERROR: Both SHARED_ACCOUNT_ID and DATABASE_ID must be set. Exiting.\n"
  exit 1
fi

snapshot_timestamp=$(date +"%Y-%m-%dt%H-%M-%S")
manual_snapshot_id="${DATABASE_ID}-${snapshot_timestamp}"

# Find if there is an existing snapshot from prior runs that should be cleaned up
cleanup_snapshot_id="$(aws rds describe-db-snapshots \
                      --db-instance-identifier "$DATABASE_ID" \
                      --snapshot-type manual \
                      --query="reverse(sort_by(DBSnapshots, &SnapshotCreateTime))[0]|DBSnapshotIdentifier" \
                      --output text)"

printf "$(date) Snapshot to delete is $cleanup_snapshot_id\n"

# Create the manual snapshot
printf "$(date) Creating snapshot $manual_snapshot_id\n"
aws rds create-db-snapshot \
  --db-instance-identifier "$DATABASE_ID" \
  --db-snapshot-identifier "$manual_snapshot_id" \
  --output text

# Wait until the snapshot is in the available state before sharing
available=false
while [ $available == false ]
do
  status="$(aws rds describe-db-snapshots \
              --db-instance-identifier "$DATABASE_ID" \
              --db-snapshot-identifier "$manual_snapshot_id" \
              --query="DBSnapshots[0]|Status" \
              --output text)"
  if [ $status != "available" ]; then
    printf "$(date) Snapshot $manual_snapshot_id not yet ready with status ${status}. Waiting...\n"
    sleep 20
  else
    available=true
    printf "$(date) Snapshot $manual_snapshot_id is now available.\n"
  fi
done

# Share the new manual snapshot with the other account
printf "$(date) Modify DB snapshot attribute"
aws rds modify-db-snapshot-attribute \
  --db-snapshot-identifier $manual_snapshot_id \
  --attribute-name restore \
  --values-to-add "[\"$SHARED_ACCOUNT_ID\"]"

# Remove old snapshots from prior executions
# TODO: This is commented out because it seems like it might be a bad idea to delete an older
# snapshot that might still be relied on. We can manually clean up old snapshots when we need to.
# if [[ -z $cleanup_snapshot_id || "$cleanup_snapshot_id" == "None" ]]; then
#   printf "$(date) No manual database snapshots need to be cleaned up for ${DATABASE_ID}.\n"
# else
#   printf "$(date) Deleting snapshot ${cleanup_snapshot_id}.\n"
#   aws rds delete-db-snapshot --db-snapshot-identifier $cleanup_snapshot_id
# fi
