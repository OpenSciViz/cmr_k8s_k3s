import urllib, http, os, base64, urllib
PROXY_ENDPOINT = os.environ['PROXY_ENDPOINT']
PROXY_PATH = os.environ['PROXY_PATH']
BASE_PATH = os.environ['BASE_PATH']
PROXY_PORT= os.environ['PROXY_PORT']

def lambda_handler(event, context):
    if 'queryStringParameters' in event.keys() and event.get('queryStringParameters'):
        queryParams = event.get('queryStringParameters')
    else:
        queryParams = {}
    if 'headers' in event.keys() and event.get('headers'):
        headerValues = event.get('headers')
    else:
        headerValues = {}
    path = PROXY_PATH
    if 'path' in event.keys():
        event_path = event['path']
        if BASE_PATH in event['path']:
            event_path = event['path'].split(BASE_PATH)[-1]
        if not urllib.parse.urlencode(queryParams):
            path = PROXY_PATH + event_path
        else:
            path = PROXY_PATH + event_path + "?" + urllib.parse.urlencode(queryParams)
    if PROXY_PORT is not None:
        TARGET_ENDPOINT = PROXY_ENDPOINT + ":" + PROXY_PORT
    headerValues['host'] = TARGET_ENDPOINT
    headerValues['x-forwarded-proto'] = "https"
    headerValues['x-forwarded-port'] = "443"
    headerValues = strip_cloudfront_headers(headerValues)
    output = {}
    method = "GET"
    if 'httpMethod' in event.keys():
        method = event['httpMethod']
    originalRequestBody = event.get('body')
    requestBody = None
    if originalRequestBody is not None:
        try:
            print("originalRequestBody: ",base64.b64decode(originalRequestBody).decode('utf-8'))
            requestBody = base64.b64decode(originalRequestBody).decode('utf-8')
        except Exception:
            try:
                requestBody = originalRequestBody.encode('utf-8')
            except UnicodeEncodeError:
                requestBody = originalRequestBody
    print("TARGET_ENDPOINT: ", TARGET_ENDPOINT)
    print("method: ",method)
    print("path: ",path)
    print("requestBody: ",requestBody)
    print("headerValues: ",headerValues)
    connection = http.client.HTTPSConnection(TARGET_ENDPOINT)
    connection.request(method,
                        path,
                        requestBody,
                        headerValues)
    response = connection.getresponse()
    output['statusCode'] = response.status
    output['headers'] = dict((key, value) for key, value in response.getheaders())
    # Catch decoding errors in case we are sending bytes objects like images
    responseBody = response.read()
    output['body'] = responseBody
    try:
        output['body'] = responseBody.decode("utf-8")
    except UnicodeDecodeError:
        output['body'] = base64.b64encode(responseBody).decode('utf-8')
        output['isBase64Encoded'] = True
    # Add support for multi-value cookies
    cookie_headers = response.msg.get_all('Set-Cookie')
    if(cookie_headers is not None and len(cookie_headers) > 0):
        output['headers'] = multiple_cookie_header(cookie_headers, output['headers'])
    print("output: ", output)
    return output

def multiple_cookie_header(cookie_headers, headers):
    setCookie = "Set-Cookie"
    cookieCounter = 0
    for cookie in cookie_headers:
        indices = set([cookieCounter])
        setCookieVal = "".join(c.upper() if i in indices else c for i, c in enumerate(setCookie))
        cookieCounter = cookieCounter + 1
        if(cookieCounter == 3): # Skip special character
            cookieCounter = cookieCounter + 1
        headers[setCookieVal] = cookie
    return headers

def strip_cloudfront_headers(headers):
    if 'x-amz-cf-id' in headers.keys():
        del headers['x-amz-cf-id']
    if 'X-Amz-Cf-Id' in headers.keys():
        del headers['X-Amz-Cf-Id']
    return headers
