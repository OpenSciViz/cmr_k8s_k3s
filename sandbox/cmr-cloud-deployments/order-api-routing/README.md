# CMR API routing for CMR Ordering API

This uses the [Serverless Framework](https://serverless.com/) to define the deployment code for the routing lambda function and setting up ALB listener rule to expose Ordering API via the CMR application service API.

## Development Quick Start

### Prerequisites

* node.js 14
* AWS CLI

### Setup

`npm install -g serverless`
`npm install -g serverless-pseudo-parameters`

### Deploy

`serverless deploy --stage <environment name>`

For example: `serverless deploy --stage sit` will deploy a CloudFormation stack named `cmr-ordering-api-sit` that contains the routing lambda function and ALB listener rule, etc. in SIT environment.

### Roll back

`serverless remove --stage <environment name>`
