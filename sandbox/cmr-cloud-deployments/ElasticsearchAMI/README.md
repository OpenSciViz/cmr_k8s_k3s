# Packer Project to Create Elasticsearch Base AMI

## Prerequisites

### Packer

```sh
brew install packer
```

This project will create the base AMI for CMR Elasticsearch.

## Create the AMI

Run the following command
```
packer build packer.json
```
The following variables must be set:

| Variable                          | Example       |                                                                                                                                                                                                                                                                             |
|-----------------------------------|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `AWS_REGION`                      | us-east-1     |                                                                                                                                                                                                                                                                             |
| `SUBNET_ID`                       | subnet-XXXXXX |                                                                                                                                                                                                                                                                             |
| `VPC_ID`                          | vpc-XXXXXXX   |                                                                                                                                                                                                                                                                             |
| `SOURCE_AMI_ACCOUNT_ID`           | 18291XXXXX    |                                                                                                                                                                                                                                                                             |
| `SSH_BASTION_HOST`                |                                                                                                                                                                                                                                         |
| `SSH_BASTION_PRIVATE_KEY_FILE`    |               |                                                                                                                                                                                                                                                                             |
| `ES_SPATIAL_PLUGIN_PATH`          |               | Path to the ES spatial plugin .zip. This zip should contain the spatial plugin .jar and plugin-descriptor.properties. You can run `lein package-es-plugin` in `es-spatial-plugin` project to build this zip. This will create `target/cmr-es-spatial-plugin-<VERSION>.zip`. |
| `ES_SPATIAL_PLUGIN_DEPS_PATH`     |               | Path to ES spatial plugin dependencies .jar. This can be created by running `lein install-es-deps` in the `es-spatial-plugin` project. This will create `es-deps/cmr-es-spatial-plugin-deps-<VERSION>-standalone.jar`                                                       |
| `CMR_PLUGIN_SECURITY_POLICY_PATH` |               | Path to the security policy needed to run the spatial plugin. This should be located in `es-spatial-plugin/resources/plugin/plugin-security.policy`.                                                                                                                        |


The `elasticsearch.yml` and `elasticsearch` files are just place holders. They are replaced when standing
up the cluster nodes.
