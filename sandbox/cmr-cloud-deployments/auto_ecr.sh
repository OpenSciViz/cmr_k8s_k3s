#!/bin/bash
#
# To use: pipe the output of `tf output` to this file after running `tf apply`.
# Example (from the `terraform` directory):
# `tf output | ../auto_ecr.sh`

# This assumes your CMR repo is at the same level as the cmr-cloudwatch-repo and is named `Common-Metadata-Reposiotry`
awk '/core_repository_url/ {print substr($3, 46), $3}' - | xargs ../scripts/ecr.sh ../../Common-Metadata-Repository/dev-system/Dockerfile.ecs latest
