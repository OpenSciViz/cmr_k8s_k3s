>>/
DECLARE
hdnl NUMBER;
BEGIN
hdnl := DBMS_DATAPUMP.OPEN( operation => 'EXPORT', job_mode => 'SCHEMA', job_name=>'DB_BACKUP');
DBMS_DATAPUMP.ADD_FILE( handle => hdnl, filename => 'schema.dmp', directory => 'DATA_PUMP_DIR', filetype => dbms_datapump.ku$_file_type_dump_file,reusefile => 1);
DBMS_DATAPUMP.ADD_FILE( handle => hdnl, filename => 'schems_exp.log', directory => 'DATA_PUMP_DIR', filetype => dbms_datapump.ku$_file_type_log_file);
DBMS_DATAPUMP.METADATA_FILTER(hdnl,'SCHEMA_EXPR','IN (''APPQOSSYS'', ''CMR_BOOTSTRAP'', ''CMR_INGEST'', ''CTXSYS'', ''DBSNMP'', ''DIP'', ''I_10_0_TESTBED_BUSINESS'', ''METADATA_DB'', ''ORACLE'', ''OUTLN'', ''PUBLIC'', ''RDSADMIN'', ''READ_ALL'', ''RO_USER'', ''SYS'', ''SYSTEM'')');
DBMS_DATAPUMP.START_JOB(hdnl);
END;
/
