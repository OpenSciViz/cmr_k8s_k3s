# Elasticsearch Alerts Lambda

This is a [serverless](https://serverless.com) project that provides an AWS Lambda function that
monitors the status of an Elasticsearch cluster and alerts on various conditions. Alerts are sent
via an SNS topic.

# Building the project

Run the following command:

```
npm install
```

# Running Tests

Execute the `npm run test` command.

# Testing Locally

Use the serverless offline module to execute the lambda locally:

```
export AWS_PROFILE=<YOUR PROFILE>
SNS_TOPIC_ARN=<ARN FOR SNS TOPIC> NODE_COUNT=1 ./node_modules/.bin/sls invoke local -f es-alerts --data "{}" --config ./serverless-local.yml
```

You will need to execute it five times to fill the history in order to see alerts for CPU, JVM heap, and JVM memory pressure. Alternatively
you can export the environment variable `ES_HISTORY_WINDOW_SIZE` to something smaller than five.

This requires Redis and Elasticsearch available on local ports 6379 and 9200, respectively (via local instances or tunnels). It also requires an SNS topic for sending the alerts.

# Deploying

Use serverless to deploy to AWS

```
export AWS_PROFILE=<YOUR PROFILE>
export ES_CF_STACK_NAME=<optional override if the ES CloudFormation name is not 'cmr-{stage}-elasticsearch-cluster'>
NODE_COUNT=3 CPU=90 HEAP=80 PRESSURE=80 npm run deploy -- --stage <stage>
where <stage> can be sit, uat or prod
```

The deployment makes use of outputs from the main CMR Terraform deployments (via a CloudFormation stack).
