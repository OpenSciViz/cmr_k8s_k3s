import { createClient } from 'redis';

const REDIS_STATUS_KEY = 'ELASTICSEARCH_STATUS';
const REDIS_CPU_HISTORY_KEY = 'ES_CPU_HISTORY';
const REDIS_JVM_HEAP_HISTORY_KEY = 'ES_JVM_HEAP_HISTORY';
const REDIS_JVM_PRESSURE_HISTORY_KEY = 'ES_JVM_PRESSURE_HISTORY';

const host = process.env.REDIS_HOST || '127.0.0.1';
const port = Number.parseInt(process.env.REDIS_PORT || '6379', 10);
let client;

(async () => {
  client = createClient({
    // eslint-disable-next-line @typescript-eslint/camelcase
    url: `redis://${host}:${port}`
  });
  client.on('error', (err) => console.log('Redis Client Error', err));

  await client.connect();
})();

export const getPreviousStatus = async (): Promise<string> =>
  ((await client.get(REDIS_STATUS_KEY)) || '').toString();

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const setPreviousStatus = async (status: string): Promise<any> => {
  await client.set(REDIS_STATUS_KEY, status);
};

/**
 * Returns the cpu usage history of the ES cluster nodes
 */
export const cpuHistory = async (): Promise<Record<string, Array<number>>> => {
  const historyStr: string = ((await client.get(REDIS_CPU_HISTORY_KEY)) || '{}').toString();
  return JSON.parse(historyStr);
};

/**
 * Save the CPU usage history to the cache
 * @param history - The CPU usage history for each node
 */
export const saveCpuHistory = async (history: Record<string, Array<number>>): Promise<void> => {
  await client.set(REDIS_CPU_HISTORY_KEY, JSON.stringify(history));
};

/**
 * Returns the JVM heap history of the ES cluster nodes
 */
export const jvmHeapHistory = async (): Promise<Record<string, Array<number>>> => {
  const historyStr: string = ((await client.get(REDIS_JVM_HEAP_HISTORY_KEY)) || '{}').toString();
  return JSON.parse(historyStr);
};

/**
 * Save the JVM heap usage history to the cache
 * @param history - The JVM heap usage history for each node
 */
export const saveJvmHeapHistory = async (history: Record<string, Array<number>>): Promise<void> => {
  await client.set(REDIS_JVM_HEAP_HISTORY_KEY, JSON.stringify(history));
};

/**
 * Returns the JVM pressure history of the ES cluster nodes
 */
export const jvmPressureHistory = async (): Promise<Record<string, Array<number>>> => {
  const historyStr: string = ((await client.get(REDIS_JVM_PRESSURE_HISTORY_KEY)) || '{}').toString();
  return JSON.parse(historyStr);
};

/**
 * Save the JVM heap pressure history to the cache
 * @param history - The JVM heap pressure history for each node
 */
export const saveJvmPressureHistory = async (history: Record<string, Array<number>>): Promise<void> => {
  await client.set(REDIS_JVM_PRESSURE_HISTORY_KEY, JSON.stringify(history));
};

/**
 * Check if key exists in cache
 * @param key - The key to check
 */
export const keyExists = async (key: string): Promise<boolean> => {
  return (await client.exists(key));
};

/**
 * Set key with expire.
 * @param key - key to set
 * @param value - value to set for key
 * @param expire - expire time of key in seconds
 */
export const setKeyWithExpire = async (key: string, value: string, expire: number): Promise<void> => {
  await client.set(key, value, {'EX': expire});
};
