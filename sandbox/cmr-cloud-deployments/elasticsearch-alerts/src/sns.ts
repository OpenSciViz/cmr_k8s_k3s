import * as AWS from 'aws-sdk';
import { env } from 'process';
import { promisify } from 'util';
import {
  keyExists,
  setKeyWithExpire
} from './cache'

const sns = new AWS.SNS();
const publish = promisify(sns.publish.bind(sns));
const REDIS_THROTTLE_PERIOD = Number.parseInt(env.REDIS_THROTTLE_PERIOD || '3600', 10);

// eslint-disable-next-line import/prefer-default-export
/**
 * Publish to SNS with throttle. If msg does not exists in cache, publish it and
 * set msg in cache with expire REDIS_THROTTLE_PERIOD. Otherwise do not publish.
 * @param msg - Message to publish
 */
export async function sendAlert(msg: string): Promise<void> {
  const exists = await keyExists(msg);
  if (!exists) {
    publish({
      Message: msg,
      TopicArn: env.SNS_TOPIC_ARN
    });
    setKeyWithExpire(msg, '', REDIS_THROTTLE_PERIOD);
  }
}
