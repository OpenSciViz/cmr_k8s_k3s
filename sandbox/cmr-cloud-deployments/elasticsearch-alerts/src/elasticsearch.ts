/* eslint-disable @typescript-eslint/camelcase */
import { env } from 'process';
import got from 'got';
import * as log from 'loglevel';
import { URL } from 'url';

const elasticUrl = new URL(env.ELASTICSEARCH_LB_HOST_URL || 'http://127.0.0.1:9200/');
const elasticHealthUrl = `${elasticUrl}_cluster/health`;
const elasticStatsUrl = `${elasticUrl}_nodes/stats/jvm,os`;
const elasticAllStatsUrl = `${elasticUrl}_nodes/stats/adaptive_selection,breaker,discovery,fs,http,indexing_pressure,indices,ingest,jvm,os,process,thread_pool,transport`;
const elasticActiveTasksUrl = `${elasticUrl}_tasks`;
const elasticPendingTasksUrl = `${elasticUrl}_cluster/pending_tasks`
const elasticHotThreadsUrl = `${elasticUrl}_nodes/hot_threads?type=cpu&threads=25`
const elasticExplainUrl = `${elasticUrl}_cluster/allocation/explain`
const elasticShardsHealthUrl = `${elasticUrl}_cluster/health?level=shards`

export const elasticHealthStatus = async (): Promise<{ status: string; count: number }> => {
  let nodeCount = -1;
  let esStatus = 'unknown';
  try {
    const resp = await got.get(elasticHealthUrl);
    const { status, number_of_nodes } = JSON.parse(resp.body);
    esStatus = status;
    nodeCount = number_of_nodes;
  } catch (error) {
    log.error(error);
  }

  return { status: esStatus, count: nodeCount };
};

export const elasticStats = async (): Promise<string> => (await got.get(elasticStatsUrl)).body;

/**
 * Given the set of shards, return the explanations for why those shards do not have a green
 * status.
 * @param elasticShardsHealth - JSON string response body from elasticsearch health query
 * @returns A stringified JSON object containing the results of elasticsearch explain queries
 */
async function getShardExplanations(elasticShardsHealth: string): Promise<string> {
  const elasticShardsHealthJson = JSON.parse(elasticShardsHealth);

  const result: Array<string> = []
  for (const [indexKey, indexOjbect] of Object.entries(elasticShardsHealthJson["indices"])) {
    if (indexOjbect["status"] !== "green") {
      for (const [shardKey, shardObject] of Object.entries(indexOjbect["shards"])) {
        if (shardObject["status"] !== "green") {
          // If the shard is not in a healthy state, we want to query elasticsearch to find
          // out why that is.  If the primary shard is 'active' then it is healthy, so
          // we set "primary": false to check the replica instead. This should coincide with
          // shard status.  Yellow status means only replicas are down, Red status means primaries
          // are down (ingest not possible).
          const requestBody = {
            "index": indexKey,
            "primary": shardObject["primary_active"] === true ? false : true,
            "shard": shardKey
          }

          const shardExplainResponse = JSON.parse((await got.post(elasticExplainUrl, { json: requestBody })).body);

          result.push(shardExplainResponse)
        }
      }
    }
  }
  return JSON.stringify(result)
}

export const elasticStateReport = async (): Promise<{
  nodeStats: string,
  pendingTasks: string,
  activeTasks: string,
  hotThreads: string,
  shardExplanations: string
}> => {
  const allStats = (await got.get(elasticAllStatsUrl)).body;
  const pendingTasks = (await got.get(elasticPendingTasksUrl)).body;
  const activeTasks = (await got.get(elasticActiveTasksUrl)).body;
  const hotThreads = (await got.get(elasticHotThreadsUrl)).body;
  const shardExplanations = await getShardExplanations((await got.get(elasticShardsHealthUrl)).body);


  const result = {
    nodeStats: allStats,
    pendingTasks: pendingTasks,
    activeTasks: activeTasks,
    hotThreads: hotThreads,
    shardExplanations: shardExplanations
  }

  return result;
};