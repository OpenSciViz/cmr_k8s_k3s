/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/camelcase */
import {
  slideWindows,
  thresholdReached,
  cpuValues,
  cpuStats,
  jvmHeapValues,
  jvmHeapStats,
  jvmPressureValues,
  jvmPressureStats
} from '../stats';

const esStats = {
  cluster_name: 'cmr-prod',
  nodes: {
    oyPnlHBOSvW1TwX38FoQjA: {
      timestamp: 1573844277212,
      name: 'Adaptoid',
      transport_address: 'inet[ip-10-8-16-158.ec2.internal/10.8.16.158:9300]',
      host: 'localhost',
      ip: ['inet[ip-10-8-16-158.ec2.internal/10.8.16.158:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2076026,
        load_average: [0.27, 0.4, 0.44],
        cpu: {
          sys: 0,
          user: 0,
          idle: 99,
          usage: 0,
          stolen: 0
        },
        mem: {
          free_in_bytes: 486203392,
          used_in_bytes: 66230607872,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34379198464,
          actual_used_in_bytes: 32337612800
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2075814999,
        mem: {
          heap_used_in_bytes: 20043870296,
          heap_used_percent: 66,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 190564832,
          non_heap_committed_in_bytes: 193449984,
          pools: {
            young: {
              used_in_bytes: 163193200,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 69730304,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 19810946792,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29158961384,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 132,
          peak_count: 146
        },
        gc: {
          collectors: {
            young: {
              collection_count: 714612,
              collection_time_in_millis: 31595277
            },
            old: {
              collection_count: 889,
              collection_time_in_millis: 316961
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 21194,
            used_in_bytes: 374821906,
            total_capacity_in_bytes: 374821906
          },
          mapped: {
            count: 1168,
            used_in_bytes: 97378248760,
            total_capacity_in_bytes: 97378248760
          }
        }
      }
    },
    '1gyL4HwjTq6-o9Sbdvf__Q': {
      timestamp: 1573844277212,
      name: 'Crimson Cowl',
      transport_address: 'inet[/10.8.16.249:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.16.249:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2073526,
        load_average: [0.22, 0.31, 0.37],
        cpu: {
          sys: 0,
          user: 6,
          idle: 92,
          usage: 6,
          stolen: 0
        },
        mem: {
          free_in_bytes: 422051840,
          used_in_bytes: 66294759424,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34415394816,
          actual_used_in_bytes: 32301416448
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2073295285,
        mem: {
          heap_used_in_bytes: 19704883960,
          heap_used_percent: 65,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 186637448,
          non_heap_committed_in_bytes: 189530112,
          pools: {
            young: {
              used_in_bytes: 467939304,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 35471696,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 19201472960,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29257210640,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 123,
          peak_count: 156
        },
        gc: {
          collectors: {
            young: {
              collection_count: 704833,
              collection_time_in_millis: 32441893
            },
            old: {
              collection_count: 1093,
              collection_time_in_millis: 329485
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 22319,
            used_in_bytes: 392082912,
            total_capacity_in_bytes: 392082912
          },
          mapped: {
            count: 1137,
            used_in_bytes: 108811873540,
            total_capacity_in_bytes: 108811873540
          }
        }
      }
    },
    'GBOhugSmRJKRwOpGmR1G-A': {
      timestamp: 1573844277212,
      name: 'Prodigy',
      transport_address: 'inet[/10.8.18.115:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.18.115:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2074047,
        load_average: [0.28, 0.23, 0.35],
        cpu: {
          sys: 0,
          user: 5,
          idle: 94,
          usage: 5,
          stolen: 0
        },
        mem: {
          free_in_bytes: 490586112,
          used_in_bytes: 66226225152,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34292523008,
          actual_used_in_bytes: 32424288256
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2073834918,
        mem: {
          heap_used_in_bytes: 19553261624,
          heap_used_percent: 65,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 194241648,
          non_heap_committed_in_bytes: 197218304,
          pools: {
            young: {
              used_in_bytes: 108489032,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 48281856,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 19396490736,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29034453704,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 131,
          peak_count: 150
        },
        gc: {
          collectors: {
            young: {
              collection_count: 539386,
              collection_time_in_millis: 26052596
            },
            old: {
              collection_count: 979,
              collection_time_in_millis: 234388
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 24035,
            used_in_bytes: 420182447,
            total_capacity_in_bytes: 420182447
          },
          mapped: {
            count: 1121,
            used_in_bytes: 107372451750,
            total_capacity_in_bytes: 107372451750
          }
        }
      }
    },
    r5KowXHQQyOJG5r1u4jwtQ: {
      timestamp: 1573844277212,
      name: 'Katu',
      transport_address: 'inet[/10.8.19.252:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.19.252:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2077425,
        load_average: [0.31, 0.29, 0.28],
        cpu: {
          sys: 0,
          user: 0,
          idle: 99,
          usage: 0,
          stolen: 0
        },
        mem: {
          free_in_bytes: 468201472,
          used_in_bytes: 66248609792,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34356129792,
          actual_used_in_bytes: 32360681472
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2077195034,
        mem: {
          heap_used_in_bytes: 18582897208,
          heap_used_percent: 62,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 189987976,
          non_heap_committed_in_bytes: 192757760,
          pools: {
            young: {
              used_in_bytes: 135744568,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 37496960,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 18409655680,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29259703880,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 123,
          peak_count: 148
        },
        gc: {
          collectors: {
            young: {
              collection_count: 721657,
              collection_time_in_millis: 31886015
            },
            old: {
              collection_count: 787,
              collection_time_in_millis: 296077
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 23177,
            used_in_bytes: 406195908,
            total_capacity_in_bytes: 406195908
          },
          mapped: {
            count: 1137,
            used_in_bytes: 97414048907,
            total_capacity_in_bytes: 97414048907
          }
        }
      }
    },
    'xK-QyV4wRPKFiJ-R_W-a8w': {
      timestamp: 1573844277212,
      name: 'Norrin Radd',
      transport_address: 'inet[/10.8.17.245:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.17.245:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2078182,
        load_average: [0.43, 0.28, 0.27],
        cpu: {
          sys: 0,
          user: 6,
          idle: 93,
          usage: 6,
          stolen: 0
        },
        mem: {
          free_in_bytes: 449437696,
          used_in_bytes: 66267373568,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34249273344,
          actual_used_in_bytes: 32467537920
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2077975028,
        mem: {
          heap_used_in_bytes: 19032788744,
          heap_used_percent: 63,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 190417288,
          non_heap_committed_in_bytes: 194301952,
          pools: {
            young: {
              used_in_bytes: 437117208,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 35661360,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 18560010176,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29261543552,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 131,
          peak_count: 159
        },
        gc: {
          collectors: {
            young: {
              collection_count: 738167,
              collection_time_in_millis: 31996489
            },
            old: {
              collection_count: 1040,
              collection_time_in_millis: 357997
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 22489,
            used_in_bytes: 395143275,
            total_capacity_in_bytes: 395143275
          },
          mapped: {
            count: 1176,
            used_in_bytes: 113405090142,
            total_capacity_in_bytes: 113405090142
          }
        }
      }
    },
    jDnykrNDR1eBPv91e51rig: {
      timestamp: 1573844277212,
      name: 'Shotgun',
      transport_address: 'inet[/10.8.18.46:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.18.46:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2079497,
        load_average: [0.12, 0.18, 0.24],
        cpu: {
          sys: 0,
          user: 1,
          idle: 97,
          usage: 1,
          stolen: 0
        },
        mem: {
          free_in_bytes: 460980224,
          used_in_bytes: 66255831040,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34368933888,
          actual_used_in_bytes: 32347877376
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2079294948,
        mem: {
          heap_used_in_bytes: 19169861776,
          heap_used_percent: 63,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 190168376,
          non_heap_committed_in_bytes: 194695168,
          pools: {
            young: {
              used_in_bytes: 546798184,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 46095064,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 18576968528,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29115106640,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 123,
          peak_count: 153
        },
        gc: {
          collectors: {
            young: {
              collection_count: 516438,
              collection_time_in_millis: 24419404
            },
            old: {
              collection_count: 940,
              collection_time_in_millis: 296103
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 22058,
            used_in_bytes: 387883390,
            total_capacity_in_bytes: 387883390
          },
          mapped: {
            count: 1139,
            used_in_bytes: 111200887388,
            total_capacity_in_bytes: 111200887388
          }
        }
      }
    },
    LZveeY3eSkiYTjGJGepBRA: {
      timestamp: 1573844277212,
      name: 'Equinox',
      transport_address: 'inet[/10.8.19.114:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.19.114:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2074716,
        load_average: [0.35, 0.45, 0.45],
        cpu: {
          sys: 0,
          user: 5,
          idle: 94,
          usage: 5,
          stolen: 0
        },
        mem: {
          free_in_bytes: 523669504,
          used_in_bytes: 66193141760,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34374512640,
          actual_used_in_bytes: 32342298624
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2074494733,
        mem: {
          heap_used_in_bytes: 17774061608,
          heap_used_percent: 59,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 188744480,
          non_heap_committed_in_bytes: 192143360,
          pools: {
            young: {
              used_in_bytes: 382459632,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 53573936,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 17338028040,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29281079104,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 124,
          peak_count: 143
        },
        gc: {
          collectors: {
            young: {
              collection_count: 698421,
              collection_time_in_millis: 30294332
            },
            old: {
              collection_count: 967,
              collection_time_in_millis: 326586
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 21965,
            used_in_bytes: 386288474,
            total_capacity_in_bytes: 386288474
          },
          mapped: {
            count: 1108,
            used_in_bytes: 106023045636,
            total_capacity_in_bytes: 106023045636
          }
        }
      }
    },
    'JS8ZOf5cQ0-fOZOFHkYM9A': {
      timestamp: 1573844277211,
      name: 'Hood',
      transport_address: 'inet[/10.8.16.196:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.16.196:9300]', 'NONE'],
      os: {
        timestamp: 1573844277211,
        uptime_in_millis: 2076787,
        load_average: [0.84, 0.57, 0.4],
        cpu: {
          sys: 0,
          user: 4,
          idle: 95,
          usage: 4,
          stolen: 0
        },
        mem: {
          free_in_bytes: 505110528,
          used_in_bytes: 66211700736,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34307969024,
          actual_used_in_bytes: 32408842240
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277211,
        uptime_in_millis: 2076595338,
        mem: {
          heap_used_in_bytes: 20996051680,
          heap_used_percent: 70,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 193710224,
          non_heap_committed_in_bytes: 196464640,
          pools: {
            young: {
              used_in_bytes: 319849728,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 69730304,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 20606471648,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 28766817232,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 131,
          peak_count: 144
        },
        gc: {
          collectors: {
            young: {
              collection_count: 558045,
              collection_time_in_millis: 26477827
            },
            old: {
              collection_count: 1018,
              collection_time_in_millis: 247366
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 23344,
            used_in_bytes: 409777392,
            total_capacity_in_bytes: 409777392
          },
          mapped: {
            count: 1127,
            used_in_bytes: 111236953969,
            total_capacity_in_bytes: 111236953969
          }
        }
      }
    },
    '5YKOSJ_kSmOMmFvw3pcDHQ': {
      timestamp: 1573844277212,
      name: 'Brain Drain',
      transport_address: 'inet[/10.8.16.228:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.16.228:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2080163,
        load_average: [0.53, 0.37, 0.37],
        cpu: {
          sys: 0,
          user: 6,
          idle: 93,
          usage: 6,
          stolen: 0
        },
        mem: {
          free_in_bytes: 538644480,
          used_in_bytes: 66178166784,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34261114880,
          actual_used_in_bytes: 32455696384
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2079954851,
        mem: {
          heap_used_in_bytes: 21548156424,
          heap_used_percent: 71,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 195927312,
          non_heap_committed_in_bytes: 198955008,
          pools: {
            young: {
              used_in_bytes: 370527792,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 45633832,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 21131994800,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29008574040,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 124,
          peak_count: 152
        },
        gc: {
          collectors: {
            young: {
              collection_count: 633879,
              collection_time_in_millis: 30651208
            },
            old: {
              collection_count: 925,
              collection_time_in_millis: 279491
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 23142,
            used_in_bytes: 406590142,
            total_capacity_in_bytes: 406590142
          },
          mapped: {
            count: 1146,
            used_in_bytes: 101453430970,
            total_capacity_in_bytes: 101453430970
          }
        }
      }
    },
    'EMZ-dQYESlaKTPLU8_DATQ': {
      timestamp: 1573844277212,
      name: 'Super-Skrull',
      transport_address: 'inet[/10.8.19.168:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.19.168:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2081498,
        load_average: [0.6, 0.48, 0.39],
        cpu: {
          sys: 0,
          user: 5,
          idle: 93,
          usage: 5,
          stolen: 0
        },
        mem: {
          free_in_bytes: 507973632,
          used_in_bytes: 66208837632,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34394734592,
          actual_used_in_bytes: 32322076672
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2081274469,
        mem: {
          heap_used_in_bytes: 21650573616,
          heap_used_percent: 72,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 189975256,
          non_heap_committed_in_bytes: 192794624,
          pools: {
            young: {
              used_in_bytes: 457115112,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 21086472,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 21172372032,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29316839496,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 123,
          peak_count: 156
        },
        gc: {
          collectors: {
            young: {
              collection_count: 536561,
              collection_time_in_millis: 25054553
            },
            old: {
              collection_count: 806,
              collection_time_in_millis: 250091
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 20110,
            used_in_bytes: 355798546,
            total_capacity_in_bytes: 355798546
          },
          mapped: {
            count: 1157,
            used_in_bytes: 106142216118,
            total_capacity_in_bytes: 106142216118
          }
        }
      }
    },
    '2vwumuTETL2DbiaanEwjCA': {
      timestamp: 1573844277212,
      name: 'Porcupine',
      transport_address: 'inet[/10.8.16.152:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.16.152:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2069748,
        load_average: [0.56, 0.4, 0.42],
        cpu: {
          sys: 0,
          user: 0,
          idle: 99,
          usage: 0,
          stolen: 0
        },
        mem: {
          free_in_bytes: 530120704,
          used_in_bytes: 66186690560,
          free_percent: 50,
          used_percent: 49,
          actual_free_in_bytes: 33998139392,
          actual_used_in_bytes: 32718671872
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2069677556,
        mem: {
          heap_used_in_bytes: 20829946976,
          heap_used_percent: 69,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 193016992,
          non_heap_committed_in_bytes: 196382720,
          pools: {
            young: {
              used_in_bytes: 475517632,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 30478280,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 20323951064,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29235912320,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 131,
          peak_count: 141
        },
        gc: {
          collectors: {
            young: {
              collection_count: 865530,
              collection_time_in_millis: 39948274
            },
            old: {
              collection_count: 1330,
              collection_time_in_millis: 452605
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 24306,
            used_in_bytes: 424730395,
            total_capacity_in_bytes: 424730395
          },
          mapped: {
            count: 1151,
            used_in_bytes: 107104501361,
            total_capacity_in_bytes: 107104501361
          }
        }
      }
    },
    pGaKzChEQHC8_RwJtqRzZQ: {
      timestamp: 1573844277211,
      name: 'Living Lightning',
      transport_address: 'inet[/10.8.16.9:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.16.9:9300]', 'NONE'],
      os: {
        timestamp: 1573844277211,
        uptime_in_millis: 2078856,
        load_average: [0.56, 0.36, 0.33],
        cpu: {
          sys: 0,
          user: 4,
          idle: 95,
          usage: 4,
          stolen: 0
        },
        mem: {
          free_in_bytes: 430956544,
          used_in_bytes: 66285854720,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34300944384,
          actual_used_in_bytes: 32415866880
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277211,
        uptime_in_millis: 2078634148,
        mem: {
          heap_used_in_bytes: 19029466640,
          heap_used_percent: 63,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 190585440,
          non_heap_committed_in_bytes: 194351104,
          pools: {
            young: {
              used_in_bytes: 300409144,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 39624416,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 18689433080,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 27623196872,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 131,
          peak_count: 160
        },
        gc: {
          collectors: {
            young: {
              collection_count: 509094,
              collection_time_in_millis: 24724516
            },
            old: {
              collection_count: 989,
              collection_time_in_millis: 213977
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 22516,
            used_in_bytes: 395112957,
            total_capacity_in_bytes: 395112957
          },
          mapped: {
            count: 1074,
            used_in_bytes: 109678158224,
            total_capacity_in_bytes: 109678158224
          }
        }
      }
    },
    '3SrGNA2BTR66Fm4T6Gu8MQ': {
      timestamp: 1573844277212,
      name: 'Sugar Man',
      transport_address: 'inet[/10.8.18.189:9300]',
      host: 'localhost',
      ip: ['inet[/10.8.18.189:9300]', 'NONE'],
      os: {
        timestamp: 1573844277212,
        uptime_in_millis: 2075384,
        load_average: [0.33, 0.3, 0.33],
        cpu: {
          sys: 0,
          user: 8,
          idle: 91,
          usage: 8,
          stolen: 0
        },
        mem: {
          free_in_bytes: 434421760,
          used_in_bytes: 66282389504,
          free_percent: 51,
          used_percent: 48,
          actual_free_in_bytes: 34323165184,
          actual_used_in_bytes: 32393646080
        },
        swap: {
          used_in_bytes: 0,
          free_in_bytes: 0
        }
      },
      jvm: {
        timestamp: 1573844277212,
        uptime_in_millis: 2075154810,
        mem: {
          heap_used_in_bytes: 18766842296,
          heap_used_percent: 62,
          heap_committed_in_bytes: 29953097728,
          heap_max_in_bytes: 29953097728,
          non_heap_used_in_bytes: 186180336,
          non_heap_committed_in_bytes: 190005248,
          pools: {
            young: {
              used_in_bytes: 505780808,
              max_in_bytes: 558432256,
              peak_used_in_bytes: 558432256,
              peak_max_in_bytes: 558432256
            },
            survivor: {
              used_in_bytes: 49167200,
              max_in_bytes: 69730304,
              peak_used_in_bytes: 69730304,
              peak_max_in_bytes: 69730304
            },
            old: {
              used_in_bytes: 18211894288,
              max_in_bytes: 29324935168,
              peak_used_in_bytes: 29306554256,
              peak_max_in_bytes: 29324935168
            }
          }
        },
        threads: {
          count: 131,
          peak_count: 150
        },
        gc: {
          collectors: {
            young: {
              collection_count: 688691,
              collection_time_in_millis: 31081454
            },
            old: {
              collection_count: 1059,
              collection_time_in_millis: 376309
            }
          }
        },
        buffer_pools: {
          direct: {
            count: 22944,
            used_in_bytes: 402556039,
            total_capacity_in_bytes: 402556039
          },
          mapped: {
            count: 1174,
            used_in_bytes: 109263884473,
            total_capacity_in_bytes: 109263884473
          }
        }
      }
    }
  }
};

const prevCpu = {
  'Adaptoid': [8, 9, 10, 7, 1],
  'Crimson Cowl': [5, 4, 3, 3, 4],
  'Prodigy': [1, 2, 3, 4, 5],
  'Katu': [1, 2, 3, 4, 0],
  'Norrin Radd': [1, 2, 3, 4, 1],
  'Shotgun': [1, 2, 3, 4, 4],
  'Equinox': [1, 2, 3, 4, 3],
  'Hood': [1, 2, 3, 4, 0],
  'Brain Drain': [1, 2, 3, 4, 5],
  'Super-Skrull': [5, 7, 2, 1, 0],
  'Porcupine': [4, 5, 8, 3, 7],
  'Living Lightning': [3, 2, 2, 2, 7],
  'Sugar Man': [1, 5, 7, 9, 5]
};

const prevJvmHeap = {
  'Adaptoid': [80, 75, 90, 77, 88],
  'Crimson Cowl': [45, 60, 77, 88, 92],
  'Prodigy': [71, 32, 33, 34, 55],
  'Katu': [81, 72, 63, 74, 80],
  'Norrin Radd': [71, 62, 93, 94, 91],
  'Shotgun': [81, 82, 83, 84, 94],
  'Equinox': [11, 21, 34, 46, 32],
  'Hood': [11, 82, 93, 84, 90],
  'Brain Drain': [11, 22, 34, 44, 55],
  'Super-Skrull': [95, 97, 92, 91, 90],
  'Porcupine': [34, 45, 38, 73, 97],
  'Living Lightning': [93, 92, 92, 92, 27],
  'Sugar Man': [11, 51, 71, 91, 51]
};

const prevJvmPressure = {
  'Adaptoid': [80.53, 75.63, 90.72, 77.56, 88.1],
  'Crimson Cowl': [45.64, 60.74, 77.73, 88.55, 92.88],
  'Prodigy': [71.35, 32.66, 33.83, 34.23, 55.83],
  'Katu': [81.1, 72.6, 63.75, 74.53, 80.23],
  'Norrin Radd': [71.74, 62.25, 93.85, 94.24, 91.11],
  'Shotgun': [81.74, 82.74, 83.62, 84.25, 94.62],
  'Equinox': [11.77, 21.88, 34.66, 46.42, 32.23],
  'Hood': [11.66, 82.23, 93.68, 84.27, 90.78],
  'Brain Drain': [11.73, 22.79, 34.67, 44.36, 55.1],
  'Super-Skrull': [95.2, 97.3, 92.45, 91.67, 90.89],
  'Porcupine': [34.12, 45.68, 38.25, 73.78, 97.85],
  'Living Lightning': [93.79, 92.89, 92.11, 92.57, 27.72],
  'Sugar Man': [11.83, 51.75, 71.21, 91.12, 51.55]
};

const currentCpu = {
  'Adaptoid': 0,
  'Crimson Cowl': 6,
  'Prodigy': 5,
  'Katu': 0,
  'Norrin Radd': 6,
  'Shotgun': 1,
  'Equinox': 5,
  'Hood': 4,
  'Brain Drain': 6,
  'Super-Skrull': 5,
  'Porcupine': 0,
  'Living Lightning': 4,
  'Sugar Man': 8
};

const currentJvmHeap = {
  'Adaptoid': 66,
  'Crimson Cowl': 65,
  'Prodigy': 65,
  'Katu': 62,
  'Norrin Radd': 63,
  'Shotgun': 63,
  'Equinox': 59,
  'Hood': 70,
  'Brain Drain': 71,
  'Super-Skrull': 72,
  'Porcupine': 69,
  'Living Lightning': 63,
  'Sugar Man': 62
};

const currentJvmPressure = {
  'Adaptoid': 67.56,
  'Brain Drain': 72.06,
  'Crimson Cowl': 65.48,
  'Equinox': 59.12,
  'Hood': 70.27,
  'Katu': 62.78,
  'Living Lightning': 63.73,
  'Norrin Radd': 63.29,
  'Porcupine': 69.31,
  'Prodigy': 66.14,
  'Shotgun': 63.35,
  'Sugar Man': 62.1,
  'Super-Skrull': 72.2,
};

describe('slideWindows', () => {
  test('full history', () => {
    const oldValues = {
      'A': [1, 2, 3, 4],
      'B': [5, 6, 7, 8]
    };
    const newValues = {
      'A': 9,
      'B': 10
    };
    expect(slideWindows(oldValues, newValues, 4)).toStrictEqual({
      'A': [2, 3, 4, 9],
      'B': [6, 7, 8, 10]
    });
  });

  test('empty history', () => {
    const oldValues = {};
    const newValues = {
      'A': 9,
      'B': 10
    };
    expect(slideWindows(oldValues, newValues, 4)).toStrictEqual({
      'A': [9],
      'B': [10]
    });
  });

  test('Node becomes inactive', () => {
    const oldValues = {
      'Old': [9]
    };
    const newValues = {
      'New': 10
    }
    expect(slideWindows(oldValues, newValues, 4)).toStrictEqual({
        'New': [10]
    });
  })
});

describe('Thresholding values', () => {
  test('Threshold reached returns true', () => {
    expect(thresholdReached([8, 8, 8], 8, 3)).toBeTruthy();
  });

  test('Threshold not reached returns false', () => {
    expect(thresholdReached([8, 7, 8], 8, 3)).toBeFalsy();
  });

  test('Threshold reached, window smaller than history returns true', () => {
    expect(thresholdReached([1, 8, 8, 8], 8, 3)).toBeTruthy();
  });

  test('Threshold not reached, window smaller than history returns false', () => {
    expect(thresholdReached([9, 8, 7, 8], 8, 3)).toBeFalsy();
  });

  test('Window larger than history returns false', () => {
    expect(thresholdReached([9, 8, 8, 8], 8, 5)).toBeFalsy();
  });
});

test('cpuValues', () => {
  expect(cpuValues(JSON.stringify(esStats))).toStrictEqual(currentCpu);
});

test('jvmHeapValues', () => {
  expect(jvmHeapValues(JSON.stringify(esStats))).toStrictEqual(currentJvmHeap);
});

test('jvmPressureValues', () => {
  expect(jvmPressureValues(JSON.stringify(esStats))).toStrictEqual(currentJvmPressure);
});

describe('CPU Status', () => {
  test('full history', () => {
    expect(cpuStats(JSON.stringify(esStats), prevCpu, 3, 5)).toStrictEqual([
      ['Crimson Cowl', 'Sugar Man'],
      {
        'Adaptoid': [9, 10, 7, 1, 0],
        'Crimson Cowl': [4, 3, 3, 4, 6],
        'Prodigy': [2, 3, 4, 5, 5],
        'Katu': [2, 3, 4, 0, 0],
        'Norrin Radd': [2, 3, 4, 1, 6],
        'Shotgun': [2, 3, 4, 4, 1],
        'Equinox': [2, 3, 4, 3, 5],
        'Hood': [2, 3, 4, 0, 4],
        'Brain Drain': [2, 3, 4, 5, 6],
        'Super-Skrull': [7, 2, 1, 0, 5],
        'Porcupine': [5, 8, 3, 7, 0],
        'Living Lightning': [2, 2, 2, 7, 4],
        'Sugar Man': [5, 7, 9, 5, 8]
      }
    ]);
  });

  test('empty history', () => {
    expect(cpuStats(JSON.stringify(esStats), {}, 3, 5)).toStrictEqual([
      [],
      {
        'Adaptoid': [0],
        'Crimson Cowl': [6],
        'Prodigy': [5],
        'Katu': [0],
        'Norrin Radd': [6],
        'Shotgun': [1],
        'Equinox': [5],
        'Hood': [4],
        'Brain Drain': [6],
        'Super-Skrull': [5],
        'Porcupine': [0],
        'Living Lightning': [4],
        'Sugar Man': [8]
      }
    ]);
  });

  test('empty new values', () => {
    expect(cpuStats(JSON.stringify({}), prevCpu, 3, 5)).toStrictEqual([
      [],
      {}
    ]);
  });
});

describe('JVM Heap status', () => {
  test('full history', () => {
    expect(jvmHeapStats(JSON.stringify(esStats), prevJvmHeap, 70, 5)).toStrictEqual([
      ['Hood', 'Super-Skrull'],
      {
        'Adaptoid': [75, 90, 77, 88, 66],
        'Crimson Cowl': [60, 77, 88, 92, 65],
        'Prodigy': [32, 33, 34, 55, 65],
        'Katu': [72, 63, 74, 80, 62],
        'Norrin Radd': [62, 93, 94, 91, 63],
        'Shotgun': [82, 83, 84, 94, 63],
        'Equinox': [21, 34, 46, 32, 59],
        'Hood': [82, 93, 84, 90, 70],
        'Brain Drain': [22, 34, 44, 55, 71],
        'Super-Skrull': [97, 92, 91, 90, 72],
        'Porcupine': [45, 38, 73, 97, 69],
        'Living Lightning': [92, 92, 92, 27, 63],
        'Sugar Man': [51, 71, 91, 51, 62]
      }
    ]);
  });

  test('empty history', () => {
    expect(jvmHeapStats(JSON.stringify(esStats), {}, 70, 5)).toStrictEqual([
      [],
      {
        'Adaptoid': [66],
        'Crimson Cowl': [65],
        'Prodigy': [65],
        'Katu': [62],
        'Norrin Radd': [63],
        'Shotgun': [63],
        'Equinox': [59],
        'Hood': [70],
        'Brain Drain': [71],
        'Super-Skrull': [72],
        'Porcupine': [69],
        'Living Lightning': [63],
        'Sugar Man': [62]
      }
    ]);
  });

  test('empty new values', () => {
    expect(jvmHeapStats(JSON.stringify({}), prevJvmHeap, 70, 5)).toStrictEqual([
      [],
      {}
    ]);
  });
});

describe('JVM Pressure status', () => {
  test('full history', () => {
    expect(jvmPressureStats(JSON.stringify(esStats), prevJvmPressure, 70, 5)).toStrictEqual([
      ['Hood', 'Super-Skrull'],
      {
        'Adaptoid': [75.63, 90.72, 77.56, 88.1, 67.56],
        'Crimson Cowl': [60.74, 77.73, 88.55, 92.88, 65.48],
        'Prodigy': [32.66, 33.83, 34.23, 55.83, 66.14],
        'Katu': [72.6, 63.75, 74.53, 80.23, 62.78],
        'Norrin Radd': [62.25, 93.85, 94.24, 91.11, 63.29],
        'Shotgun': [82.74, 83.62, 84.25, 94.62, 63.35],
        'Equinox': [21.88, 34.66, 46.42, 32.23, 59.12],
        'Hood': [82.23, 93.68, 84.27, 90.78, 70.27],
        'Brain Drain': [22.79, 34.67, 44.36, 55.1, 72.06],
        'Super-Skrull': [97.3, 92.45, 91.67, 90.89, 72.2],
        'Porcupine': [45.68, 38.25, 73.78, 97.85, 69.31],
        'Living Lightning': [92.89, 92.11, 92.57, 27.72, 63.73],
        'Sugar Man': [51.75, 71.21, 91.12, 51.55, 62.1]
      }
    ]);
  });

  test('empty history', () => {
    expect(jvmPressureStats(JSON.stringify(esStats), {}, 70, 5)).toStrictEqual([
      [],
      {
        'Adaptoid': [67.56],
        'Crimson Cowl': [65.48],
        'Prodigy': [66.14],
        'Katu': [62.78],
        'Norrin Radd': [63.29],
        'Shotgun': [63.35],
        'Equinox': [59.12],
        'Hood': [70.27],
        'Brain Drain': [72.06],
        'Super-Skrull': [72.2],
        'Porcupine': [69.31],
        'Living Lightning': [63.73],
        'Sugar Man': [62.1]
      }
    ]);
  });

  test('empty new values', () => {
    expect(jvmPressureStats(JSON.stringify({}), prevJvmPressure, 70, 5)).toStrictEqual([
      [],
      {}
    ]);
  });
});
