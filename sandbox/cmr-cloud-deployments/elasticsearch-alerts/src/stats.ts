/* eslint-disable comma-dangle */
/* eslint no-unused-vars: ["error", { "argsIgnorePattern": "^_" }] */

// eslint-disable-next-line @typescript-eslint/no-var-requires
const jp = require('jsonpath');

/**
 * Given a map of strings to arrays of number and a map of strings to numbers,
 * Remove the first element of each array and append the corresponding value from
 * the second map. If the length of the `history` array is less than `window` then
 * the first element is not removed.
 *
 * @param history - The map of arrays that will be updated
 * @param newValues - The map of new values
 * @param window - The length of the window
 * @returns The new map of arrays with updated values
 *
 * **Example:**
 * ```
 * slideWindows(
 *  {
 *   'A': [1, 2, 3, 4],
 *   'B': [5, 6, 7, 8]
 *  },
 *  {
 *   'A': 9,
 *   'B': 10
 *  },
 *  4)
 * =>
 *  {
 *    'A': [2, 3, 4, 9],
 *    'B': [6, 7, 8, 10]
 *  }
 * ```
 */
export const slideWindows = (
  history: Record<string, Array<number>>,
  // eslint-disable-next-line comma-dangle
  newValues: Record<string, number>,
  window: number
): Record<string, Array<number>> => {
  const rval = {};

  Object.keys(newValues).forEach((key, _index) => {
    let updatedValues = history[key] || [] || [];
    if (updatedValues.length >= window) {
      updatedValues = updatedValues.slice(1);
    }
    updatedValues.push(newValues[key]);
    rval[key] = updatedValues;
  });

  return rval;
};

/**
 * Check to see if all values in the tail (of length `window`) of the given array equal or exceed
 * the given threshold
 * @param history - The array of numbers to be thresholded
 * @param threshold - The value that should not be exceeded
 * @param window - The length of the tail to be checked
 * @returns A boolean indicating whether or not the threshold was exceeded
 */
export const thresholdReached = (
  history: Array<number>,
  threshold: number,
  // eslint-disable-next-line comma-dangle
  window: number
): boolean => {
  if (history.length < window) return false;

  const startIndex = history.length - window;
  for (let index = startIndex; index < history.length; index += 1) {
    if (history[index] < threshold) return false;
  }
  return true;
};

/**
 * Parse the cpu usage values out of the ES status response
 * @param json - The JSON string returned by the call to the ES status
 * @returns A map of node names to values
 */
export const cpuValues = (json: string): Record<string, number> => {
  const rval = {};
  const stats = JSON.parse(json);
  const cpuStatsArry: Array<Array<string | number>> = jp
    .query(stats, '$..nodes[*]')
    .map((value, _index) => [value.name, value.os.cpu.usage]);
  cpuStatsArry.forEach(([key, value], _index) => {
    rval[key] = value;
  });
  return rval;
};

/**
 * Check to see if any of the nodes have had a cpu usage above the threshold for a given
 * number of time periods
 * @param statsResp - The response from the ES stats endpoint
 * @param previous - A map of node names to cpu usage histories
 * @param threshold - The cpu usage threshold that triggers alarms
 * @param window - The number of time periods to check
 * @returns A tuple containing two entries. The first is a list of nodes that have exceeded
 * the threshold for too long. The second is the new history map with updated values.
 */
export function cpuStats(
  statsResp: string,
  previous: Record<string, Array<number>>,
  threshold: number,
  window: number
): [Array<string>, Record<string, Array<number>>] {
  const exceededNodes: string[] = [];
  const newHistory = slideWindows(previous, cpuValues(statsResp), window);
  Object.keys(newHistory).forEach((key, _index) => {
    const history = newHistory[key];
    if (thresholdReached(history, threshold, window)) {
      exceededNodes.push(key);
    }
  });

  return [exceededNodes, newHistory];
}

/**
 * Parse the memory usage values out of the ES status response
 * @param json - The JSON string returned by the call to the ES status
 * @returns A map of node names to values
 */
export const jvmHeapValues = (json: string): Record<string, number> => {
  const rval = {};
  const stats = JSON.parse(json);
  const memStats: Array<Array<string | number>> = jp
    .query(stats, '$..nodes[*]')
    .map((value, _index) => [value.name, value.jvm.mem.heap_used_percent]);
  memStats.forEach(([key, value], _index) => {
    rval[key] = value;
  });
  return rval;
};

/**
 * Check to see if any of the nodes have had a jvm heap usage above the threshold for a given
 * number of time periods
 * @param statsResp - The response from the ES stats endpoint
 * @param previous - A map of node names to heap usage histories
 * @param threshold - The heap usage threshold that triggers alarms
 * @param window - The number of time periods to check
 * @returns A tuple containing two entries. The first is a list of nodes that have exceeded
 * the threshold for too long. The second is the new history map with updated values.
 */
export function jvmHeapStats(
  statsResp: string,
  previous: Record<string, Array<number>>,
  threshold: number,
  window: number
): [Array<string>, Record<string, Array<number>>] {
  const exceededNodes: string[] = [];
  const newHistory = slideWindows(previous, jvmHeapValues(statsResp), window);
  Object.keys(newHistory).forEach((key, _index) => {
    const history = newHistory[key];
    if (thresholdReached(history, threshold, window)) {
      exceededNodes.push(key);
    }
  });

  return [exceededNodes, newHistory];
}

/**
 * Calculates the jvm memory pressure of each node out of the parsed ES status response
 * @param stats - The parsed JSON object
 * @returns JVM Memory Pressure percentage
 */
const getPressurePercent = (stats): number => {
  const pct = (stats.jvm.mem.pools.old.used_in_bytes / stats.jvm.mem.pools.old.max_in_bytes);
  const pressure = 100 * pct;

  // round to 2 decimal points for convenience
  return Math.round(pressure * 100) / 100;
};

/**
 * Parses the jvm memory pressure of each node out of the ES status response
 * @param json - The JSON string returned by the call to the ES status
 * @returns A map of node names to memory pressure values
 */
export const jvmPressureValues = (json: string): Record<string, number> => {
  const rval = {};
  const stats = JSON.parse(json);
  const memStats: Array<Array<string | number>> = jp
    .query(stats, '$..nodes[*]')
    .map((value, _index) => [value.name, getPressurePercent(value)]);
  memStats.forEach(([key, value], _index) => {
    rval[key] = value;
  });
  return rval;
};

/**
 * Check to see if any of the nodes have had a jvm memory pressure above the threshold
 * for a given number of time periods
 * @param statsResp - The response from the ES stats endpoint
 * @param previous - A map of node names to node memory pressure histories
 * @param threshold - The jvm memory pressure threshold that triggers alarms
 * @param window - The number of time periods to check
 * @returns A tuple containing two entries. The first is a list of nodes that have exceeded
 * the threshold for too long. The second is the new history map with updated values.
 */
export function jvmPressureStats(
  statsResp: string,
  previous: Record<string, Array<number>>,
  threshold: number,
  window: number
): [Array<string>, Record<string, Array<number>>] {
  const exceededNodes: string[] = [];
  const newHistory = slideWindows(previous, jvmPressureValues(statsResp), window);
  Object.keys(newHistory).forEach((key, _index) => {
    const history = newHistory[key];
    if (thresholdReached(history, threshold, window)) {
      exceededNodes.push(key);
    }
  });

  return [exceededNodes, newHistory];
}
