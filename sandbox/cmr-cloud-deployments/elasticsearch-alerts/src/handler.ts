import { env } from 'process';
import * as log from 'loglevel';
import { elasticHealthStatus, elasticStats, elasticStateReport } from './elasticsearch';
import {
  getPreviousStatus,
  setPreviousStatus,
  cpuHistory,
  jvmHeapHistory,
  jvmPressureHistory,
  saveCpuHistory,
  saveJvmHeapHistory,
  saveJvmPressureHistory
} from './cache';
import { sendAlert } from './sns';
import { cpuStats, jvmHeapStats, jvmPressureStats } from './stats';

const expectedNodeCount = Number.parseInt(env.EXPECTED_ES_NODE_COUNT || '2', 10);
const cpuThreshold = Number.parseInt(env.ES_CPU_THRESHOLD || '90', 10);
const jvmHeapThreshold = Number.parseInt(env.ES_JVM_HEAP_THRESHOLD || '80', 10);
const jvmPressureThreshold = Number.parseInt(env.ES_JVM_PRESSURE_THRESHOLD || '80', 10);
const windowSize = Number.parseInt(env.ES_HISTORY_WINDOW_SIZE || '5', 10);
const environment = (env.ENVIRONMENT || 'local').toUpperCase();

log.setLevel('info');

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const monitor = async (event: any): Promise<any> => {
  const { status, count } = await elasticHealthStatus();

  const previousStatus = await getPreviousStatus();

  if (status !== previousStatus) {
    log.warn(`${environment}: Elasticsearch status changed from ${previousStatus} to ${status}`);
    sendAlert(`${environment}: Elasticsearch status changed from ${previousStatus} to ${status}`);

    await setPreviousStatus(status);
  }

  if (count !== expectedNodeCount) {
    log.warn(`${environment}: Elasticsearch node count is ${count}, expected ${expectedNodeCount}`);
    sendAlert(
      `${environment}: Elasticsearch node count is ${count}, expected ${expectedNodeCount}`
    );
  }

  const esStats = await elasticStats();
  const prevCpu = await cpuHistory();
  const prevJvm = await jvmHeapHistory();
  const prevPressure = await jvmPressureHistory();
  const [overCpu, newCpuHistory] = await cpuStats(esStats, prevCpu, cpuThreshold, windowSize);
  const [overJvmHeap, newJvmHeapHistory] = await jvmHeapStats(
    esStats,
    prevJvm,
    jvmHeapThreshold,
    windowSize
  );
  const [overJvmPressure, newJvmPressureHistory] = await jvmPressureStats(
    esStats,
    prevPressure,
    jvmPressureThreshold,
    windowSize
  );

  // check the cpu results
  if (overCpu.length > 0) {
    log.warn(
      `${environment}: Elasticsearch nodes [${overCpu}] have exceeded the ${cpuThreshold}% CPU threshold for ${windowSize} minutes.`
    );
    sendAlert(
      `${environment}: Elasticsearch nodes [${overCpu}] have exceeded the ${cpuThreshold}% CPU threshold for ${windowSize} minutes.`
    );
  }

  // check the JVM heap results
  if (overJvmHeap.length > 0) {
    log.warn(
      `${environment}: Elasticsearch nodes [${overJvmHeap}] have exceeded the ${jvmHeapThreshold}% JVM HEAP threshold for ${windowSize} minutes.`
    );
    sendAlert(
      `${environment}: Elasticsearch nodes [${overJvmHeap}] have exceeded the ${jvmHeapThreshold}% JVM HEAP threshold for ${windowSize} minutes.`
    );
  }

  if (overJvmPressure.length > 0) {
    log.warn(
      `${environment}: Elasticsearch nodes [${overJvmPressure}] have exceeded the ${jvmPressureThreshold}% JVM HEAP PRESSURE threshold for ${windowSize} minutes.`
    );
    sendAlert(
      `${environment}: Elasticsearch nodes [${overJvmPressure}] have exceeded the ${jvmPressureThreshold}% JVM HEAP PRESSURE threshold for ${windowSize} minutes.`
    );
  }

  if (overCpu.length > 0 || overJvmHeap.length > 0 || overJvmPressure.length > 0) {
    // if any of the alerts trigger, dump the elasticsearch report to the logs
    const { nodeStats, pendingTasks, activeTasks, hotThreads, shardExplanations } = await elasticStateReport();
    log.warn(`${environment}: Elasticsearch State Report`,
      "\nNode Stats:", nodeStats,
      "\nPending Tasks:", pendingTasks,
      "\nActive Tasks:", activeTasks,
      "\nHot Threads:", hotThreads,
      "\nShard Explanations:", shardExplanations
    );
  }

  // save the history
  saveCpuHistory(newCpuHistory);
  saveJvmHeapHistory(newJvmHeapHistory);
  saveJvmPressureHistory(newJvmPressureHistory);

  log.info(`status: ${status}`);
  log.info(`number of nodes: ${count}`);

  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Elasticsearch status checked',
        input: event,
      },
      null,
      2
    ),
  };
};

exports.monitor = monitor;
