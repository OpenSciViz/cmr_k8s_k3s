#!/bin/bash

# Someone with NGAP Admin permissions in the AWS account needs to run this before deploying anything else

aws iam create-service-linked-role --aws-service-name ecs.amazonaws.com
aws iam create-service-linked-role --aws-service-name elasticache.amazonaws.com