# CMR Cloud Deployments

This repo is used to setup and deploy the CMR applications in NGAP 2.0 accounts.

## terraform

We will use terraform to manage setting up the infrastructure for the 2.0
accounts. All terraform code is contained in the terraform subdirectory.

## Creating and Uploading Docker Image for Services Automatically from TF Output

The script `auto_ecr.sh` can be used to generate and upload the Docker image for the
microservices after a deployment as follows (from the `terraform` directory):

`tf output | ../auto-ecr.sh`

The script makes assumptions about the location of the CMR repository directory so it may need
to be edited for your individual setup.

## Utilizing bamboo scripts

The following environment variables are expected:

- bamboo_AWS_ACCESS_KEY_ID_PASSWORD
- bamboo_AWS_SECRET_ACCESS_KEY_PASSWORD
- bamboo_AWS_DEFAULT_REGION
- bamboo_ENVIRONMENT_NAME
- RELEASE_VERSION

From within the root repository directory:

1. `source scripts/bamboo_init.sh`
2. `source scripts/bamboo_ecr.sh <TERRAFORM_ECR_RESOURCE_NAME, example: cmr_legacy_services> <DOCKERFILE_PATH>`
3. `./scripts/bamboo_deploy.sh <SERVICE-TO-DEPLOY-1, example: legacy-services> <SERVICE-TO-DEPLOY-2> ... <SERVICE-TO-DEPLOY-N>`

Sourcing bamboo_init.sh will export the following environment variables:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION
- ENVIRONMENT_NAME
- RELEASE_VERSION

### Webhooks for SNS subscriptions

| Environment Variable           | Bamboo Variable               | Format                                                       |
|--------------------------------|-------------------------------|--------------------------------------------------------------|
| TF_VAR_cmr_sns_alerts_webhooks | CMR_SNS_ALERTS_WEBHOOKS_<env> | ["http://webhook1","http://webhook2"] or left empty for none |

Sourcing bamboo_ecr.sh will export:
- IMAGE: <image_url>:<RELEASE_VERSION>

Deploying multiple services with different IMAGES require you to source bamboo_ecr.sh between bamboo_deploy.sh runs.

## Service-Linked Roles

Service-linked roles are special IAM roles that must be created once for each service that will be used in an account. Someone with sufficient IAM permissions (e.g. NGAP staff) must run `create-service-linked-roles.sh` for each account before the Terraform configuration can be deployed.

## Run terraform deployment locally

Assuming a `terraform destroy` has been run and all configurations (`cmr.auto.tfvars` in cmr-cloud-deployments and configs for services in AWS parameter store) has been set up. The local testing environment name is set up as `environment_name` in `cmr.auto.tfvars`.

It is extremely important that you start up your own Elasticsearch cluster (in cmr-cloud-elasticsearch repo) and have your local configs referencing your own Elasticsearch cluster rather than the SIT Elasticsearch cluster if you want to do any data changing tasks such as ingest or reindex, etc.

### Set up ECR repos for ECR images needed by CMR deployment
  There are 4 ECR repos: cmr_core, cmr_opensearch, cmr_legacy_services, and cmr_service_bridge to hold the ECR images for deploying CMR services to ECS.
  The cmr_core ECR image is used for all cmr core services, such as ingest, indexer, search, etc. The other images are for the corresponding services indicated by the name.
  Each ECR image is built from a base docker file and pushed to ECR.
  e.g. to build and push the cmr_core image to ECR locally (assuming cloud-deployments local config.yml is ready to go),

  - in cmr/dev-system, build the uberjar
    `lein uberjar`
    The uberjar is needed to build the cmr-core docker image.
  - in cmr-cloud-deployments/terraform, set up ECR repo for cmr_core:
    `terraform apply -target=aws_ecr_repository.cmr_core`
  - Push cmr_core docker image to ECR:
    `../scripts/ecr.sh <docker-file-path> <release-version> <ecr-repo-name> <ecr-repo-url>`
    e.g. `../scripts/ecr.sh ../../cmr/dev-system/Dockerfile.ecs latest cmr-core-yliu 832706493240.dkr.ecr.us-east-1.amazonaws.com/cmr-core-yliu`

## Terraform naming conventions

- Module names and service-names should be equivalent except module names are written in snake_case while the service-names are written in kebab-case.

## Deploy Script

deploy.sh is used to replace a container definition with a new image and any updated environment variables.
The script assumes the cluster and service has initially been setup via terraform apply and is only used to update the container definition.

- Usage: `scripts/deploy.sh <service-name-to-deploy> <image (repository-url:version)> <environment> <cluster (name or ARN)>`

## Set Latest Versions

Useful when wanting to preserve the image versions currently in use when running a global terraform apply.

Also retrieves the browse scaler lambda function as zip and sets the location.

Usage: `source scripts/set_latest_versions.sh <environment> || return`

Sets the following TF_VARS as environment variables:
- TF_VAR_cmr_heritage_version
- TF_VAR_cmr_version
- TF_VAR_cmr_opensearch_version
- TF_VAR_cmr_service_bridge_version
- TF_VAR_cmr_browse_scaler_zip_path

## Workload

### Spin-up workload

Run `workload/spin-up-wl.sh` to create the necessary infrastructure to be able to deploy apps.
Afterwards you must push the necessary images to ECR to run the apps. Once the apps are running and stable
Run `(cd terraform/deployments/workload && terraform apply)` with the necessary TF VARS set to deploy the drivers.

### Spin-down workload

Run `workload/spin-down-wl.sh` which will suspend elasticsearch and destroy all other infrastructure.

### Workload states

Workload has the ability to run off of a remote state or a local state. To generate and run a remote state
setup run `terraform apply` in `terraform/prereqs`. When prompted for `use_workload` set to true. This will setup
remote state for the applications, core infrastructure, and workload. Next run `scripts/generate-backend <environment>` and
run as you normally would.

To run locally ensure these files do not exist / have been removed:
- `terraform/terraform_backend.tf`
- `terraform/deployments/core_infrastructure/terraform_backend.tf`
- `terraform/deployments/workload/terraform_backend.tf`

In the same directories remove your `.terraform` directory just to be safe and rerun terraform init.

The absence of `terraform_backend.tf` signals the use of local state.

## Initial provisioning of the browse scaler lambda

- You can either supply a zip location to the `browse_scaler_zip_path` tf var or leave it blank to provision a dummy zip file.
- To update browse scaler code run `scripts/deploy_lambda.sh FUNCTION_NAME ZIP_FILE_LOCATION`.

## Serverless with Terraform

Add needed terraform outputs to the `serverless_outputs` resource in `serverless.tf`.
You are then able to reference these outputs in CloudFormation / Serverless with `{cf:<environment_name>.<output_name>}`.

## Using Oracle Datapump to backup and restore the Oracle RDS instance

The various PL/SQL scripts are located in the `sql` directory at the top level.

### Backing up the datbase with Oracle Datapump

1. Set up a jump box using EC2
2. Connect to the RDS instance using DBVisualizer via an SSH tunnel through the bastion and the jumpbox
`ssh -v -L1521:<RDS INTERNAL DNS ENTRY>:1521  -J <SANDBOX BASTION IP> -i <PATH TO JUMP BOX PEM FILE> ec2-user@<JUMP BOX PRIVATE IP>`
3. Connect to the DB using DBVisualizer connecting to localhost:1521
**Note:** All of the *.sql scripts should be run in DBVisualizer.
4. Execute the `backup_to_dir.sql` script.
5. Monitor the progress of the script using the `query_backup_status.sql` script.
6. When the job completes, copy the backup files to an S3 bucket using the `upload_to_s3.sql` script.
7. Monitor the progress of the copy using the task id returned by the previous script and the `query_upload_to_s3_status.sql` script.

### Restoring from backup

1. Set up  jump box and ssh tunnel DB connection as described in steps 1 - 3 of the previous section
2. Execute the `copy_from_s3_to_rds.sql` script.
3. Monitor the status of the task using the task id from the previous step and the `query_upload_to_s3_status.sql` script (this script works for uploads and downloads).
4. Initiate the restore using the `restore_from_backup.sql` script.
5. Monitor the restore using the `monitor_restore.sql` script.
6. Once the restore is complete, query a few tables in the METADATA_DB schema to make sure the data looks correct.

NOTE: The backup and restore operations can be very slow - it took about 12 hours each in testing. Similarly the copy to/from S3 was extremely slow - taking several hours. So it's important to allow a couple of days to complete the backup.

Copyright © 2022 NASA
