#!/bin/bash
# Call from top level repo directory.
# USE set_latest_versions.sh instead!
# Set the latest versions from ECR.
# WARNING: This should only be used if set_latest_versions.sh cannot retrieve
# the task definitions due to them all being inactive.
# In the event of a rollback this will not be the latest version in use, it is the latest
# image pushed to ECR.

script_name="$(basename -- $0)"

if [ "$#" -ne 1 ]; then
  printf "USAGE: $script_name ENVIRONMENT\n"
  exit 1
fi

environment=$1

# ECR Repos
repo_names=($(cd terraform && terraform output -raw cmr_legacy_services_repository_name)
            $(cd terraform && terraform output -raw cmr_core_repository_name)
            $(cd terraform && terraform output -raw cmr_opensearch_repository_name)
            $(cd terraform && terraform output -raw cmr_service_bridge_repository_name))

# Terraform version input name. Corresponds to repo_names indices.
version_names=("cmr_heritage_version"
               "cmr_version"
               "cmr_opensearch_version"
               "cmr_service_bridge_version")

if [ ${#repo_names[@]} -ne ${#version_names[@]} ]; then
  printf "repo_names must map to version_names by index.\n"
  exit 1
fi

# Setup version variables
for i in "${!repo_names[@]}"
do
  version=$(aws ecr describe-images \
                --repository-name ${repo_names[$i]} \
                --query 'reverse(sort_by(imageDetails,& imagePushedAt)[*])' | \
                 jq -r '.[0].imageTags[0]')
  if [ -z "$version" ]; then
    printf "Unable to retrieve latest version of ${version_names[$i]}. Aborting...\n"
    exit 1
  fi

  printf "Setting ${version_names[$i]} to $version.\n"
  export TF_VAR_${version_names[$i]}=$version
done

# There is no setting browse scaler version. Instead we will grab the latest code
# running from the lambda function and deploy that.
lambda_function=$(aws lambda get-function --function-name "browse-scaler-${environment}")

# Download code
code_location=$(jq -r '.Code.Location' <<< $lambda_function)
output_location="$PWD/terraform/browse_scaler.zip"
curl -o $output_location $code_location

# Compare hashes
lambda_hash=$(jq -r '.Configuration.CodeSha256' <<< $lambda_function)
received_lambda_hash=$(openssl dgst -sha256 -binary $output_location | openssl enc -base64)
if [  $lambda_hash != $received_lambda_hash ]; then
    printf "Hashes do not equal. Consider retrying.\n"
    rm $output_location
    exit 1
fi

# Set zip location TF VAR. We set the location to be relative to the terraform directory
# To allow us to zip the lambda function with a terraform.tfplan and terraform, then
# be able to run the plan on a different machine (ie bamboo).
printf "Setting browse scaler code location.\n"
export TF_VAR_browse_scaler_zip_path="browse_scaler.zip"
