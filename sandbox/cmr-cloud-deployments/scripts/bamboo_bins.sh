#!/bin/bash
# Retrieve a copy of terraform and aws.
# Make available on path.

curr_dir=$PWD
tf_version=$(cat .terraform_version)
mkdir -p exec_bin
cd exec_bin

echo "pulling a terraform script."
if ! curl -o terraform_${tf_version}_linux_amd64.zip https://releases.hashicorp.com/terraform/${tf_version}/terraform_${tf_version}_linux_amd64.zip ; then
  echo "ERROR: coudn't download terraform script" >&2
  exit 1
else
  unzip -u terraform_${tf_version}_linux_amd64.zip
  chmod a+x terraform
  rm terraform_${tf_version}_linux_amd64.zip
fi

cat > aws <<EOS
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

# enable interruption signal handling
trap - INT TERM

docker run --rm \
	-t \$(tty &>/dev/null && echo "-i") \
	-e "AWS_ACCESS_KEY_ID=\${AWS_ACCESS_KEY_ID}" \
	-e "AWS_SECRET_ACCESS_KEY=\${AWS_SECRET_ACCESS_KEY}" \
	-e "AWS_DEFAULT_REGION=\${AWS_DEFAULT_REGION}" \
	-v "\$(pwd):/project" \
	maven.earthdata.nasa.gov/aws-cli \
	"\$@"
EOS

chmod a+x aws

export PATH=$PWD:$PATH
cd $curr_dir
