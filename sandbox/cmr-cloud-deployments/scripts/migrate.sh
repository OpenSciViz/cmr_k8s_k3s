#!/bin/bash

# Performs a database migration of a given CMR App in NGAP.
# Takes one argument, the application name, e.g., metadata-db
# Uses the following environment variables:
#  RETRY_COUNT: (optional) Number of times to retry before throwing an error.
#   Defaults to 3
#  ENVIRONMENT_NAME: sit, uat, prod, etc.
#  LOCAL_PORT - the port on the local side of the tunnel (default 8080)
#
# NOTE: This script must be run in an environment with the aws command available
# and the services accessible on a port on localhost (usually via a tunnel)
# AWS credentials should be set up
#

app_name=$1

if [ -z "${app_name}" ] ; then
    echo "ERROR: APP_NAME must be passed as the first argument" >&2
    exit 1
fi

if [ -z "${ENVIRONMENT_NAME}" ] ; then
    echo "Error: ENVIRONMENT_NAME must be set as an environment variable"
fi

local_port=${LOCAL_PORT:-8080}

# Get the ECHO system token from paraemter store
token=$(aws ssm get-parameter --with-decryption --name /${ENVIRONMENT_NAME}/access-control/CMR_ECHO_SYSTEM_TOKEN | jq -r '.["Parameter"]["Value"]')

retry_count=${RETRY_COUNT:-3}

echo "Waiting for service to stabilize before attempting database migrations."
aws ecs wait services-stable --cluster "cmr-service-${ENVIRONMENT_NAME}" --service "${app_name}-${ENVIRONMENT_NAME}"
# exit if the wait failed
[ $? -eq 0 ]  || exit 1

echo "Beginning database migrations for ${app_name}."

base_url=${DB_MIGRATION_BASE_URL}

migration_path="${app_name}"
if [ "${app_name}" = "legacy-services" ] ; then
    migration_path="${migration_path}/rest"
fi
migration_path="${migration_path}/db-migrate"
db_migrate_text=$(mktemp db_migrate_output.XXXXXXXXXXXXXXXXXXX)

while [ $retry_count -gt 0 ] ; do


    echo "running: curl -XPOST -H \"Authorization: TOKEN\" \
    \"http://localhost:${local_port}/${migration_path}\" up to $retry_count times"
    if ! http_code=$(curl -XPOST -H "Authorization: ${token}" \
        -w "%{http_code}" \
        -o "$db_migrate_text" \
        "http://localhost:${local_port}/${migration_path}" -d '') ; then
        status=$?
        echo "ERROR: db migrate of ${app_name} failed with status $status" >&2
        # Interestingly, if the curl command failed (as above), then the http_code
        # is set to octal '000'.
        exit $status
    else
        # Under _normal_ circumstances, a successful migration should return a 20x.
        # This code was copied from somewhere else. It's possible that a 3xx response
        # could be legitimate, but we'd like to fail on this post anyway if that's
        # the case.
        if [ $http_code -lt 300 ] ; then
            # If we get to here, the curl command worked, and we're done.
            echo "Completed migrating db for $app_name"
            break
        fi

        echo "WARNING: db migrate of ${app_name} failed with http status code of ${http_code}.\
        Retrying up to $retry_count more times..." >&2
    fi
    # The db migration could take minutes to run, but the db migrate call itself through Net::HTTP
    # will timeout in 60 seconds. We sleep for 5 minutes before the next db migrate run hoping
    # by the time we call back, all migration tasks have been done and it returns right away
    sleep 300
    retry_count=$((retry_count-1))
done
if [ $retry_count -eq 0 ] ; then
    if [ -r $db_migrate_text ] ; then
        cat $db_migrate_text
    fi
    echo
    echo "ERROR: ran out of retries, db migrate of ${app_name} failed with http code ${http_code}" >&2
    exit 1
fi
