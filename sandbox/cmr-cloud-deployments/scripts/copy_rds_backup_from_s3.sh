#!/bin/bash
CMR_BACKUP_BUCKET=${CMR_BACKUP_BUCKET:-cmr-sandbox-oracle-backups}
file=$1

if [ -z ${file+x} ]; then 
    echo "Usage: copy_rds_backup_from_s3.sh <FILE NAME>"
    exit 1
fi
aws s3 cp "s3://$CMR_BACKUP_BUCKET/$file" "./$file"