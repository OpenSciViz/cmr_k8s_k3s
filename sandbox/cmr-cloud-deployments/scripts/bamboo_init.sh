#!/bin/bash
# Setup environment and initialize backend.
# Should be called from the root directory of this repository.
# Expect the following environment variables to be set before use:
#   bamboo_AWS_ACCESS_KEY_ID_PASSWORD
#   bamboo_AWS_SECRET_ACCESS_KEY_PASSWORD
#   bamboo_AWS_DEFAULT_REGION

# Abort on failures
set -e

# Ensure environment variables are set
export AWS_ACCESS_KEY_ID="${bamboo_AWS_ACCESS_KEY_ID_PASSWORD:?Need to set bamboo_AWS_ACCESS_KEY_ID_PASSWORD.}"
export AWS_SECRET_ACCESS_KEY="${bamboo_AWS_SECRET_ACCESS_KEY_PASSWORD:?Need to set bamboo_AWS_SECRET_ACCESS_KEY_PASSWORD.}"
export AWS_DEFAULT_REGION="${bamboo_AWS_DEFAULT_REGION:?Need to set bamboo_AWS_DEFAULT_REGION.}"
export ENVIRONMENT_NAME="${bamboo_ENVIRONMENT_NAME:?Need to set bamboo_ENVIRONMENT_NAME}"
export RELEASE_VERSION="${RELEASE_VERSION:?Need to set RELEASE_VERSION}"

# Generate backend
./scripts/generate-backend.sh $ENVIRONMENT_NAME
(cd terraform && terraform init -input=false -no-color)
