#!/bin/bash
# Extract parameters from parameter store for environment and place in respective json files.
# Creates json file per app name.
# Creates files in output_directory

set -e

script_name="$(basename $0)"

if [ "$#" -ne 2 ]; then
  printf "Usage: $script_name environment_name output_directory\n"
  exit 1
fi

environment_name=$1
output_directory=$2
found_apps=()
aws_params=$(aws ssm get-parameters-by-path --path /$environment_name/ --recursive --with-decrypt)
readable_params=$(jq '[.Parameters[] | select((.Name | split("/") | length) == 4)]' <<< $aws_params)
unreadable_params=$(jq -c -r '.Parameters[] | select((.Name | split("/") | length) != 4)' <<< $aws_params)

if [ ! -z "$unreadable_params" ]; then
  echo "The following parameters are not in the correct parsing format of /env/app-name/ENVIRONMENT_VAR_NAME. Skipping: "
  echo $unreadable_params
fi

while read -r app_name; do
  if [[ ! " ${found_apps[@]} " =~ " ${app_name} " ]]; then
    jq -r --arg app_name $app_name '[.Parameters[] |
          select((.Name | split("/")[2])==$app_name) |
          {"key": (.Name | split("/")[3]), "value": (.Value | tostring)}] |
          from_entries' <<< $aws_params > "${output_directory}/${app_name}.json"
    found_apps+=("$app_name")
  fi
done< <(jq --raw-output '.[] | "\(.Name | split("/")[2])"' <<< $readable_params)
