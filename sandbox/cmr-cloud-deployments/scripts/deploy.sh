#!/bin/bash
# Should be called from the root directory of this repository.
# This script performs the following actions:
#   Retrieve the current task definition of the service.
#   Retrieve all parameters from parameter store and replace in container definition.
#   Replace image in container definition.
#   Register a new task definition.
#   Update the ECS service with the new task definition.
# Expects the following command line arguments:
#   SERVICE-NAME: The name of the ECS service being deployed (ex access-control, search, etc.).
#   image: The image location to change the container to.
#   environment: The environment to deploy in.
#   cluster: The cluster name or ARN to deploy to.

set -e

script_name="$(basename $0)"

if [ "$#" -ne 4 ]; then
  printf "USAGE: $script_name service-name image environment cluster\n"
  exit 1
fi

service_name=$1
image=$2
environment=$3
cluster=$4
task_def_name="${service_name}-${environment}"

printf "Getting task definition.\n"
task_def=$(aws ecs describe-task-definition --task-definition $task_def_name | jq .taskDefinition)

# Make changes to the container definition
container_def=$(jq .containerDefinitions[0] <<< $task_def)

# Change environment variables
printf "Replacing non secret parameters.\n"
non_secret_parameters=$(./scripts/parameter_store.sh /${environment}/${service_name}/ String | jq '.ENVS | fromjson')
if [ -z "$non_secret_parameters" ]; then
  non_secret_parameters = []
fi
container_def=$(jq --arg envs "$non_secret_parameters" '.environment = ($envs | fromjson)' <<< $container_def)

# Change secret variables
printf "Replacing secret parameters.\n"
secret_parameters=$(./scripts/parameter_store.sh /${environment}/${service_name}/ SecureString | jq '.ENVS | fromjson')
if [ -z "$secret_parameters" ]; then
  secret_parameters = []
fi
container_def=$(jq --arg envs "$secret_parameters" '.secrets = ($envs | fromjson)' <<< $container_def)

printf "Replacing image.\n"
container_def=$(jq --arg image "$image" '.image = $image' <<< $container_def)

# Register a new task definition with everything the same except container definition
printf "Creating new task definition.\n"
task_def_file=$(mktemp aws_ecs_task_def_tmp.json.XXXXXXXXXXXXXXXXXXX)
jq --arg container_def "$container_def" \
   '{family,
     taskRoleArn,
     executionRoleArn,
     networkMode,
     volumes,
     placementConstraints,
     requiresCompatibilities,
     cpu,
     memory,
     tags,
     pidMode,
     ipcMode,
     proxyConfiguration} +
     {containerDefinitions: [($container_def | fromjson)]} |
     to_entries | map(select(.value != null)) | from_entries' <<< $task_def > $task_def_file

printf "Registering new task definition.\n"
registered_task_def=$(aws ecs register-task-definition --cli-input-json "file://$task_def_file")
rm $task_def_file

printf "Updating service to use new task definition.\n"
registered_task_def_arn=$(jq -r '.taskDefinition.taskDefinitionArn' <<< $registered_task_def)
# Slightly misleading with --service "$task_def_name":
# This is actually the service-name with the environment appended to the end.
# This is already done for task_def_name so we use that variable here.
aws ecs update-service --cluster $cluster --service $task_def_name --task-definition $registered_task_def_arn
