#!/bin/bash
# Deploy services. Wait for services to be stable.
# Should be called from the root directory of this repository.
# Expects bamboo_init.sh and bamboo_ecr.sh to be sourced beforehand.
# Expects all necessary TF VARS to be stored as environment variables.
# Expects the following command line arguments:
#   SERVICE-NAMES: Space separated list of SERVICE-NAMES to deploy.

# Abort on failures
set -e

script_name="$(basename $0)"

if [ "$#" -lt 1 ]; then
  printf "Usage: $script_name SERVICE-NAME-1 SERVICE-NAME-2 ... SERVICE-NAME-N\n"
  exit 1
fi

cluster=$(cd terraform && terraform output -raw cmr_cluster_arn)

for service in "$@"
do
  if [ $service = "access-control" ] || [ $service = "search" ] || [ $service = "ingest" ]; then
    set -x
    aws ssm put-parameter --name "/$ENVIRONMENT_NAME/$service/CMR_RELEASE_VERSION" \
                          --value "$RELEASE_VERSION" \
                          --type String \
                          --overwrite
    set +x
  fi
  if [ $service = "legacy-services" ]; then
    set -x
    aws ssm put-parameter --name "/$ENVIRONMENT_NAME/$service/CMR_DEPLOYMENT_VERSION" \
                          --value "$RELEASE_VERSION" \
                          --type String \
                          --overwrite
    set +x
  fi
  if [ $service = "opensearch" ]; then
    set -x
    aws ssm put-parameter --name "/$ENVIRONMENT_NAME/$service/USE_CWIC_SERVER" \
                          --value "$USE_CWIC_SERVER" \
                          --type String \
                          --overwrite
    set +x
  fi
  ./scripts/deploy.sh $service $IMAGE $ENVIRONMENT_NAME $cluster
done

# Wait for service to become steady
# Append -environment to apps
./scripts/ecs_wait_for_steady_service.sh $cluster "${@/%/-$ENVIRONMENT_NAME}"
