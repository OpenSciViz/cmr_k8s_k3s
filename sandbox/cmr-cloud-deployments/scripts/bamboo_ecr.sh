#!/bin/bash
# Retrieve repository name and url from terraform. ECR must have already been setup beforehand.
# Should be called from the root directory of this repository.
# Expects bamboo_init.sh to be sourced beforehand.
# Expects all necessary TF VARS to be stored as environment variables.
# Expects the follow command line argument:
#   terraform_ecr_resource_name: The ECR resource name in terraform. Ex. cmr_core, cmr_opensearch
#   dockerfile_path: The path to the dockerfile to build and push.

# Abort on failures
set -e

script_name=$(basename -- $0)

if [ "$#" -ne 2 ]; then
  printf "Usage: $script_name terraform_ecr_resource_name dockerfile_path\n"
  exit 1
fi

terraform_ecr_resource_name=$1
dockerfile_path=$2

# ECR repo info grabbed from terraform outputs
ecr_repo_name=$(cd terraform && terraform output -raw ${terraform_ecr_resource_name}_repository_name)
ecr_repo_url=$(cd terraform && terraform output -raw ${terraform_ecr_resource_name}_repository_url)

# Build and push Docker Image to ECR
./scripts/ecr.sh $dockerfile_path $RELEASE_VERSION $ecr_repo_name $ecr_repo_url

export IMAGE="${ecr_repo_url}:${RELEASE_VERSION}"
