#!/bin/bash

# create an ssh tunnel through a jumpbox
# Environment variables
# SSH_PRIVATE_KEYFILE - the path to the ssh key file for the jumpbox (default ~/.ssh/id_rsa)
# LOCAL_PORT - the port on the local side of the tunnel (default 8080)
# REMOTE_PORT - the port on the remote host to which to connect (default 80)
# REMOTE_HOST - the address of the host to which to connect
# SSH_USER - the user to connect as (defaults to ec2-user)
# JUMP_BOX - the instance ID of the ec2 jumpbox to use, (optional, will be determined by query if not set)
# JUMP_BOX_FILTER - the name filter to use to find a jumpbox, defaults to `*SSM Jumpbox*`

ssh_private_key_file=${SSH_PRIVATE_KEY_FILE:-~/.ssh/id_rsa}
local_port=${LOCAL_PORT:-8080}
remote_port=${REMOTE_PORT:-80}
user=${SSH_USER:-ec2-user}
jump_box=${JUMP_BOX}
jump_box_filter=${JUMP_BOX_FILTER:-*SSM Jumpbox*}

if [ -z "$jump_box" ]; then
  echo "Fetching jumpbox from AWS"
  jump_box=$(aws ec2 describe-instances \
             --filter "Name=tag:Name,Values=${jump_box_filter}" \
             --max-items 1 \
             --query "Reservations[].Instances[?State.Name == 'running'].InstanceId[]" \
             --output text | sed -r 's/\s+//g')
fi

if [ -z "$jump_box" ]; then
  echo "A running Jumpbox could not located using filter [ \"Name=tag:Name,Values=${jump_box_filter}\" ]"
  exit -1
fi

ssh -i ${ssh_private_key_file} -f -N \
    -L ${local_port}:${remote_host}:${remote_port} \
    ${user}@${jump_box} \
    -o ProxyCommand="aws ssm start-session --target %h --document-name AWS-StartSSHSession --parameters 'portNumber=%p'" \
    -o ServerAliveInterval=60 \
    -o UserKnownHostsFile=/dev/null \
    -o StrictHostKeyChecking=no
