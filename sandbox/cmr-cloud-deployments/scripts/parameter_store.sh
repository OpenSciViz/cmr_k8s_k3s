#!/bin/bash

# Retrieve and parse path parameters in order to be used as environment variables.
# Secret environment variables take the form {"name": "<var name>", "valueFrom": "<ARN of value>"}
# Non secret environment variables take the form {"name": "<var name>", "value": "<value>"}
if [ $# -ne 2 ]; then
  echo "Must supply arguments: <path> <parameter-type (String or SecureString)>"
  exit 1
fi

if [ "$2" == "SecureString" ]; then
  value=".ARN"
  value_str="valueFrom"
elif [ "$2" == "String" ]; then
  value=".Value"
  value_str="value"
else
  echo "String type must be String or SecureString."
  exit 1
fi

# Terraform only allows json objects with string values to be used as an
# external data source. The below converts the result of parsing the environment
# variables into a string. The map returned is in the form of:
# {"ENVS": "[<all-environment-variables>]"}
# The string is then decoded in the container definition.

# Parameter store does not allow empty values.
# The below assumes "\"\"" represents an empty value.
# If the value is == "\"\"" set value to "".
aws ssm get-parameters-by-path --path $1 --recursive | \
  jq "[.Parameters[] |
  select(.Type == \"$2\")] |
  map({\"name\": (.Name | split(\"/\")[-1]), \"$value_str\": (if $value == \"\\\"\\\"\" then \"\" else $value end)}) |
  {\"ENVS\": tostring}"
