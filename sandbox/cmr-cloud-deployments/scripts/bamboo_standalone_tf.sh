#!/bin/bash
# Used to run a global terraform apply. Retrieves latest versions to ensure
# version state does not change for each module.
# Generates the output plan

set -e

# Setup aws environment variables
export AWS_ACCESS_KEY_ID="${bamboo_AWS_ACCESS_KEY_ID_PASSWORD:?Need to set bamboo_AWS_ACCESS_KEY_ID_PASSWORD.}"
export AWS_SECRET_ACCESS_KEY="${bamboo_AWS_SECRET_ACCESS_KEY_PASSWORD:?Need to set bamboo_AWS_SECRET_ACCESS_KEY_PASSWORD.}"
export AWS_DEFAULT_REGION="${bamboo_AWS_DEFAULT_REGION:?Need to set bamboo_AWS_DEFAULT_REGION.}"
export TF_VAR_environment_name="${bamboo_ENVIRONMENT_NAME:?Need to set bamboo_ENVIRONMENT_NAME}"

# Generate backend
./scripts/generate-backend.sh $TF_VAR_environment_name
(cd terraform && terraform init)

source scripts/set_latest_versions.sh $TF_VAR_environment_name

echo "run terraform plan"
(cd terraform && terraform plan -out="terraform.tfplan" -no-color)
