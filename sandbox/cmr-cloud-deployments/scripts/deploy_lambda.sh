#!/bin/bash
# Update lambda function code.
# Expects the following command line arguments:
#   FUNCTION_NAME: The name of the lambda function to update.
#   ZIP_FILE_LOCATION: The location of the zip file with the code to update.

set -e

script_name=$(basename $0)

if [ $# -ne 2 ]; then
  printf "Usage: $script_name FUNCTION_NAME ZIP_FILE_LOCATION\n"
  exit 1
fi

function_name=$1
zip_file_location=$2

# Update lambda function code
aws lambda update-function-code --function-name $function_name \
                                --zip-file fileb://$zip_file_location
