#!/bin/bash
# Subscribe emails to an SNS topic if they are not already subscribed.
# Unsupported email option in terraform sns_topic_subscription resource
# is the reason for the need of this script.
# Options:
#   update: Diffs the emails given on the cli with the emails currently subscribed. Updates accordingly.
#   sub: Simply subscribes the provided emails.
#   unsub: Unsub the emails provided if subscription exists.
#   unsub_all: Unsub all subscriptions from topic.

set -e

# First argument is topic_arn, rest are email addresses
function subscribe_emails {
  current_subscriptions=$(aws sns list-subscriptions-by-topic --topic-arn $topic_arn | \
                          jq '[.Subscriptions[] | .Endpoint]')
  local topic_arn=$1

  for email in "${@:2}"
  do
    if [[ $current_subscriptions != *"$email"* ]]; then
      printf "Subscribing $email.\n"
      aws sns subscribe --topic-arn $topic_arn --protocol email --notification-endpoint $email
    else
      printf "$email is already subscribed.\n"
    fi
  done
}

# First argument is topic_arn, rest are email addresses
function unsubscribe_emails {
  current_subscriptions=$(aws sns list-subscriptions-by-topic --topic-arn $topic_arn | \
                          jq '.Subscriptions[]')
  local topic_arn=$1

  for email in "${@:2}"
  do
    subscription_arn=$(jq -r --arg email $email 'select(.Endpoint == $email) | .SubscriptionArn' <<< $current_subscriptions)
    if [ ! -z "$subscription_arn" ] && [ "$subscription_arn" != "PendingConfirmation" ]; then
      printf "Unsubscribing $email.\n"
      aws sns unsubscribe --subscription-arn "$subscription_arn"
    else
      printf "$email is not subscribed.\n"
    fi
  done
}

if [ $# -lt 2 ]; then
  printf "Usage: <SNS-Topic-Arn> <update | sub | unsub | unsub_all> <email-1> <email-2> ... <email-n>\n"
  exit 1
fi

topic_arn=$1
option=$2

if [ $option = "sub" ]; then
  subscribe_emails $topic_arn "${@:3}"
elif [ $option = "unsub" ]; then
  unsubscribe_emails $topic_arn "${@:3}"
elif [ $option = "unsub_all" ]; then
  emails=$(aws sns list-subscriptions-by-topic --topic-arn $topic_arn | jq -r '.Subscriptions[] | .Endpoint')
  unsubscribe_emails $topic_arn $emails
elif [ $option = "update" ]; then
  emails_to_unsub=""
  while read subbed_email; do
    if ! echo "$@" | grep -q "$subbed_email"; then
      emails_to_unsub="$subbed_email $emails_to_unsub"
    fi
  done < $(aws sns list-subscriptions-by-topic --topic-arn $topic_arn | jq -r -c '.Subscriptions[] | .Endpoint')
  subscribe_emails $topic_arn "${@:3}"
  unsubscribe_emails $topic_arn $emails_to_unsub
else
  printf "Don't know option $option. Must be either sub, unsub, unsub_all, or update.\n"
  exit 1
fi
