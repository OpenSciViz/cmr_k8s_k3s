#!/bin/bash
# Waits for an ECS service to become steady.
# aws ecs wait services-stable just waits for desiredCount to equal runningCount.
# This does not wait for containers to pass health checks and become healthy so
# it is possible for services-stable to return success and right after fargate to
# kill a container which could result in 502's and confusion.
# There is no aws cli call for steady-services, we will ready the messages stream
# for a "has reached a steady state." event.

if [ $# -lt 2 ]; then
  printf "Usage: $0 <cluster> <service 1> <service 2> ... <service n>\n"
  exit 1
fi

cluster=$1
status=0
# Wait for service to stabilize
for (( i=2; i<="$#"; i+=10 ))
do
  # Can only wait for 10 services at a time
  services_to_wait=("${@:$i:${i}+9}")
  services_to_wait="${services_to_wait[@]}"
  printf "Waiting for $services_to_wait service(s) to become steady.\n"

  # Total count time in minutes = RETRY_COUNT * 0.25
  # Default is 40 minutes.
  retry_count=${RETRY_COUNT:-160}
  while [ $retry_count -gt 0 ] ; do
    while read service; do
      service_name=$(jq -r '.serviceName' <<< $service)
      # Use wait services-stable for desired count of 0 to wait for app to spin-down.
      if [ $(jq -r '.desiredCount' <<< $service) -eq 0 ]; then
        aws ecs wait services-stable --cluster "$cluster" --service $service_name
        if [ $? -eq 0 ]; then
          is_steady="true"
        else
          is_steady="false"
        fi
      else
        created_at=$(jq -r '.deployments | sort_by(.createdAt) | reverse | .[0].createdAt' <<< $service)
        is_steady=$(jq -r \
                       --argjson created_at $created_at \
                       '[.events[] |
                       select(.createdAt >= $created_at) |
                       .message |
                       contains("has reached a steady state.")] |
                       any' <<< $service)
      fi
      if [ $is_steady = "true" ]; then
        printf "$service_name has become steady.\n"
        services_to_wait="${services_to_wait/$service_name/}"
      fi
    done < <(aws ecs describe-services --cluster "$cluster" --service $services_to_wait |\
             jq -r -c '.services[]')
    if [ -z "${services_to_wait// }" ]; then
      retry_count=0
    else
      retry_count=$((retry_count-1))
      if [ $retry_count -eq 0 ]; then
        printf "$services_to_wait failed to become steady.\n"
        status=1
      else
        sleep 15
      fi
    fi
  done
done

exit $status
