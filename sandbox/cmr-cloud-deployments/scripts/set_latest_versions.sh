#!/bin/bash
# Retrieve latest versions from container definitions
# Get latest ACTIVE task definition and retrieve version from that image being used.
# Call from root repo directory.
# Retrieves latest lambda code for browse scaler and sets the zip location to that.
# Expects the following command line arguments:
#   ENVIRONMENT: Environment name

script_name="$(basename -- $0)"

if [ "$#" -ne 1 ]; then
  printf "USAGE: $script_name ENVIRONMENT\n"
  exit 1
fi

environment=$1

# Task definition families to retrieve latest image tag from.
task_definition_families=("legacy-services-${environment}"
                          "indexer-${environment}"
                          "opensearch-${environment}"
                          "service-bridge-${environment}")

# Terraform version input name. Corresponds to task_definition_families indices.
version_names=("cmr_heritage_version"
               "cmr_version"
               "cmr_opensearch_version"
               "cmr_service_bridge_version")

if [ ${#task_definition_families[@]} -ne ${#version_names[@]} ]; then
  printf "task_definition_families must map to version_names by index.\n"
  exit 1
fi

# Setup version variables
for i in "${!task_definition_families[@]}"
do
  version=$(aws ecs describe-task-definition \
                --task-definition ${task_definition_families[$i]} | \
                jq -r '.taskDefinition.containerDefinitions[0].image | split(":")[1]')
  if [ -z "$version" ]; then
    printf "Unable to retrieve latest version of ${version_names[$i]}. Aborting...\n"
    exit 1
  fi

  printf "Setting ${version_names[$i]} to $version.\n"
  export TF_VAR_${version_names[$i]}=$version
done

# There is no setting browse scaler version. Instead we will grab the latest code
# running from the lambda function and deploy that.
lambda_function=$(aws lambda get-function --function-name "browse-scaler-${environment}")

# Download code
code_location=$(jq -r '.Code.Location' <<< $lambda_function)
output_location="$PWD/terraform/browse_scaler.zip"
curl -o $output_location $code_location

# Compare hashes
lambda_hash=$(jq -r '.Configuration.CodeSha256' <<< $lambda_function)
received_lambda_hash=$(openssl dgst -sha256 -binary $output_location | openssl enc -base64)
if [  $lambda_hash != $received_lambda_hash ]; then
    printf "Hashes do not equal. Consider retrying.\n"
    rm $output_location
    exit 1
fi

# Set zip location TF VAR. We set the location to be relative to the terraform directory
# To allow us to zip the lambda function with a terraform.tfplan and terraform, then
# be able to run the plan on a different machine (ie bamboo).
printf "Setting browse scaler code location.\n"
export TF_VAR_browse_scaler_zip_path="browse_scaler.zip"
