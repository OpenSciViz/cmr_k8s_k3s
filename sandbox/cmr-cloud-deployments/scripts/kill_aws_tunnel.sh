#!/bin/bash

# kill a tunnel set up for port forwarding
tunnel_pid=$(ps aux | grep ssh | grep "\-L" | awk '{print $2}')
if [ ! -z "$tunnel_pid" ];
then
  echo "killing ssh tunnel $tunnel_pid"
  kill $tunnel_pid
fi
