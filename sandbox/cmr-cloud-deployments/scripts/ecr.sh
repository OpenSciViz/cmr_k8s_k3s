#!/bin/bash
# Build docker image and push to ECR.

# Usage bash ecr.sh <docker-file-path> <release-version> <ecr-repo-name> <ecr-repo-url>
if [ $# -ne 4 ]; then
  printf "$(date) Missing arguments DOCKER_FILE_PATH RELEASE_VERSION ECR_REPO_NAME ECR_REPO_URL. Exiting.\n"
  exit 1
fi

# Abort on failures
set -e

# The directory path to the dockerfile
dockerfile_directory_path=$(dirname $1)

# The dockerfile filename to use
dockerfile_file_name=$(basename $1)

# Release version to tag.
release_version=$2

# Obtain repository name and url from terraform outputs
repo_name=$3
repo_url=$4

images=$(aws ecr \
             describe-images \
             --repository-name $repo_name \
             --query 'reverse(sort_by(imageDetails,& imagePushedAt)[*])')

while read image_tag; do
  if [ "$image_tag" = "$release_version" ]; then
    printf "$(date) Image with release version $release_version already exists in ECR.\n"
    exit 0
  fi
done < <(jq -r '.[].imageTags[]?' <<< $images)

# Build image and tag with release version.
(cd $dockerfile_directory_path && docker build -t $repo_name:$release_version -f $dockerfile_file_name .)

# Push to ECR
# Need to create a temporary file with the docker login command.
# Call dos2unix to ensure no issues on login.
temp_docker_login_file=$(mktemp tmp.docker.login.file.XXXXXXXXXXXXXXXXXXX)
aws ecr get-login --no-include-email > $temp_docker_login_file
chmod 755 $temp_docker_login_file
dos2unix $temp_docker_login_file
sh $temp_docker_login_file
rm $temp_docker_login_file

# Tag with release version
docker tag $repo_name:$release_version $repo_url:$release_version
docker push $repo_url:$release_version

# Cleanup image locally
docker rmi -f \
  $(docker inspect ${repo_name}:${release_version} \
  | jq -r .[].Id \
  | cut -f2 -d: \
  )

# Ensure only latest two images remain in ECR
images_to_delete=""
while read image_digest; do
  images_to_delete="$images_to_delete imageDigest=$image_digest"
done < <(jq -r '.[1:] | .[].imageDigest' <<< $images)

if [[ ! -z "$images_to_delete" ]]; then
  aws ecr batch-delete-image --repository-name $repo_name --image-ids $images_to_delete
fi
