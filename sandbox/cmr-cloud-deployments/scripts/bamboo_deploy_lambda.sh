#!/bin/bash
# Deploy lambda from bamboo.
# Should be called from the root directory of this repository.
# Expects bamboo_init.sh to be sourced beforehand.
# Expects all necessary TF VARS to be stored as environment variables.
# Expects the following command line arguments:
#   TERRAFORM_LAMBDA_RESOURCE_NAME: The name of the lambda resource in terraform, ex browse_scaler.
#     The following rules are assumed:
#       There exists a terraform output:
#         The Lambda Function Name. Follows naming convention <TERRAFORM_LAMBDA_RESOURCE_NAME>_function_name
#   ZIP_FILE_LOCATION: The location of the zip file to deploy.

set -e

script_name=$(basename $0)

if [ $# -ne 2 ]; then
  printf "Usage: $script_name TERRAFORM_LAMBDA_RESOURCE_NAME ZIP_FILE_LOCATION\n"
  exit 1
fi

function_name=$(cd terraform && terraform output -raw ${1}_function_name)
zip_file_location=$2

./scripts/deploy_lambda.sh $function_name $zip_file_location
