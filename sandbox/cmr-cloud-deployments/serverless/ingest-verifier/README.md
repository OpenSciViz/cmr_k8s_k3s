# ingest-verifier

The Ingest Verifier runs a query against the `cmr-ingest-<env>` log groups for successfully ingested concepts.
The verifier will then run a search against the public CMR search endpoint for that query. If the concept is not found an alert is sent over the alerts SNS topic.

The default schedule is to run every 10 minutes with a 10 minute window and 5 minute delay in the search. e.g. If the lambda runs at 12:00:00 the search window will be for 11:45:00 - 11:55:00

## Deployment and Updates

### Prerequisite
Have a bucket prepared to run the deployment from.

    export AWS_PROFILE=cmr-<sit|uat|prod|wl>
    aws s3 mb s3://ingest-verifier-lambda-deployment-bucket-<sit|uat|prod|wl> --region us-east-1


Running this will deploy the lambda as a CloudFormation stack.

    export AWS_PROFILE=cmr-<sit|uat|prod|wl>
    npm install
    npm run deploy -- --stage <sit|uat|prod|wl>


## Removal

Running this will remove the CloudFormation stack.

    export AWS_PROFILE=cmr-<sit|uat|prod|wl>
    npm install
    npm run remove -- --stage <sit|uat|prod|wl>

## Configuration

Configuration is done through the use of environment variables. These are generally set through the serverless.yml file and should not need to be updated manually.

| Environment Variable | Description                         | Default                                   |
|----------------------|-------------------------------------|-------------------------------------------|
| *CMR_SEARCH_URL*     |                                     | https://cmr.sit.earthdata.nasa.gov/search |
| *REGION*             | aws-region                          | us-east-1                                 |
| *ENVRIONMENT*        | which CMR environment this is for   | test                                      |
| *SEARCH_WINDOW_SIZE* | size in minutes to search over      | 10                                        |
| *SEARCH_DELAY*       | size in minutes to go back from now | 5                                         |

## Development Commands

Run linter
```bash
npm run lint
```

Run tests
```bash
npm run test
```

Run tests and watch for changes
```bash
npm run test -- --watch
```

Invoke the Lambda manually
```bash
export AWS_PROFILE=cmr-<wl|sit|uat|prod>
serverless invoke --function verifyIngest --stage <wl|sit|uat|prod>
```
