const { mockClient } = require('aws-sdk-client-mock');
const { SSMClient, GetParameterCommand } = require('@aws-sdk/client-ssm');
const {
  CloudWatchLogsClient,
  GetQueryResultsCommand,
  StartQueryCommand,
} = require('@aws-sdk/client-cloudwatch-logs');

const { SNSClient, PublishCommand } = require('@aws-sdk/client-sns');

const mockSSM = mockClient(SSMClient);
const mockCloudWatchLogsClient = mockClient(CloudWatchLogsClient);
const mockSNS = mockClient(SNSClient);
const nock = require('nock');
const chai = require('chai');
const handler = require('../handler');

const { expect } = chai;

const message = '2022-03-15 16:00:30.457 ip-10-5-39-171.ec2.internal [318b6d3a-0ac2-4bed-ac3f-63b74f1b2c9d] INFO [cmr.ingest.api.core] - Successfully ingested {:format "application/echo10+xml", :native-id "gc", :concept-type :granule, :provider-id "CMR_ONLY", :concept-id "G1200416251-CMR_ONLY", :revision-id 3, :metadata-size-bytes 13559} from client super-squirrel, token_type Legacy-EDL';

const deletedMessage = '2022-03-23 18:51:52.727 ip-10-5-39-187.ec2.internal [f03192c6-785a-4c15-a41a-a5f95e0576bb] INFO [cmr.ingest.api.core] - Deleting collection {:provider-id "CMR_ONLY", :native-id "cc", :concept-type :collection, :user-id "super-duper"} from client super-squirrel';

beforeEach(() => {
  mockSSM.reset();
  mockCloudWatchLogsClient.reset();
  mockSNS.reset();

  if (!nock.isActive()) {
    nock.activate();
  }
});

after(nock.cleanAll);
afterEach(nock.restore);

describe('Log Message Parsing', () => {
  it('extracts concept additional information from a log message when present', () => {
    expect(handler.extractConcept(message)).to.include({
      /* eslint-disable quote-props */
      'format': 'application/echo10+xml',
      'native-id': 'gc',
      'concept-id': 'G1200416251-CMR_ONLY',
      'revision-id': 3,
      'concept-type': 'granule',
      'provider-id': 'CMR_ONLY',
      'metadata-size-bytes': 13559,
      /* eslint-enable quote-props */
    });
  });
});

describe('Alert case - ingest detected but no concept found', () => {
  it('should alert SNS when an ingest is found but CMR search fails', async () => {
    nock('http://localhost:3003', {
      reqheaders: {
        Authorization: (headerValue) => headerValue.includes('max'),
      },
    })
      .get(/.*/)
      .reply(200, {
        items: [],
      });

    mockSSM
      .on(GetParameterCommand)
      .resolves({
        Parameter: { Value: 'max' },
      });

    mockCloudWatchLogsClient
      .on(StartQueryCommand)
      .resolvesOnce({ queryId: 'ingestQuery' })
      .resolvesOnce({ queryId: 'deletedQuery' })

      .on(GetQueryResultsCommand, { queryId: 'ingestQuery' })
      .resolves({
        status: 'Complete',
        results: [[{
          field: '@message',
          value: message,
        }]],
      })

      .on(GetQueryResultsCommand, { queryId: 'deletedQuery' })
      .resolves({ status: 'Complete', results: [] });

    mockSNS
      .on(PublishCommand)
      .resolves({});

    await handler.run();

    expect(mockSNS.calls()).to.have.length(1);
  });
});

describe('Success case - ingest detected and concept found', () => {
  it('should not alert SNS when an ingest is found and CMR finds the concept', async () => {
    nock('http://localhost:3003', {
      reqheaders: {
        Authorization: (headerValue) => headerValue.includes('foo'),
      },
    })
      .get(/.*/)
      .reply(200, {
        items: [{
          'concept-id': 'C123467890-TEST',
          meta: {
            /* eslint-disable quote-props */
            'concept-type': 'granule',
            'concept-id': 'G1200416251-CMR_ONLY',
            'revision-id': 3,
            'native-id': 'gc',
            'provider-id': 'CMR_ONLY',
            'format': 'application/echo10+xml',
            'revision-date': '2022-03-15T16:00:30.377Z',
            /* eslint-enable quote-props */
          },
        }],
      });

    mockSSM
      .on(GetParameterCommand)
      .resolves({
        Parameter: { Value: 'foo' },
      });

    mockCloudWatchLogsClient
      .on(StartQueryCommand)
      .resolvesOnce({ queryId: 'ingestQuery' })
      .resolvesOnce({ queryId: 'deletedQuery' })

      .on(GetQueryResultsCommand, { queryId: 'ingestQuery' })
      .resolves({
        status: 'Complete',
        results: [[{
          field: '@message',
          value: message,
        }]],
      })

      .on(GetQueryResultsCommand, { queryId: 'deletedQuery' })
      .resolves({
        status: 'Complete',
        results: [],
      });

    mockSNS
      .on(PublishCommand)
      .resolves({});

    await handler.run();

    expect(mockSNS.calls()).to.have.length(0);
  });
});

describe('Alert case - ingest detected and concept found but incorrect revision', () => {
  it('should alert SNS when an ingest is found and CMR finds an older revision id', async () => {
    nock('http://localhost:3003', {
      reqheaders: {
        Authorization: (headerValue) => headerValue.includes('foo'),
      },
    })
      .get(/.*/)
      .reply(200, {
        items: [{
          meta: {
            /* eslint-disable quote-props */
            'concept-type': 'granule',
            'concept-id': 'G1200416251-CMR_ONLY',
            'revision-id': 1,
            'native-id': 'gc',
            'provider-id': 'CMR_ONLY',
            'format': 'application/echo10+xml',
            'revision-date': '2022-03-15T16:00:30.377Z',
            /* eslint-enable quote-props */
          },
        }],
      });

    mockSSM
      .on(GetParameterCommand)
      .resolves({
        Parameter: { Value: 'foo' },
      });

    mockCloudWatchLogsClient
      .on(StartQueryCommand)
      .resolvesOnce({ queryId: 'ingestQuery' })
      .resolvesOnce({ queryId: 'deletedQuery' })

      .on(GetQueryResultsCommand, { queryId: 'ingestQuery' })
      .resolves({
        status: 'Complete',
        results: [[{
          field: '@message',
          value: message,
        }]],
      })

      .on(GetQueryResultsCommand, { queryId: 'deletedQuery' })
      .resolves({
        status: 'Complete',
        results: [],
      });

    mockSNS
      .on(PublishCommand)
      .resolves({});

    await handler.run();

    expect(mockSNS.calls()).to.have.length(1);
  });
});

describe('Error Handling', () => {
  it('should handle long-running queries correctly', async () => {
    nock('http://localhost:3003', {
      reqheaders: {
        Authorization: (headerValue) => headerValue.includes('foo'),
      },
    })
      .get(/.*/)
      .reply(200, {
        items: [{
          meta: {
            /* eslint-disable quote-props */
            'concept-type': 'granule',
            'concept-id': 'G1200416251-CMR_ONLY',
            'revision-id': 3,
            'native-id': 'gc',
            'provider-id': 'CMR_ONLY',
            'format': 'application/echo10+xml',
            'revision-date': '2022-03-15T16:00:30.377Z',
            /* eslint-enable quote-props */
          },
        }],
      });

    mockSSM
      .on(GetParameterCommand)
      .resolves({
        Parameter: { Value: 'foo' },
      });

    mockCloudWatchLogsClient
      .on(StartQueryCommand)
      .resolvesOnce({ queryId: 'ingestedQuery' })
      .resolvesOnce({ queryId: 'deletedQuery' })
      .on(GetQueryResultsCommand, { queryId: 'ingestedQuery' })
      .resolvesOnce({
        status: 'Scheduled',
        results: [],
      })
      .resolvesOnce({
        status: 'Running',
        results: [],
      })
      .resolvesOnce({
        status: 'Complete',
        results: [[{
          field: '@message',
          value: message,
        }]],
      })
      .on(GetQueryResultsCommand, { queryId: 'deletedQuery' })
      .resolves({
        status: 'Complete',
        results: [],
      });

    mockSNS
      .on(PublishCommand)
      .resolves({});

    await handler.run();

    expect(mockSNS.calls()).to.have.length(0);
  }).timeout(60000);
});

describe('Error Handling AWS problems', () => {
  it('handle rate-excecptions', async () => {
    nock('http://localhost:3003', {
      reqheaders: {
        Authorization: (headerValue) => headerValue.includes('foo'),
      },
    })
      .get(/.*/)
      .reply(200, {
        items: [{
          meta: {
            /* eslint-disable quote-props */
            'concept-type': 'granule',
            'concept-id': 'G1200416251-CMR_ONLY',
            'revision-id': 3,
            'native-id': 'gc',
            'provider-id': 'CMR_ONLY',
            'format': 'application/echo10+xml',
            'revision-date': '2022-03-15T16:00:30.377Z',
            /* eslint-enable quote-props */
          },
        }],
      });

    mockSSM
      .on(GetParameterCommand)
      .resolves({
        Parameter: { Value: 'foo' },
      });

    mockCloudWatchLogsClient
      .on(StartQueryCommand)
      .resolvesOnce({ queryId: 'ingestedQuery' })
      .resolvesOnce({ queryId: 'deletedQuery' })

      .on(GetQueryResultsCommand, { queryId: 'ingestedQuery' })
      .resolvesOnce({
        status: 'Scheduled',
        results: [],
      })
      .rejectsOnce({
        errorType: 'Runtime.UnhandledPromiseRejection',
        errorMessage: 'ThrottlingException: Rate exceeded',
        trace: [
          'Runtime.UnhandledPromiseRejection: ThrottlingException: Rate exceeded',
          '    at process.<anonymous> (/var/runtime/index.js:35:15)',
          '    at process.emit (events.js:314:20)',
          '    at processPromiseRejections (internal/process/promises.js:209:33)',
          '    at processTicksAndRejections (internal/process/task_queues.js:98:32)',
        ],
      })

      .on(GetQueryResultsCommand, { queryId: 'deletedQuery' })
      .resolves({
        status: 'Complete',
        results: [],
      });

    mockSNS
      .on(PublishCommand)
      .resolves({});

    await handler.run();

    expect(mockSNS.calls()).to.have.length(0);
  }).timeout(60000);
});

describe('The concept was deleted right after being ingested', () => {
  it('handle rate-excecptions', async () => {
    mockSSM
      .on(GetParameterCommand)
      .resolves({
        Parameter: { Value: 'foo' },
      });

    mockCloudWatchLogsClient
      .on(StartQueryCommand)
      .resolvesOnce({ queryId: 'ingestedQuery' })
      .resolvesOnce({ queryId: 'deletedQuery' })

      .on(GetQueryResultsCommand, { queryId: 'ingestedQuery' })
      .resolves({
        status: 'Complete',
        results: [[{
          field: '@message',
          value: message,
        }]],
      })
      .on(GetQueryResultsCommand, { queryId: 'deletedQuery' })
      .resolves({
        status: 'Complete',
        results: [[{
          field: '@message',
          value: deletedMessage,
        }]],
      });

    mockSNS
      .on(PublishCommand)
      .resolves({});

    await handler.run();

    expect(mockSNS.calls()).to.have.length(0);
  }).timeout(60000);
});
