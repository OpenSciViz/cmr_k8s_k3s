const axios = require('axios');
const {
  CloudWatchLogsClient,
  GetQueryResultsCommand,
  StartQueryCommand,
} = require('@aws-sdk/client-cloudwatch-logs');
const { SNSClient, PublishCommand } = require('@aws-sdk/client-sns');
const { SSMClient, GetParameterCommand } = require('@aws-sdk/client-ssm');

const region = process.env.REGION || 'us-east-1';
const env = process.env.ENVIRONMENT || 'test';
const logGroupName = `cmr-ingest-${env}`;

const windowSize = process.env.SEARCH_WINDOW_SIZE
  ? parseInt(process.env.SEARCH_WINDOW_SIZE, 10)
  : 10;

const searchDelay = process.env.SEARCH_DELAY ? parseInt(process.env.SEARCH_DELAY, 10) : 5;
const searchUrl = process.env.CMR_SEARCH_URL || 'http://localhost:3003';
const alertsSns = process.env.CMR_SNS_ALERTS_ARN;

const MS_PER_MINUTE = 60000;

/**
 * Runs a Cloudwatch Log Insights query
 * @returns queryId of the query
 */
async function startQuery(client, query) {
  const res = await client.send(new StartQueryCommand(query));
  return res.queryId;
}

/**
 * Converts a CSV string to a map
 * Numeric values will be converted to numbers
 *
 * ':akey "avalue", "anotherKey" 1234' => {'akey': 'avalue', 'anotherKey': 1234}
 */
function stringToMap(edn) {
  const [baseKey, baseValue] = edn.trim().split(/\s+/);
  if (!baseKey || !baseValue) {
    console.error(`[${edn}] contained unparseable values`);
    return {};
  }
  const key = baseKey.replace(/:/, '');
  const value = baseValue.replace(/"/g, '').replace(/:/, '');
  const rc = {};
  rc[key] = Number.isNaN(Number(value)) ? value : parseInt(value, 10);
  return rc;
}

/**
 * Extracts concept information from a log message
 * @param {string} message field from cloudwatch logs result
 */
function extractConcept(message) {
  const rx = /Successfully ingested \{(.*)\} from client/;

  // only grab the second element if present
  const [, rawConcept] = rx.exec(message);
  let concept;

  if (rawConcept) {
    concept = rawConcept.split(',')
      .map(stringToMap)
      .reduce((tgt, ele) => Object.assign(tgt, ele), {});
  }
  return concept;
}

/**
 * Get query results asynchronously
 */
const getQueryResults = async (client, queryId, attempts = 3, timeout = 10000) => {
  const promise = new Promise((resolve, reject) => {
    if (attempts < 0) {
      reject(new Error(`Ran out of attempts looking for CloudWatch Log query results for queryId [${queryId}]`));
    } else {
      client.send(new GetQueryResultsCommand({ queryId }))
        .then((resp) => {
          switch (resp.status) {
            case 'Complete':
              resolve(resp);
              break;
            case 'Cancelled':
            case 'Failed':
            case 'Timeout':
            case 'Unknown':
              reject(resp);
              break;
            case 'Running':
            case 'Scheduled':
            default:
              setTimeout(() => {
                Promise.resolve(getQueryResults(client, queryId, (attempts - 1))
                  .then(resolve)
                  .catch(reject));
              }, timeout);
          }
        })
        .catch(reject);
    }
  });
  return promise;
};

/**
 * Returns results from the AWS query
 */
async function getLastIngest(client, queryId) {
  let latestIngest;
  try {
    const resp = await getQueryResults(client, queryId);
    const [queryResult] = resp.results;
    if (queryResult) {
      const [data] = queryResult;
      latestIngest = extractConcept(data.value);
    }
  } catch (err) {
    console.error('A problem occurred getting query results', err);
  }
  return latestIngest;
}

/**
 * Returns results from the AWS query
 */
async function getConceptDeleted(client, queryId) {
  const resp = await getQueryResults(client, queryId);
  return resp.results.length;
}

/**
 * Gets a secureString from the ParameterStore
 */
async function getSecureParam(param) {
  const client = new SSMClient({ region });
  const command = new GetParameterCommand({ Name: param, WithDecryption: true });
  const { Parameter: { Value: value } } = await client.send(command);
  return value;
}

/**
 * Searches CMR for a concept by a conceptId and conceptType
 */
async function findByConceptId(conceptType, conceptId) {
  const url = `${searchUrl}/${conceptType}s.umm_json?concept_id=${conceptId}`;

  const echoSystemToken = await getSecureParam(`/${env}/search/CMR_ECHO_SYSTEM_TOKEN`);

  const opts = echoSystemToken ? { headers: { Authorization: echoSystemToken } } : {};

  console.info(`Searching CMR for ${conceptType} with concept-id [${conceptId}]`);

  const resp = await axios.get(url, opts);

  let foundConcept;
  if (resp.status === 200) {
    [foundConcept] = resp.data.items;
  } else {
    console.error(`An received unexpected status [${resp.status}] while searching CMR`, resp.data);
  }
  return foundConcept;
}

/**
 * Validates a concept was found
 */
const validateConceptFound = (latestIngestReported, concept) => {
  if (!concept || !concept.meta) {
    throw new Error(`Ingest reported success but the concept was not found during a search\nSEARCHED:\n${JSON.stringify(latestIngestReported)}`);
  }
  console.info('Concept successfully located', concept);
};

/**
 * Validates the revision-id of the found concept at minimum is equal to the ingested version
 * If multiple ingests occur the revision of the latest may be higher.
 * If searching for granules older revisions may not be available
 */
const validateMinimumRevisionId = (latestIngestReported, concept) => {
  const { 'revision-id': reportedRevisionId } = latestIngestReported;
  const { meta: { 'revision-id': revisionId } } = concept;

  if (revisionId < reportedRevisionId) {
    throw new Error(`The expected revision-id [${reportedRevisionId}] was lower than the most recent revision found during a search, found [${revisionId}] instead\nSEARCHED:\n${JSON.stringify(latestIngestReported)}\nFOUND:\n${JSON.stringify(concept)}`);
  }

  console.info(`Concept revision [${revisionId}] matches minimum expected revision [${reportedRevisionId}]`, latestIngestReported, concept);
};

/**
 * Main handler for the Lambda.
 * When triggered a query will be sent to CloudwatchLogs for successful ingest event
 * The concept information will be extracted and used to make a search in CMR for the
 * concept. If the concept is not found a message will be posted to the CMR Alerts SNS topic
 */
const run = async () => {
  const client = new CloudWatchLogsClient({ region });

  const now = new Date();
  const endTime = new Date(now.getTime() - (searchDelay * MS_PER_MINUTE));
  const startTime = new Date(now.getTime() - ((searchDelay + windowSize) * MS_PER_MINUTE));
  const ingestQuery = 'fields @message | filter @message like /Successfully ingested/ | filter @message like /collection|granule/ | limit 1';

  console.info('Searching CloudWatch Logs for latest successfully ingested concept');
  const queryId = await startQuery(client, {
    queryString: ingestQuery,
    startTime: startTime.getTime(),
    endTime: endTime.getTime(),
    logGroupName,
  });

  if (!queryId) throw new Error('Could not start query');

  const latestIngestReported = await getLastIngest(client, queryId);

  if (!latestIngestReported) {
    const message = `No ingests have been detected in search window [${startTime}] to [${endTime}]`;

    console.info(message);
    return {
      statusCode: 200,
      message,
    };
  }

  const {
    'concept-id': conceptId,
    'concept-type': conceptType,
    'native-id': nativeId,
  } = latestIngestReported;

  if (!conceptId || !conceptType) {
    const message = 'Logged concept was missing requried identifiable information to query for in the log message';
    console.info(message, latestIngestReported);

    return {
      statusCode: 200,
      latestIngestReported,
      error: [message],
    };
  }

  const deletedQuery = `fields @message | filter @message like /Deleting/ | filter @message like /${nativeId}/ | limit 1`;

  const deletedQueryId = await startQuery(client, {
    queryString: deletedQuery,
    startTime: startTime.getTime(),
    endTime: now.getTime(),
    logGroupName,
  });

  const deleted = await getConceptDeleted(client, deletedQueryId);

  if (deleted) {
    return {
      statusCode: 200,
      latestIngestReported,
      message: 'The ingested concept has been reported as having been deleted',
    };
  }

  try {
    const concept = await findByConceptId(conceptType, conceptId);
    validateConceptFound(latestIngestReported, concept);
    validateMinimumRevisionId(latestIngestReported, concept);

    return {
      statusCode: 200,
      latestIngestReported,
      concept,
    };
  } catch (e) {
    console.error(e);

    const snsClient = new SNSClient({ region });
    const errorAlert = new PublishCommand({
      Message: `${e.message}`,
      Subject: `CMR [${env}] Ingest Verification Failure`,
      TopicArn: alertsSns,
    });
    await snsClient.send(errorAlert);

    return {
      statusCode: 200,
      latestIngestReported,
      errors: [e.message],
    };
  }
};

module.exports.extractConcept = extractConcept;
module.exports.run = run;
