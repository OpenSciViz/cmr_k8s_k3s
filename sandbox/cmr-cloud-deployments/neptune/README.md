# CMR Neptune

This uses AWS CloudFormation template to stand up a Neptune graph database service and an EC2 instance in an NGAP AWS environment.

## Development Quick Start

### Prerequisites

* AWS CLI

### Inputs

There are a number of input parameters to this CloudFormation template:

- HTTPS: Whether or not to enforce SSL on your Neptune DB Cluster. Allowed values: '1' or '0'. Default '1'.
- EnableReadOnlyEndpoint: Whether or not to create a read-only Neptune endpoint. Allowed values: 'true' of 'false'. Default 'false'.
- VPCId: A VPC ID to create the Neptune cluster in.
- Subnet1: A Subnet ID that is associated with the VPC.
- Subnet2: Another Subnet ID that is associated with the VPC.
- AMI: NGAP AMI id for the base image of the EC2 instance.
- DbInstanceType: Instance type of the Neptune DB instance. Default 'db.r4.xlarge'.
- EC2SSHKeyPairName: Keypair name to log onto the instances.
- EC2ClientInstanceType: Instance type of the EC2 client instance. Default 't3.micro'
- SnapshotID: A snapshot id. This is optional and allows you to create a Neptune instance from a previous snapshot.

### Outputs

These are the outputs of this CloudFormation template:

- DBClusterId: Neptune Cluster Identifier
- DBClusterEndpoint: Master Endpoint for Neptune Cluster
- DBClusterReadEndpoint: DB cluster ReadOnly Endpoint, will be the same as the master endpoint if there is only one node in the cluster.
- DBClusterResourceId: Neptune Cluster Resource Identifier
- DBInstance1Endpoint: DB instance1 endpoint
- DBInstance2Endpoint: DB instance2 endpoint
- DBSubnetGroupId: Neptune DBSubnetGroup Identifier
- EC2InstanceID: Instance ID of EC2 instance

### Deploying

To deploy a CloudFormation stack with an one node Neptune cluster in the Workload environment:

```
export AWS_PROFILE=cmr-sandbox
aws cloudformation deploy --template-file neptune_cf.yaml --stack-name <cf stack name> --parameter-overrides AMI=<ngap ami id> VPCId=<vpc id> Subnet1=<private subnet 1 id> Subnet2=<private subnet 2 id> EC2SSHKeyPairName=<keypair name> NeptuneName=<name for resource creation> --capabilities CAPABILITY_NAMED_IAM
```

To deploy a CloudFormation stack with a two nodes Neptune cluster with separate read-only and write endpoints in the Workload environment:
```
aws cloudformation deploy --template-file neptune_cf.yaml --stack-name <cf stack name> --parameter-overrides EnableReadOnlyEndpoint=true AMI=<ngap ami id> VPCId=<vpc id> Subnet1=<private subnet 1 id> Subnet2=<private subnet 2 id> EC2SSHKeyPairName=<keypair name> NeptuneName=<name for resource creation> --capabilities CAPABILITY_NAMED_IAM
```

To delete the created CloudFormation stack:
```
aws cloudformation delete-stack --stack-name <cf stack name>
```
where `<cf stack name>` is the stack name you used to deploy the CloudFormation stack.

### Communicate with the Neptune endpoints

You can communicate with the Neptune instance using the endpoint returned by DBClusterEndpoint for both read and write operations; using the endpoint returned by DBClusterReadEndpoint for read-only operations if read-only is enabled. To leverage the Gremlin API, use port 8182.

For security reasons, AWS will only let you communicate with the Neptune instance from inside the VPC it was created in.

There are three ways to communicate with the Neptune endpoints:
- EC2 instance in the CloudFormation stack.

  You can log onto the EC2 instance as returned by the EC2InstanceID output and access the Neptune endpoints from there.

- Locally tunnel through the bastion host/ssm and EC2 instance
To communicate with your Neptune endpoints from outside the VPC (e.g. local development), you will have to ssh tunnel through the EC2 instance. e.g.
```
ssh -L8182:<neptune endpoint>:8182 -J sandbox-bastion -i ~/.ssh/yliu10-sandbox.pem ec2-user@<ec2 instance ip>
```
then,
```
curl -k -XPOST https://localhost:8182/gremlin -d '{"gremlin":"g.V().limit(1)"}'
```

- Through the CMR LB neptune endpoint if set up

  e.g.
```
curl -i -X POST <CMR LB url>/neptune  -d '{"gremlin":"g.V().limit(1)"}'
```

### NOTE

- The above tunnel commands goes through the bastion host. That is going away. The tunnel commands need to be updated once we switch over to SSM Session Manager.

- In an NGAP environment, you can obtain the latest approved AMI as follows:
```
aws ssm get-parameter --name image_id_amz2
```

## api-routing
`api-routing` directory contains the serverless deployment code for deploying the routing lambda function and setting up ALB listener rule for exposing Neptune gremlin API via the CMR application service API. See README in api-routing for details.
