#!/usr/bin/env python3
import requests

def main(api_url = 'http://epic.gsfc.nasa.gov/api/images.php'):
  """
  from: https://epic.gsfc.nasa.gov/about/api ... freshened for python3
  """
  try:
    # r = requests.get(api_url, auth=('user', 'pass'))
    r = requests.get(api_url)
    data = r.json()
    for img in data:
      jpg_meta = 'http://epic.gsfc.nasa.gov/epic-archive/jpg' + img['image'] + '.jpg'
      print(jpg_meta)
  except:
    print("jpg metadata fetch failed from:", api_url)


if __name__ == '__main__':
# exec test main
  main()

