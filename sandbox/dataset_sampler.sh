#!/bin/bash
echo 'strings *.h* *.H* *.fits'
echo https://hdfeos.org/zoo/
echo https://hdfeos.org/zoo/index_openGESDISC_Examples.php

echo ZA
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/HIRDLS-Aura_L3ZFCNO2_v07-00-20-c01_2005d022-2008d077.he5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/BUV-Nimbus04_L3zm_v01-02-2013m0422t101810.h5

echo Swath
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/AMSR_E_L2_Land_V11_201110031920_D.he5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/AMSR_E_L2_Ocean_V06_200206190029_D.hdf
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/GLAH10_633_2131_001_1317_0_01_0001.H5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/GLAH13_633_2103_001_1317_0_01_0001.h5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/T2010001000000.L2_LAC_SST.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/AQUA_MODIS.20020704T185006.L2.SST.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/2A12.20140308.92894.7.HDF
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/AIRS.2002.08.30.227.L2.RetStd_H.v6.0.12.0.G14101125810.hdf
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/HIRDLS-Aura_L3ZFCNO2_v07-00-20-c01_2005d022-2008d077.he5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/ATL02_20190520193327_08020311_004_01.h5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/MORE/GESDISC/OMI/OMI-Aura_L2-OMAERUV_2019m0801t0453-o80020_v003-2019m0802t194820.SUB.he5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/MORE/GESDISC/OMI/OMI-Aura_L2-OMTO3_2012m0817t1917-o43040_v003-2013m0215t193715.he5

echo Grid
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/MOD10C1.A2005018.006.2016141204712.hdf
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/AMSR_E_L3_DailyLand_V06_20050118.hdf
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/NISE_SSMISF17_20110424.HDFEOS
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/ATM_SURFACE_TEMP_HUM_WIND_PRES_day_mean_2017-12-31_ECCO_V4r4_latlon_0p50deg.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/V20120012012366.L3m_YR_SNPP_KD490_Kd_490_4km.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/O1997011.L3m_DAY_CHL_chlor_a_9km.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/T2010001000000.L2_LAC_SST.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/A20021612002192.L3m_R32_SST_sst_4km.nc
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/SW_S3E_2003100.20053531923.hdf
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/ATL16_20210101010136_01231001_003_01.h5
echo https://gamma.hdfgroup.org/ftp/pub/outgoing/NASAHDF/AIRS.2003.02.05.L3.RetStd_H001.v6.0.12.0.G14112124328.hdf
echo 

echo http://sites.science.oregonstate.edu/ocean.productivity/index.php
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2000.hdf
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2001.hdf
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2002.hdf
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2003.hdf
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2004.hdf
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2005.hdf
echo http://orca.science.oregonstate.edu/data/2x4/yearly/landOcean/hdf/npp.2006.hdf

echo FITS tests
echo https://www.cosmos.esa.int/web/planck/pla
echo https://fits.gsfc.nasa.gov/fits_samples.html
echo https://lambda.gsfc.nasa.gov/product/cobe/dmr_cio_get.html
echo https://lambda.gsfc.nasa.gov/data/cobe/cobe_analysis_software/cgis-for.tar
echo https://lambda.gsfc.nasa.gov/product/wmap/dr5/m_products.html

