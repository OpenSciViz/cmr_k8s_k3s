#!/usr/bin/env python3
import os, sys, json, urllib.request # urllib3

def main(api_url = 'http://epic.gsfc.nasa.gov/api/images.php'):
  """
  from: https://epic.gsfc.nasa.gov/about/api ... freshened for python3
  """
  try:
    #http = urllib3.PoolManager() # ala https://github.com/urllib3/urllib3
    #json_data = http.request("GET", api_url)
    json_data = urllib.request.urlopen(api_url).read()
    data = json.loads(json_data.decode("utf-8"))
    for img in data:
      jpg_meta = 'http://epic.gsfc.nasa.gov/epic-archive/jpg' + img['image'] + '.jpg'
      print(jpg_meta)
  except:
    print("jpg metadata fetch failed from:", api_url)


if __name__ == '__main__':
# exec test main
  main()

