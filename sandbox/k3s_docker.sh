#!/bin/bash

echo https://cwienczek.com/2020/06/import-images-to-k3s-without-docker-registry/
echo Build and package your docker container into tar archive:

echo docker build -t test-app:v1.0.0 .
echo docker save --output test-app-v1.0.0.tar test-app:v1.0.0

echo copy it over to the target machine with k3s intalled:

echo rsync -v test-app-v1.0.0.tar remote:/home/ubuntu/test-app-v1.0.0.tar
echo ssh into remote machine and import the image to the cluster:

echo sudo k3s ctr images import /home/ubuntu/test-app-v1.0.0.tar


echo use the image in your kubernetes manifests like this:

cat -n <<HEREDOC
apiVersion: apps/v1
kind: Deployment
metadata:
  name: test-app
  labels:
    name: test-app
spec:
  replicas: 1
  selector:
    matchLabels:
      name: test-app
  template:
    metadata:
      labels:
        name: test-app
    spec:
      containers:
        - name: test-app
          image: test-app:v1.0.0
          imagePullPolicy: Never
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
HEREDOC

