AWS console: https://us-east-2.console.aws.amazon.com/settings/home?region=us-east-2#/
AWS cloud9: https://us-east-1.console.aws.amazon.com/cloud9control/home?region=us-east-1#/
AWS shell: https://us-east-1.console.aws.amazon.com/cloudshell/home?region=us-east-1#bf7f9c01-1441-4762-86ed-51cc4962cfce
AWS blog: https://aws.amazon.com/blogs/containers/reducing-aws-fargate-startup-times-with-zstd-compressed-container-images/

Try CMR client APIs following documented examples online:

https://wiki.earthdata.nasa.gov/display/CMR/Common+Metadata+Repository+Home
https://www.earthdata.nasa.gov/eosdis/science-system-description/eosdis-components/cmr
https://www.earthdata.nasa.gov/learn/use-data/tools

https://github.com/nasa/Common-Metadata-Repository.git
https://cmr.earthdata.nasa.gov/ingest/site/docs/ingest/api.html
https://cmr.earthdata.nasa.gov/ingest/site/docs/search/api.html

https://nasa.github.io/cumulus/docs/cumulus-docs-readme
https://github.com/nasa/cumulus/releases
https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.AuroraPostgreSQL.html
https://docs.aws.amazon.com/prescriptive-guidance/latest/patterns/migrate-data-from-an-on-premises-oracle-database-to-aurora-postgresql.html

https://www.ncei.noaa.gov/maps/monthly/ -- tbd in opengis repo...
https://towardsdatascience.com/deploy-your-python-app-with-aws-fargate-tutorial-7a48535da586 -- does CMR use AWS Fargate?
https://www.baeldung.com/ops/docker-jvm-heap-size

Note makefile can be used for running tests on localhost and for doc gen
https://epic.gsfc.nasa.gov/about/api
https://wiki.earthdata.nasa.gov/display/CMR/CMR+Client+Partner+User+Guide#CMRClientPartnerUserGuide-Chapter3:Searchingformetadata
    time ./epic_images.py
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226010436.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226025239.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226044041.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226062844.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226081646.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226100449.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226115251.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226134054.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226152856.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226171659.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226190501.jpg
    http://epic.gsfc.nasa.gov/epic-archive/jpgepic_1b_20230226205304.jpg

    real	0m1.998s
    user	0m0.116s
    sys	0m0.025s


python-gfortran lowtran ... is there hdf5 support?
https://www.admin-magazine.com/HPC/Articles/HDF5-with-Python-and-Fortran

https://graphql.org/code/ -- mentions postman
https://learning.postman.com/docs/getting-started/introduction/#testing-apis

Institutional Repository as catalogue?
https://investigating-archiving-git.gitlab.io/updates/Self-Archiving-Software-in-IRs/

Coonsider CMR deployment "as-is / no code mod" and try extendig with new featuer via new container pods?

https://www.catalyst2.com/knowledgebase/networking/how-to-run-multiple-containers-with-an-nginx-reverse-proxy/

Testsing: https://0x2142.com/from-postman-to-python-your-first-get-request/

#################################### Notes from Gian ################################

Hi CMR Team,
I know there is lots of communication about the CMR code and implementation.
I want to share some background since we implemented CMR in NCIS and have kept very detailed documents and all the changes we made.

The first rule we created with NASA  CMR management was that we would not change the code until we agreed with them to minimize the development efforts. We did not want to deviate from the release and wanted to keep with their release model. Since then, we have updated CMR three times, and yes, we had some challenges, but they were related to configuration and not the code changes.
We do have a Prod and UAT environment to test all changes before we migrate them to Prod.
We have around 40 collections (datasets) and probably 20 to 30M granules.
We do have some data sets that are ingested on a near real-time basis 7x24. 

In our initial implementation, we had support from NASA, and we brought up a working CMR in about three weeks. We used their process to install (with minimum modifications). 
To keep costs lower, we only have one server per service vs. seven servers in NASA (CMR uses AWS Forgates for services, but they are full-time operating using SAM to configure and install). We used smaller instances for Oracle etc.
Gefter Chongong copied on this email, did most of the configuration and made it work. 

One main issue was the authentication, and to minimize development efforts, we used the Eartdata login as our authentication; now, work will be required to use NESDIS SSO or Login.gov or some other solution. This function is required if download includes multiple collections.
NESDIS CMR is not public; one needs ERAV VPN to access it. 

 I would recommend you, please go through these documents, and we can set up a session to explain additional details.

Once the CMR is up and running, the next challenge is the creation of Collections and the actual process to ingest data and store the metadata using UMM format into CMR.

Here is a diagram that explains a process for Fire Data from NODD (old name BDP) - we used NCCF code for metadata extraction and modified it perform changes and used CMR API to write into records.

CMR Ingest Fire data and conversion to Zarr and COG

There are several variations to this process. Initially, this code was running on a server with docker, and now it is in EKS.

In CMR, we implemented Earth Data search, Open Search and STAC plus Commandline/Curl - using Rest API.

Here are CMR Commands document
NESDIS CMR Environments Commands

Here is a document where I have copied data from NASA's internal WIKI site, so you don't need a login.

CMR Description from NASA Wiki Internal Site

Gefetr and I can meet in the next few days to share more details once you have reviewed these documents.

If I missed someone, please feel free to copy them.

Regards,
Gian

---------- Forwarded message ---------
From: gefter chongong - NOAA Affiliate <gefter.chongong@noaa.gov>
Date: Wed, Feb 1, 2023 at 12:14 PM
Subject: Re: CMR installation info
To: Evan McQuinn - NOAA Affiliate <evan.mcquinn@noaa.gov>
Cc: Thomas Foster - NOAA Affiliate <thomas.foster@noaa.gov>, Gian Dilawari - NOAA Affiliate <gian.dilawari@noaa.gov>


Evan,
Gian and I discussed about the CMR documents and are adding the following repos with NOAA changes for the CMR


https://git-codecommit.us-east-1.amazonaws.com/v1/repos/nesdis-cmr-earthdata-search-upgrade - Earthdata Search

https://git-codecommit.us-east-1.amazonaws.com/v1/repos/nesdis-cmr-edsc-graphql-upgrade - graphql

https://git-codecommit.us-east-1.amazonaws.com/v1/repos/cmr-upgrade-209 - repo with the version 1.209 of CMR we are currently running

Thanks

On Wed, Feb 1, 2023 at 11:16 AM gefter chongong - NOAA Affiliate <gefter.chongong@noaa.gov> wrote:
Evan,
There is a confluence page with information and documents on CMR.
https://confluence.nesdis-hq.noaa.gov/confluence/pages/viewpage.action?pageId=4296249

There is an installation guide with all the NOAA changes here that was made back in 2021, this has to be updated 
https://docs.google.com/document/d/1hemKdspC7vx55ahje69FJW9cQTJpy2t7uvB9kUfsjSU/edit

There are important repositories linked here, the bottom 4 requires credentials from NASA

https://github.com/nasa/Common-Metadata-Repository
https://github.com/nasa/cmr-opensearch
https://github.com/nasa/earthdata-search

https://git.earthdata.nasa.gov/projects/EDSC/repos/edsc-graphql/browse
https://git.earthdata.nasa.gov/projects/CMR/repos/cmr-cloud-deployments/browse
https://git.earthdata.nasa.gov/projects/CMR/repos/cmr-cloud-elasticsearch/browse
https://git.earthdata.nasa.gov/projects/CMR/repos/populate-parameter-store/browse

On Tue, Jan 31, 2023 at 5:29 PM Evan McQuinn - NOAA Affiliate <evan.mcquinn@noaa.gov> wrote:
Gefter, Thomas -

Hey guys. As you know NCCF is preparing to install CMR, probably into the new 5006 Dev environment once it's ready. Pura has asked that we collect existing information about our CMR usage thus far, so I'm reaching out to you two.

Thomas: You put together an implementation plan for CMR in NCCF and I believe it's a set of Jira issues, right? Is there an epic or filter or something you can point me at?

Gefter: Aside from the CMR wiki documents, I know you've been generating documents regarding CMR in NCIS. I'm aware of NESDIS CMR Environment commands, but are there others I could share?

Thanks guys,
Evan


--
Gefter Chongong
Sr. Devops/Infrastructure Engineer
NIIS - Alpha Omega Integration Inc.
 
1335 East-West Highway (SSMC1)
Silver Spring, MD 20910
Cell:830-261-8625
Email: gefter.chongong@noaa.gov
Email: gchongong@actionet.com
Contractor

