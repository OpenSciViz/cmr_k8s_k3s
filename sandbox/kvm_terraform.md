https://computingforgeeks.com/how-to-provision-vms-on-kvm-with-terraform/
https://docs.fedoraproject.org/en-US/quick-docs/using-nested-virtualization-in-kvm/
https://www.virtualizationhowto.com/2022/09/install-kvm-ubuntu-22-04-step-by-step/
https://itnext.io/beware-of-depends-on-for-modules-it-might-bite-you-da4741caac70

from https://stackoverflow.com/questions/75544520/terraform-depend-existing-module-resource-on-newly-created-resources
    Looks like oci_identity_policy can take up to 10seconds:

    New policies take effect typically within 10 seconds.

    So indeed even if you set an explicit dependency with depends_on or have an implicit one by just using attributes from oci_identity_policy.boot_volume you would still need to wait for that duration.

    I suggest either filing an issue in OCI provider's repository so they implement this "readiness" check in the provider, or using workarounds like time_sleep:

    resource "oci_identity_policy" "boot_volume"{

    count = local.create_kms_key ? 1 : 0

    ...

    }

    resource "time_sleep" "wait_for_identity_policy" {

      depends_on = [oci_identity_policy.boot_volume]

      create_duration = "15s"
    }

    resource "SOMETHING" "next" {
      depends_on = [time_sleep.wait_for_identity_policy]
    }


