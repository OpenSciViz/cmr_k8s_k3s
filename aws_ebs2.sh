#!/bin/bash
echo https://us-east-1.console.aws.amazon.com/cloud9/ide/e975033d8cce44939d6d774b819b9e58
echo https://aws.amazon.com/blogs/compute/how-to-mount-linux-volume-and-keep-mount-point-consistency/
echo https://docs.aws.amazon.com/cloud9/latest/user-guide/move-environment.html#move-environment-resize
dmesg|grep -i uuid
cat /etc/fstab
blkid
file -s /dev/nvm*
file -s /dev/nvme1n1
mkfs.xfs /dev/nvme1n1
mkdir /vol20g
echo "$(blkid -o export /dev/nvme1n1 | grep ^UUID=) /vol20g xfs defaults,noatime" | tee -a /etc/fstab
mount -a
df -h
