#!/bin/bash
# https://clojure.org/guides/install_clojure
echo 'curl -O https://download.clojure.org/install/linux-install-1.11.1.1257.sh'
echo chmod +x linux-install-1.11.1.1257.sh'
echo sudo ./linux-install-1.11.1.1257.sh'
echo 'extend the MANPATH in /etc/man_db.conf to include the manual pages: MANPATH_MAP /opt/infrastructure/clojure/bin /opt/infrastructure/clojure/man'
