# datahub docker quickstart

## An error occured that required edit of click/utils.py: near line 299

    vim /home/hon/.local/lib/python3.8/site-packages/click/utils.py +299

## rerun of datahub docker quicsktart completed ...

hon@4science:~/hon_gitlab/cmr_k8s_k3s/py_utils$ docker ps -a


    CONTAINER ID   IMAGE                                       COMMAND                   CREATED          STATUS                     PORTS                                                           NAMES

    aa9adcc479d1   linkedin/datahub-kafka-setup:head           "/bin/sh -c ./kafka-…"    16 minutes ago   Exited (0) 7 minutes ago                                                                   kafka-setup

    277d8947184d   confluentinc/cp-schema-registry:7.2.2       "/etc/confluent/dock…"    16 minutes ago   Up 16 minutes              0.0.0.0:8081->8081/tcp, :::8081->8081/tcp                       schema-registry

    ff71e6ecc162   acryldata/datahub-actions:head              "/bin/sh -c 'dockeri…"    16 minutes ago   Up 16 minutes                                                                              datahub-datahub-actions-1

    29cb49ccb376   linkedin/datahub-frontend-react:head        "/bin/sh -c ./start.…"    16 minutes ago   Up 16 minutes (healthy)    0.0.0.0:9002->9002/tcp, :::9002->9002/tcp                       datahub-frontend-react

    dd2b3eadadca   confluentinc/cp-kafka:7.2.2                 "/etc/confluent/dock…"    16 minutes ago   Up 16 minutes              0.0.0.0:9092->9092/tcp, :::9092->9092/tcp                       broker

    e9ea5ed11227   linkedin/datahub-gms:head                   "/bin/sh -c /datahub…"    16 minutes ago   Up 16 minutes (healthy)    0.0.0.0:8080->8080/tcp, :::8080->8080/tcp                       datahub-gms

    bbee1e6afcb0   linkedin/datahub-elasticsearch-setup:head   "/bin/sh -c 'if [ \"$…"   16 minutes ago   Exited (0) 7 minutes ago                                                                   elasticsearch-setup

    5e01fe45656a   acryldata/datahub-mysql-setup:head          "/bin/sh -c 'dockeri…"    16 minutes ago   Exited (0) 7 minutes ago                                                                   mysql-setup

    9974e503109f   confluentinc/cp-zookeeper:7.2.2             "/etc/confluent/dock…"    17 minutes ago   Up 16 minutes              2888/tcp, 0.0.0.0:2181->2181/tcp, :::2181->2181/tcp, 3888/tcp   zookeeper

    bae454c0b7bd   elasticsearch:7.10.1                        "/tini -- /usr/local…"    17 minutes ago   Up 16 minutes (healthy)    0.0.0.0:9200->9200/tcp, :::9200->9200/tcp, 9300/tcp             elasticsearch

    020ddc911f72   acryldata/datahub-upgrade:head              "java -jar /datahub/…"    17 minutes ago   Exited (0) 6 minutes ago                                                                   datahub-upgrade

    75790b42cf5e   mysql:5.7                                   "docker-entrypoint.s…"    17 minutes ago   Up 16 minutes              0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp            mysql

    2d513b8718e8   qgis-server                                 "/tini -- /home/qgis…"    5 weeks ago      Exited (143) 2 days ago  



