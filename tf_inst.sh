#/bin/bash
echo https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
echo
echo 'sudo apt-get update && sudo apt-get install -y gnupg software-properties-common'
echo 'wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list'
echo 'sudo apt update'
echo 'sudo apt-get install terraform'
echo
echo Now that you have added the HashiCorp repository, you can install Vault, Consul, Nomad and Packer with the same command.
echo terraform -help
echo terraform -help plan

