# Cluster management template project init for NASA CMR K8s and K3s deployments

## CMR git repos (related git replos have been or will be added as submodules in NOAA CMR gitlab repos):

https://github.com/amundsen-io/amundsen.git -- python

https://github.com/nasa/Common-Metadata-Repository.git -- clojure

https://practical.li/clojure/

https://clojure.org/guides/install_clojure

    curl -O https://download.clojure.org/install/linux-install-1.11.1.1257.sh
    chmod +x linux-install-1.11.1.1257.sh
    sudo ./linux-install-1.11.1.1257.sh

https://www.braveclojure.com/getting-started/

https://clojurescript.org/index

https://clojurescript.org/about/differences

https://planck-repl.org/ -- ClojureScript repl

https://emacs-lsp.github.io/lsp-mode/tutorials/clojure-guide/

https://arielortiz.info/apps/s201413/tc2006/notes_installing_clojure/

https://github.com/nasa/cmr-metadata-review.git -- ruby and python and json with postgres cloudformation scripts 

https://github.com/nasa/cmr-stac.git -- terraform

https://github.com/nasa/python_cmr.git -- python3.? 

## Refs (some git replos have been or will be added as submodules in NOAA CMR gitlab repos):

Access IceSat data and more -- https://nsidc.org/data/user-resources/help-center/programmatic-data-access-guide

A potential CMR cat suplemental index webapp: https://wiki.lyrasis.org/display/DSPACE/Jython+webapp+for+DSpace

Another potential open source cat to consider: https://datahubproject.io/

    datahub docker quickstart

    datahub docker ingest-sample-data

Yet Another cat and more opensource proj: https://github.com/ckan and https://github.com/ckan/ckan-docker.git

https://docs.ckan.org/en/2.9/extensions/adding-custom-fields.html

https://developer.hashicorp.com/terraform/tutorials/docker-get-started/docker-build 

Note CMR deployment uses Terraform and tarball snapshots have benn placed in the backup/ folder here

TBD: Try localhost CMR terraform install via KVM VMs and compare CMR Elastic Search to DSpace Solr and Ckan search?

https://solr.apache.org/guide/solr/latest/deployment-guide/solr-in-docker.html

Note both Solr and Elastic search rely on Lucene -- https://github.com/apache/lucene.git

https://github.com/elastic/dockerfiles.git

Note according to Joeseph H. Jython is NOT of use/interest in the Science Sandbox.

https://api.nasa.gov/ -- compare planetary and earthsci api
https://epic.gsfc.nasa.gov/about/api -- Earth Polychromatic Imaging Camera -- "Blue Marbles!"

    Your API key for evizbiz@gmail.com is:

    Ei9rUdHsL2fRfgeQ81BoZ1vbcosESKMgzEPv9hm3
    You can start using this key to make web service requests. Simply pass your key in the URL when making a web request. Here's an example:

    https://api.nasa.gov/planetary/apod?api_key=Ei9rUdHsL2fRfgeQ81BoZ1vbcosESKMgzEPv9hm3

    For additional support, please contact us. When contacting us, please tell us what API you're accessing and provide the following account details so we can quickly find you:

    Account Email: evizbiz@gmail.com
    Account ID: 51707c10-4e92-483c-aead-9d105400a068


TBD: CMR AWS Fargate deploy

https://cmr.earthdata.nasa.gov/search/site/docs/search/api.html

https://cmr.earthdata.nasa.gov/search/site/docs/search/api.html#json

https://cmr.earthdata.nasa.gov/ingest/site/docs/ingest/api.html

https://search.earthdata.nasa.gov/search

https://www.earthdata.nasa.gov/engage/open-data-services-and-software/api/cmr-api

https://www.earthdata.nasa.gov/learn/acronym-list

https://www.star.nesdis.noaa.gov/star/acronyms.php

https://lpdaac.usgs.gov/data/get-started-data/workflow-examples/

https://docs.google.com/document/d/1A4eHHYBXi0F-38XM4chljZQbizLKy2eE/ -- Next Generation Fire System

https://app.diagrams.net/#G1BNvGNB5dTaUawsv9VpHJSMm_YdnZMoZ_  -- Fire Storefront App 

    NGFS -- Network for Greening the Financial System -- https://www.ngfs.net/ngfs-scenarios-portal/

    NIFC -- National Interagency Fire Center -- https://nifc.gov

    NWS  -- National Weather Service -- https://www.weather.gov/

    CALFIRE -- https://www.fire.ca.gov/

    FLFIRE -- https://ffspublic.firesponse.com/

    FMIS -- https://www.fdacs.gov/Forest-Wildfire/Wildland-Fire/Resources/Fire-Tools-and-Downloads/Fire-Management-Information-System-FMIS-Mapping-Tool


https://docs.k3s.io/

https://developers.google.com/apps-script/api/quickstart/python -- chatops-devops

https://about.gitlab.com/blog/2022/03/08/how-to-troubleshoot-a-gitlab-pipeline-failure/

https://docs.gitlab.com/ee/user/project/canary_deployments.html#how-to-set-up-a-canary-ingress-in-a-canary-deployment


... more github repos of interest:

https://github.com/space-physics/lowtran.git -- Lowtran lives well into the 21st century!

https://github.com/evizbiz/earth.eviz.biz.git -- my 8 year old nodejs GIS map + NWS json local weather popup

https://github.com/googleworkspace

https://github.com/googleworkspace/python-samples.git

https://github.com/postmanlabs/postman-code-generators.git


###########

Example [cluster management](https://docs.gitlab.com/ee/user/clusters/management_project.html) project.

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

For more information, see [the documentation for this template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/cluster-management).

## Supported Kubernetes versions

The project should be used with a [supported version of Kubernetes cluster](https://docs.gitlab.com/ee/user/project/clusters/#supported-cluster-versions).
