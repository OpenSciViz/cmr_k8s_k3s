REPOSITORY                        TAG               IMAGE ID       CREATED         SIZE

whumphrey/swish-e                 latest            3ce647a9fcea   47 hours ago    1.37GB

dspace/dspace-angular             dspace-7_x        07cc8af91120   2 days ago      2.81GB

dspace/dspace                     dspace-7_x-test   98847c882697   6 days ago      975MB

solr                              8.11-slim         3def194b0e02   2 weeks ago     516MB

node                              18-alpine         f520ad35ba68   3 weeks ago     175MB

qgis-server                       latest            1ffa3c2f7a78   5 weeks ago     1.32GB

nginx                             latest            3f8a00f137a0   7 weeks ago     142MB

debian                            bullseye-slim     a36a86fb63b1   7 weeks ago     80.5MB

debian                            stretch           662c05203bab   9 months ago    101MB

clojure                           openjdk-8-lein    72503bb79b01   11 months ago   314MB

dspace/dspace-postgres-pgcrypto   latest            c2b6540d83bd   2 years ago     300MB

kartoza/qgis-server               latest            6655fdb2fd95   3 years ago     1.16GB

