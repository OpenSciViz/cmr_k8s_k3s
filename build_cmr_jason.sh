#!/bin/bash
echo presumably after a successfull build ./install-bits/ should be populated with jars...
echo from Jason 
echo
echo java -version
echo chmod +x install-bits/lein
echo cp install-bits/lein /usr/local/bin/
echo mkdir oracle-lib/support/
echo cp install-bits/ojdbc8.jar install-bits/ons.jar install-bits/ucp.jar oracle-lib/support/
echo export PATH=$PATH:`pwd`/bin
echo source resources/shell/cmr-bash-autocomplete
echo cmr install oracle-libs
echo cmr setup profile
echo cmr setup dev
echo cd dev-system
echo cmr build uberjars

# CMR readme:
echo export PATH=$PATH:`pwd`/bin
echo source resources/shell/cmr-bash-autocomplete
echo which cmr
echo '"export PATH=\$PATH:`pwd`/bin" >> ~/.profile'
echo '"source `pwd`/resources/shell/cmr-bash-autocomplete" >> ~/.profile'
echo 'copy Oracle JDBC JAR files into ./oracle-lib/support'
echo 'mkdir -p ./oracle-lib/support'
echo 'cp -p /var/tmp/*.jar ./oracle-lib/support'
echo 'cmr install oracle-libs'

echo 'touch ./dev-system/profiles.clj'
echo '{:dev-config {:env {:cmr-metadata-db-password "<YOUR PASSWORD HERE>"
                    :cmr-sys-dba-password "<YOUR PASSWORD HERE>"
                    :cmr-bootstrap-password "<YOUR PASSWORD HERE>"
                    :cmr-ingest-password "<YOUR PASSWORD HERE>"
                    :cmr-urs-password "<YOUR PASSWORD HERE>"}}} >> ./dev-system/profiles.clj'

echo
echo cmr setup dev
echo cmr start repl

echo 'Once given a Clojure prompt, run (reset)'


echo my bash history
echo 'mkdir -p {./install-bits,./oracle-lib/support}'
echo 'assume oracle-8 related jar files have been downloaded to /var/tmp'
echo 'cp -p /var/tmp/*.jar install-bits/'
echo 'export PATH=/home/hon/.local/bin:/opt/conda3/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin:/home/hon/hon_gitlab/cmr_k8s_k3s/Common-Metadata-Repository/bin'
echo which cmr

