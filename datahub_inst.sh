#!/bin/bash
echo install datahub
echo alias pip3='python4-m pip'
echo pip3 install --upgrade pip wheel setuptools
echo pip3 install --upgrade acryl-datahub
echo pip3 install acryl-datahub[datahub-lite]
echo pip3 install acryl-datahub[s3]
echo pip3 install acryl-datahub[mongodb]
echo pip3 install acryl-datahub[postgres]
echo
echo which datahub
echo datahub version
echo datahub docker check

